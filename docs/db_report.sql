/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : db_report

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2018-02-24 09:46:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_account
-- ----------------------------
DROP TABLE IF EXISTS `tb_account`;
CREATE TABLE `tb_account` (
  `AccNo` int(11) NOT NULL,
  `AccName` varchar(75) DEFAULT NULL,
  `AccGroup` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AccNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_account
-- ----------------------------
INSERT INTO `tb_account` VALUES ('111', 'Kas', '1', '2017-05-03 14:31:22', '2017-05-12 08:12:14');
INSERT INTO `tb_account` VALUES ('112', 'Piutang Usaha', '1', '2017-05-03 14:31:42', '2017-05-12 08:12:13');
INSERT INTO `tb_account` VALUES ('113', 'Peralatan Kantor', '1', '2017-05-03 14:32:05', '2017-05-12 08:16:42');
INSERT INTO `tb_account` VALUES ('121', 'Perlengkapan Kantor', '1', '2017-05-03 14:32:39', '2017-05-12 08:16:55');
INSERT INTO `tb_account` VALUES ('211', 'Hutang Usaha', '2', '2017-05-03 14:33:03', '2017-05-12 08:16:56');
INSERT INTO `tb_account` VALUES ('311', 'Modal Ibrahim', '3', '2017-05-03 14:33:24', '2017-05-12 08:14:01');
INSERT INTO `tb_account` VALUES ('312', 'Penarikan Tn Ibrahim', '3', '2017-05-03 15:10:25', '2017-05-12 08:13:08');
INSERT INTO `tb_account` VALUES ('411', 'Pendapatan Jasa Perbaikan Jalan', '4', '2017-05-03 14:33:43', '2017-05-12 08:33:08');
INSERT INTO `tb_account` VALUES ('511', 'Beban Sewa', '5', '2017-05-03 14:34:00', '2017-05-12 08:16:33');
INSERT INTO `tb_account` VALUES ('512', 'Beban Gaji', '5', '2017-05-03 14:34:14', '2017-05-12 08:13:53');
INSERT INTO `tb_account` VALUES ('513', 'Tagihan Listrik dan Telepon', '5', '2017-05-03 14:34:30', '2017-05-12 08:12:22');
INSERT INTO `tb_account` VALUES ('514', 'Beban Lain-Lain', '5', '2017-05-03 14:34:40', '2017-05-12 08:13:47');

-- ----------------------------
-- Table structure for tb_log_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_log_user`;
CREATE TABLE `tb_log_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(20) DEFAULT NULL,
  `namakomputer` varchar(50) DEFAULT NULL,
  `browser` text,
  `tanggal` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tb_log_user_ibfk_1` (`username`),
  CONSTRAINT `tb_log_user_ibfk_1` FOREIGN KEY (`username`) REFERENCES `tb_user` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_log_user
-- ----------------------------
INSERT INTO `tb_log_user` VALUES ('1', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-23 16:29:39');
INSERT INTO `tb_log_user` VALUES ('2', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-23 16:57:56');
INSERT INTO `tb_log_user` VALUES ('3', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-24 08:39:40');
INSERT INTO `tb_log_user` VALUES ('4', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-24 13:07:51');
INSERT INTO `tb_log_user` VALUES ('5', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-24 13:08:44');
INSERT INTO `tb_log_user` VALUES ('6', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-24 16:14:14');
INSERT INTO `tb_log_user` VALUES ('7', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-25 07:21:33');
INSERT INTO `tb_log_user` VALUES ('8', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36', '2017-03-27 07:54:39');
INSERT INTO `tb_log_user` VALUES ('9', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-19 08:01:11');
INSERT INTO `tb_log_user` VALUES ('10', 'sumarijan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-19 13:21:01');
INSERT INTO `tb_log_user` VALUES ('11', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-19 13:51:25');
INSERT INTO `tb_log_user` VALUES ('12', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-20 08:14:13');
INSERT INTO `tb_log_user` VALUES ('13', 'sumarijan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-22 08:22:56');
INSERT INTO `tb_log_user` VALUES ('14', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-22 08:43:41');
INSERT INTO `tb_log_user` VALUES ('15', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-22 14:18:39');
INSERT INTO `tb_log_user` VALUES ('16', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-05-31 12:49:36');
INSERT INTO `tb_log_user` VALUES ('17', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-03 08:58:27');
INSERT INTO `tb_log_user` VALUES ('18', 'ilhamdsofyan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-03 09:06:42');
INSERT INTO `tb_log_user` VALUES ('19', 'sumarijan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-03 09:07:20');
INSERT INTO `tb_log_user` VALUES ('20', 'jagung200', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-03 09:07:55');
INSERT INTO `tb_log_user` VALUES ('21', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-03 10:05:01');
INSERT INTO `tb_log_user` VALUES ('22', 'jagung200', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-05 11:51:58');
INSERT INTO `tb_log_user` VALUES ('23', 'sumarijan', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-05 11:52:41');
INSERT INTO `tb_log_user` VALUES ('24', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-06 07:59:03');
INSERT INTO `tb_log_user` VALUES ('25', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-17 07:14:32');
INSERT INTO `tb_log_user` VALUES ('26', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-19 11:38:44');
INSERT INTO `tb_log_user` VALUES ('27', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '2017-06-20 13:15:34');
INSERT INTO `tb_log_user` VALUES ('28', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', '2017-07-04 16:16:29');
INSERT INTO `tb_log_user` VALUES ('29', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', '2017-07-10 10:10:26');
INSERT INTO `tb_log_user` VALUES ('30', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', '2017-07-11 16:19:39');
INSERT INTO `tb_log_user` VALUES ('31', 'superadmin', '::1', 'IT-PC', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', '2017-07-27 07:49:38');
INSERT INTO `tb_log_user` VALUES ('32', 'superadmin', '::1', 'DESKTOP-P85PQHF', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36', '2018-02-22 10:02:34');

-- ----------------------------
-- Table structure for tb_transd
-- ----------------------------
DROP TABLE IF EXISTS `tb_transd`;
CREATE TABLE `tb_transd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NoVoucher` varchar(15) DEFAULT NULL,
  `AccNo` int(11) DEFAULT NULL,
  `debit` int(11) DEFAULT NULL,
  `kredit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tb_transd_ibfk_1` (`NoVoucher`),
  KEY `tb_transd_ibfk_2` (`AccNo`),
  CONSTRAINT `tb_transd_ibfk_1` FOREIGN KEY (`NoVoucher`) REFERENCES `tb_transh` (`NoVoucher`),
  CONSTRAINT `tb_transd_ibfk_2` FOREIGN KEY (`AccNo`) REFERENCES `tb_account` (`AccNo`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_transd
-- ----------------------------
INSERT INTO `tb_transd` VALUES ('1', 'V1705701', '111', '75000000', '0');
INSERT INTO `tb_transd` VALUES ('3', 'V1705702', '511', '1500000', '0');
INSERT INTO `tb_transd` VALUES ('4', 'V1705702', '111', '0', '1500000');
INSERT INTO `tb_transd` VALUES ('5', 'V1705703', '113', '500000', '0');
INSERT INTO `tb_transd` VALUES ('6', 'V1705703', '111', '0', '500000');
INSERT INTO `tb_transd` VALUES ('7', 'V1705704', '121', '12000000', '0');
INSERT INTO `tb_transd` VALUES ('8', 'V1705704', '211', '0', '6500000');
INSERT INTO `tb_transd` VALUES ('9', 'V1705704', '111', '0', '5500000');
INSERT INTO `tb_transd` VALUES ('10', 'V1705705', '111', '3500000', '0');
INSERT INTO `tb_transd` VALUES ('11', 'V1705705', '411', '0', '3500000');
INSERT INTO `tb_transd` VALUES ('12', 'V1705706', '512', '2000000', '0');
INSERT INTO `tb_transd` VALUES ('13', 'V1705706', '111', '0', '2000000');
INSERT INTO `tb_transd` VALUES ('14', 'V1705707', '112', '1500000', '0');
INSERT INTO `tb_transd` VALUES ('15', 'V1705707', '411', '0', '1500000');
INSERT INTO `tb_transd` VALUES ('16', 'V1705708', '513', '250000', '0');
INSERT INTO `tb_transd` VALUES ('17', 'V1705708', '111', '0', '250000');
INSERT INTO `tb_transd` VALUES ('18', 'V1705709', '111', '1000000', '0');
INSERT INTO `tb_transd` VALUES ('19', 'V1705709', '112', '2000000', '0');
INSERT INTO `tb_transd` VALUES ('20', 'V1705709', '411', '0', '3000000');
INSERT INTO `tb_transd` VALUES ('21', 'V1705710', '111', '1500000', '0');
INSERT INTO `tb_transd` VALUES ('22', 'V1705710', '112', '0', '1500000');
INSERT INTO `tb_transd` VALUES ('23', 'V1705711', '312', '1500000', '0');
INSERT INTO `tb_transd` VALUES ('24', 'V1705711', '111', '0', '1500000');
INSERT INTO `tb_transd` VALUES ('25', 'V1705712', '312', '1000000', '0');
INSERT INTO `tb_transd` VALUES ('26', 'V1705712', '111', '0', '1000000');
INSERT INTO `tb_transd` VALUES ('27', 'V1705713', '111', '1750000', '0');
INSERT INTO `tb_transd` VALUES ('28', 'V1705713', '211', '0', '1750000');
INSERT INTO `tb_transd` VALUES ('29', 'V1705714', '514', '500000', '0');
INSERT INTO `tb_transd` VALUES ('30', 'V1705714', '111', '0', '500000');
INSERT INTO `tb_transd` VALUES ('31', 'V1705715', '512', '2000000', '0');
INSERT INTO `tb_transd` VALUES ('32', 'V1705715', '111', '0', '2000000');
INSERT INTO `tb_transd` VALUES ('33', 'V1705716', '111', '11250000', '0');
INSERT INTO `tb_transd` VALUES ('34', 'V1705716', '311', '0', '11250000');
INSERT INTO `tb_transd` VALUES ('35', 'V1705717', '511', '225000', '0');
INSERT INTO `tb_transd` VALUES ('36', 'V1705717', '111', '0', '225000');
INSERT INTO `tb_transd` VALUES ('37', 'V1705718', '113', '75000', '0');
INSERT INTO `tb_transd` VALUES ('38', 'V1705718', '111', '0', '75000');
INSERT INTO `tb_transd` VALUES ('39', 'V1705719', '121', '1800000', '0');
INSERT INTO `tb_transd` VALUES ('40', 'V1705719', '211', '0', '975000');
INSERT INTO `tb_transd` VALUES ('41', 'V1705719', '111', '0', '825000');
INSERT INTO `tb_transd` VALUES ('42', 'V1705720', '111', '525000', '0');
INSERT INTO `tb_transd` VALUES ('43', 'V1705720', '411', '0', '525000');
INSERT INTO `tb_transd` VALUES ('44', 'V1705721', '512', '300000', '0');
INSERT INTO `tb_transd` VALUES ('45', 'V1705721', '111', '0', '300000');
INSERT INTO `tb_transd` VALUES ('46', 'V1705722', '112', '225000', '0');
INSERT INTO `tb_transd` VALUES ('47', 'V1705722', '411', '0', '225000');
INSERT INTO `tb_transd` VALUES ('48', 'V1705723', '513', '37500', '0');
INSERT INTO `tb_transd` VALUES ('49', 'V1705723', '111', '0', '37500');
INSERT INTO `tb_transd` VALUES ('50', 'V1705724', '111', '150000', '0');
INSERT INTO `tb_transd` VALUES ('51', 'V1705724', '112', '300000', '0');
INSERT INTO `tb_transd` VALUES ('52', 'V1705724', '411', '0', '450000');
INSERT INTO `tb_transd` VALUES ('53', 'V1705725', '111', '225000', '0');
INSERT INTO `tb_transd` VALUES ('54', 'V1705725', '112', '0', '225000');
INSERT INTO `tb_transd` VALUES ('55', 'V1705726', '312', '225000', '0');
INSERT INTO `tb_transd` VALUES ('56', 'V1705726', '111', '0', '225000');
INSERT INTO `tb_transd` VALUES ('57', 'V1705727', '312', '150000', '0');
INSERT INTO `tb_transd` VALUES ('58', 'V1705727', '111', '0', '150000');
INSERT INTO `tb_transd` VALUES ('59', 'V1705728', '111', '262500', '0');
INSERT INTO `tb_transd` VALUES ('60', 'V1705728', '211', '0', '262500');
INSERT INTO `tb_transd` VALUES ('61', 'V1705729', '514', '75000', '0');
INSERT INTO `tb_transd` VALUES ('62', 'V1705729', '111', '0', '75000');
INSERT INTO `tb_transd` VALUES ('63', 'V1705729', '512', '300000', '0');
INSERT INTO `tb_transd` VALUES ('64', 'V1705729', '111', '0', '300000');
INSERT INTO `tb_transd` VALUES ('65', 'V1705701', '311', '0', '75000000');

-- ----------------------------
-- Table structure for tb_transh
-- ----------------------------
DROP TABLE IF EXISTS `tb_transh`;
CREATE TABLE `tb_transh` (
  `NoVoucher` varchar(15) NOT NULL,
  `Tanggal` date DEFAULT NULL,
  `Tanggal_input` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deskripsi` text,
  `id_user` int(11) DEFAULT NULL,
  `approve` int(11) DEFAULT '0',
  `id_user_edit` int(11) DEFAULT NULL,
  `Tanggal_update` datetime DEFAULT NULL,
  PRIMARY KEY (`NoVoucher`),
  KEY `tb_transh_ibfk_1` (`id_user`),
  KEY `tb_transh_ibfk_2` (`id_user_edit`),
  CONSTRAINT `tb_transh_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`user_id`),
  CONSTRAINT `tb_transh_ibfk_2` FOREIGN KEY (`id_user_edit`) REFERENCES `tb_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_transh
-- ----------------------------
INSERT INTO `tb_transh` VALUES ('V1705701', '2017-01-01', '2018-02-22 10:14:22', 'Modal awal', '5', '1', '7', '2018-02-22 10:14:22');
INSERT INTO `tb_transh` VALUES ('V1705702', '2017-01-05', '2017-06-20 13:45:38', 'Pembayaran sewa gedung', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705703', '2017-01-08', '2017-05-22 08:30:38', 'Pembelian alat kantor', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705704', '2017-01-10', '2017-06-20 13:39:27', 'Pembelian perlengkapan kantor', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705705', '2017-01-12', '2017-05-22 08:30:38', 'Perbaikan Jalan', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705706', '2017-01-17', '2017-07-27 07:54:50', 'Pembayaran gaji karyawan', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705707', '2017-03-19', '2017-05-22 08:30:38', 'Piutang usaha pada kas', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705708', '2017-03-21', '2017-05-22 08:30:38', 'Pembayaran listrik dan telepon', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705709', '2017-03-23', '2017-05-22 08:30:38', 'Pendapatan jasa perbaikan jalan dibayar sebagian', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705710', '2017-03-24', '2017-05-22 08:30:38', 'Pembayaran Piutang', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705711', '2017-03-25', '2017-05-22 08:30:38', 'Penarikan modal', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705712', '2017-03-14', '2017-05-22 08:30:38', 'Penarikan modal', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705713', '2017-03-28', '2017-05-22 08:30:38', 'Pembayaran hutang', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705714', '2017-03-30', '2017-05-22 08:30:38', 'Pembelian kebutuhan tambahan', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705715', '2017-03-31', '2017-05-22 08:30:38', 'Penggajian karyawan', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705716', '2017-02-01', '2017-05-22 08:30:38', 'Modal dari pemilik', '5', '0', null, null);
INSERT INTO `tb_transh` VALUES ('V1705717', '2017-02-05', '2017-05-22 08:30:38', 'Biaya sewa tempat', '5', '0', null, null);
INSERT INTO `tb_transh` VALUES ('V1705718', '2017-02-08', '2017-05-22 08:30:38', 'Pembelian alat kantor', '5', '0', null, null);
INSERT INTO `tb_transh` VALUES ('V1705719', '2017-02-10', '2017-05-22 08:30:38', 'Pembelian perlengkapan kantor', '5', '0', null, null);
INSERT INTO `tb_transh` VALUES ('V1705720', '2017-02-12', '2017-05-22 08:30:38', 'Pembayaran jasa', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705721', '2017-02-17', '2017-05-22 08:30:38', 'Pembayaran gaji', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705722', '2017-04-19', '2017-05-22 08:30:38', 'Pembayaran piutang jasa', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705723', '2017-04-21', '2017-05-22 08:30:38', 'Pembayaran listrik dan telepon', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705724', '2017-04-23', '2017-05-22 08:30:38', 'Piutang usaha atas jasa', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705725', '2017-04-24', '2017-05-22 08:30:38', 'Pembayaran piutang', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705726', '2017-04-25', '2017-05-22 08:30:38', 'Penarikan pemilik', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705727', '2017-04-14', '2017-05-22 08:30:38', 'Penarikan pemilik', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705728', '2017-04-28', '2017-05-22 08:30:38', 'Hutang dibayar', '5', '1', null, null);
INSERT INTO `tb_transh` VALUES ('V1705729', '2017-04-30', '2017-05-22 08:30:38', 'Beban lain-lain dan beban gaji', '5', '1', null, null);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `realname` varchar(100) DEFAULT NULL,
  `passwd` varchar(64) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `user_image` varchar(250) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` int(11) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telepon` varchar(13) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`username`),
  KEY `tb_user_ibfk_1` (`user_type`),
  KEY `username` (`username`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`user_type`) REFERENCES `tb_user_group` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('3', 'ilhamdsofyan', 'Director', '4497', 'ilhamdsofyan@gmail.com', '4', 'upload/ilhamdsofyan.jpg', 'Jakarta', '1997-04-04', '1', 'Jl. Kalibaru Timur VIIIE No.40', '089615893447', '2017-03-23 11:41:07', '2017-05-22 10:55:24');
INSERT INTO `tb_user` VALUES ('5', 'sumarijan', 'Operator', '123456', 'sumarijan.oce@gmail.com', '2', 'upload/sumarijan.png', 'Jakarta', '1989-12-15', '1', 'Jl. Kali Diem Aja', '089615893123', '2017-03-27 13:20:16', '2017-06-05 11:52:26');
INSERT INTO `tb_user` VALUES ('6', 'jagung200', 'Executive', '123456', 'jugs@yahoo.com', '3', null, 'Jakarta', '1989-12-15', '1', 'Jl. Kali Diem Aja', '089615893123', '2017-03-27 16:41:10', '2017-06-05 11:52:28');
INSERT INTO `tb_user` VALUES ('7', 'superadmin', 'Administrator', 'admin', 'mail@email.com', '1', 'upload/admin.jpg', 'None', '1909-01-01', '1', '-', '021-4417437', '2017-04-07 15:10:43', '2017-05-16 09:31:04');

-- ----------------------------
-- Table structure for tb_user_group
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_group`;
CREATE TABLE `tb_user_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user_group
-- ----------------------------
INSERT INTO `tb_user_group` VALUES ('1', 'Head of Finance', '2017-03-15 00:00:00', '2017-05-17 07:26:40');
INSERT INTO `tb_user_group` VALUES ('2', 'Operator Finance', '2017-03-25 11:55:20', '2017-05-17 07:26:52');
INSERT INTO `tb_user_group` VALUES ('3', 'Excecutive', '2017-03-27 16:40:39', '2017-05-17 07:26:58');
INSERT INTO `tb_user_group` VALUES ('4', 'Board of Director', '2017-05-16 09:25:20', null);
