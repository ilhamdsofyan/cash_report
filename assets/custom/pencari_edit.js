var allValid = true;

function checkPhone (sVariable) {

  var checkOK = "+1234567890 ";
  var checkStr = sVariable;
  allValid = true;
  for (i = 0;  i < checkStr.length;  i++) {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j)) break;

    if (j == checkOK.length) {
      allValid = false;
      break;
    }
  }
}

function checkIllegalchar (sVariable) {

  var checkOK = "<>";
  var checkStr = sVariable;
  allValid = true;
  for (i = 0;  i < checkStr.length;  i++) {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j)) break;

    if (j != checkOK.length) {
      allValid = false;
      break;
    }
  }
}

function submit_onclick() {
	if (!submit_validate()) {
		return false;
	}
}

function submit_validate(){
	if (document.frm.alamat_email.value == "") {
		alert("Mohon isi e-mail Anda");
		frm.alamat_email.focus();
		return false;
	}
    if ((document.frm.alamat_email.value.indexOf('@') == -1) || (document.frm.alamat_email.value.indexOf('.') == -1)) {
        alert ("Mohon isi email Anda dengan format yang benar");
        frm.alamat_email.focus();
		return false;
    }
	
	if (document.frm.nama_depan.value == "") {
		alert("Mohon isi Nama Depan Anda");
		frm.nama_depan.focus();
		return false;
	}
	if (document.frm.nama_depan.value != "") {
		checkIllegalchar(document.frm.nama_depan.value);
		if (!allValid) {
			alert("Mohon isi Nama Depan Anda dengan benar");
			document.frm.nama_depan.select();
			return (false);
		}
	}	
	if (document.frm.nama_tengah.value != "") {
		checkIllegalchar(document.frm.nama_tengah.value);
		if (!allValid) {
			alert("Mohon isi Nama Tengah Anda dengan benar");
			document.frm.nama_tengah.select();
			return (false);
		}
	}	
	if (document.frm.nama_belakang.value != "") {
		checkIllegalchar(document.frm.nama_belakang.value);
		if (!allValid) {
			alert("Mohon isi Nama Belakang Anda dengan benar");
			document.frm.nama_belakang.select();
			return (false);
		}
	}	
	if (document.frm.tgl_lahir.value == "") {
		alert("Mohon isi Tanggal Lahir Anda");
		frm.tgl_lahir.focus();
		return false;
	} 
	if (document.frm.tgl_lahir.value!="") {
		checkPhone(document.frm.tgl_lahir.value);
		if (!allValid) {
			alert("Data tentang Tanggal Lahir harus berupa data angka.");
			document.frm.tgl_lahir.select();
			return (false);
		}
	}	
	if (document.frm.bulan_lahir.value == "") {
		alert("Mohon isi Bulan Lahir Anda");
		frm.bulan_lahir.focus();
		return false;
	} 
	if (document.frm.bulan_lahir.value!="") {
		checkPhone(document.frm.bulan_lahir.value);
		if (!allValid) {
			alert("Data tentang Bulan Lahir harus berupa data angka.");
			document.frm.bulan_lahir.select();
			return (false);
		}
	}	
	if (document.frm.tahun_lahir.value == "") {
		alert("Mohon isi Tahun Lahir Anda");
		frm.tahun_lahir.focus();
		return false;
	} 
	if (document.frm.tahun_lahir.value!="") {
		checkPhone(document.frm.tahun_lahir.value);
		if (!allValid) {
			alert("Data tentang Tahun Lahir harus berupa data angka.");
			document.frm.tahun_lahir.select();
			return (false);
		}
	}	
	if (document.frm.tempat_lahir.value == "") {
		alert("Mohon isi Tempat Lahir Anda");
		frm.tempat_lahir.focus();
		return false;
	}
	if (document.frm.tempat_lahir.value != "") {
		checkIllegalchar(document.frm.tempat_lahir.value);
		if (!allValid) {
			alert("Mohon isi Tempat Lahir Anda dengan benar");
			document.frm.tempat_lahir.select();
			return (false);
		}
	}		
	if (document.frm.kewarganegaraan.value == "") {
		alert("Mohon isi Kewarganegaraan Anda");
		frm.kewarganegaraan.focus();
		return false;
	}
	if (document.frm.kewarganegaraan.value != "") {
		checkIllegalchar(document.frm.kewarganegaraan.value);
		if (!allValid) {
			alert("Mohon isi Kewarganegaraan Anda dengan benar");
			document.frm.kewarganegaraan.select();
			return (false);
		}
	}		
	if (document.frm.jumlah_anak.value!="") {
		checkPhone(document.frm.jumlah_anak.value);
		if (!allValid) {
			alert("Data tentang Jumlah Anak harus berupa data angka.");
			document.frm.jumlah_anak.select();
			return (false);
		}
	}	
	if (document.frm.alamat1.value == "") {
		alert("Mohon isi Alamat Anda");
		frm.alamat1.focus();
		return false;
	}
	if (document.frm.alamat1.value != "") {
		checkIllegalchar(document.frm.alamat1.value);
		if (!allValid) {
			alert("Mohon isi Alamat Anda dengan benar");
			document.frm.alamat1.select();
			return (false);
		}
	}
	if (document.frm.kota.value == "") {
		alert("Mohon isi Kota Anda");
		frm.kota.focus();
		return false;
	}	
	if (document.frm.kota.value != "") {
		checkIllegalchar(document.frm.kota.value);
		if (!allValid) {
			alert("Mohon isi Kota Anda dengan benar");
			document.frm.kota.select();
			return (false);
		}
	}
	if (document.frm.kode_pos.value!="") {
		checkPhone(document.frm.kode_pos.value);
		if (!allValid) {
			alert("Data tentang Kode Pos harus berupa data angka.");
			document.frm.kode_pos.select();
			return (false);
		}
	}		
	if (document.frm.provinsi.value == "") {
		alert("Mohon isi Nama Provinsinya");
		frm.provinsi.focus();
		return false;
	} 
	if (document.frm.telp_rumah.value == "") {
		alert("Mohon isi Nomor Telepon Rumah Anda");
		frm.telp_rumah.focus();
		return false;
	} 
	if (document.frm.telp_rumah.value!="") {
		checkPhone(document.frm.telp_rumah.value);
		if (!allValid) {
			alert("Data tentang Telepon Rumah harus berupa data angka.");
			document.frm.telp_rumah.select();
			return (false);
		}
	}	
	if (document.frm.telp_hp.value!="") {
		checkPhone(document.frm.telp_hp.value);
		if (!allValid) {
			alert("Data tentang Telepon HP harus berupa data angka.");
			document.frm.telp_hp.select();
			return (false);
		}
	}		
	if (document.frm.telp_kantor.value!="") {
		checkPhone(document.frm.telp_kantor.value);
		if (!allValid) {
			alert("Data tentang Telepon Kantor harus berupa data angka.");
			document.frm.telp_kantor.select();
			return (false);
		}
	}		
	if (document.frm.telp_faks.value!="") {
		checkPhone(document.frm.telp_faks.value);
		if (!allValid) {
			alert("Data tentang Telepon Fax harus berupa data angka.");
			document.frm.telp_faks.select();
			return (false);
		}
	}
	if (document.frm.pendidikan.value == "") {
		alert("Mohon isi Pendidikan Tertinggi Anda");
		frm.pendidikan.focus();
		return false;
	}		
	if (document.frm.jurusan.value == "") {
		alert("Mohon isi Jurusan Pendidikan Anda");
		frm.jurusan.focus();
		return false;
	}	
	if (document.frm.kursus.value != "") {
		checkIllegalchar(document.frm.kursus.value);
		if (!allValid) {
			alert("Mohon isi Data Kursus Anda dengan benar");
			document.frm.kursus.select();
			return (false);
		}
	}
	if (document.frm.kesediaan_tgl.value!="") {
		checkPhone(document.frm.kesediaan_tgl.value);
		if (!allValid) {
			alert("Data tentang Kesediaan Tanggal harus berupa data angka.");
			document.frm.kesediaan_tgl.select();
			return (false);
		}
	}		
	if (document.frm.kesediaan_bulan.value!="") {
		checkPhone(document.frm.kesediaan_bulan.value);
		if (!allValid) {
			alert("Data tentang Kesediaan Bulan harus berupa data angka.");
			document.frm.kesediaan_bulan.select();
			return (false);
		}
	}		
	if (document.frm.kesediaan_tahun.value!="") {
		checkPhone(document.frm.kesediaan_tahun.value);
		if (!allValid) {
			alert("Data tentang Kesediaan Tahun harus berupa data angka.");
			document.frm.kesediaan_tahun.select();
			return (false);
		}
	}			
	if (document.frm.kesediaan_hari.value!="") {
		checkPhone(document.frm.kesediaan_hari.value);
		if (!allValid) {
			alert("Data tentang Kesediaan Hari harus berupa data angka.");
			document.frm.kesediaan_hari.select();
			return (false);
		}
	}			
	if (document.frm.resume.value != "") {
		checkIllegalchar(document.frm.resume.value);
		if (!allValid) {
			alert("Mohon isi Resume Anda dengan benar");
			document.frm.resume.select();
			return (false);
		}
	}							
	if (document.frm.pendidikan1_mulai_bln.value!="") {
		checkPhone(document.frm.pendidikan1_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan1_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan1_mulai_thn.value!="") {
		checkPhone(document.frm.pendidikan1_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan1_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan1_sampai_bln.value!="") {
		checkPhone(document.frm.pendidikan1_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan1_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan1_mulai_thn.value!="") {
		checkPhone(document.frm.pendidikan1_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan1_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan1_gelar.value != "") {
		checkIllegalchar(document.frm.pendidikan1_gelar.value);
		if (!allValid) {
			alert("Mohon isi Gelar Anda dengan benar");
			document.frm.pendidikan1_gelar.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan1_instansi.value != "") {
		checkIllegalchar(document.frm.pendidikan1_instansi.value);
		if (!allValid) {
			alert("Mohon isi Instansi Anda dengan benar");
			document.frm.pendidikan1_instansi.select();
			return (false);
		}
	}								
	if (document.frm.pendidikan2_mulai_bln.value!="") {
		checkPhone(document.frm.pendidikan2_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan2_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan2_mulai_thn.value!="") {
		checkPhone(document.frm.pendidikan2_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan2_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan2_sampai_bln.value!="") {
		checkPhone(document.frm.pendidikan2_sampai_bln.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan2_sampai_bln.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan2_sampai_thn.value!="") {
		checkPhone(document.frm.pendidikan2_sampai_thn.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan2_sampai_thn.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan2_gelar.value != "") {
		checkIllegalchar(document.frm.pendidikan2_gelar.value);
		if (!allValid) {
			alert("Mohon isi Gelar Anda dengan benar");
			document.frm.pendidikan2_gelar.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan2_instansi.value != "") {
		checkIllegalchar(document.frm.pendidikan2_instansi.value);
		if (!allValid) {
			alert("Mohon isi Instansi Anda dengan benar");
			document.frm.pendidikan2_instansi.select();
			return (false);
		}
	}								
	if (document.frm.pendidikan3_mulai_bln.value!="") {
		checkPhone(document.frm.pendidikan3_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan3_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan3_mulai_thn.value!="") {
		checkPhone(document.frm.pendidikan3_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan3_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan3_sampai_bln.value!="") {
		checkPhone(document.frm.pendidikan3_sampai_bln.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan3_sampai_bln.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan3_sampai_thn.value!="") {
		checkPhone(document.frm.pendidikan3_sampai_thn.value);
		if (!allValid) {
			alert("Data tentang Latar Belakang Pendidikan harus berupa data angka.");
			document.frm.pendidikan3_sampai_thn.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan3_gelar.value != "") {
		checkIllegalchar(document.frm.pendidikan3_gelar.value);
		if (!allValid) {
			alert("Mohon isi Gelar Anda dengan benar");
			document.frm.pendidikan3_gelar.select();
			return (false);
		}
	}		
	if (document.frm.pendidikan3_instansi.value != "") {
		checkIllegalchar(document.frm.pendidikan3_instansi.value);
		if (!allValid) {
			alert("Mohon isi Instansi Anda dengan benar");
			document.frm.pendidikan3_instansi.select();
			return (false);
		}
	}								
	if (document.frm.pengalaman_kerja_thn.value!="") {
		checkPhone(document.frm.pengalaman_kerja_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja tahun harus berupa data angka.");
			document.frm.pengalaman_kerja_thn.select();
			return (false);
		}
	}			
	if (document.frm.pengalaman_kerja_bln.value!="") {
		checkPhone(document.frm.pengalaman_kerja_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja bulan harus berupa data angka.");
			document.frm.pengalaman_kerja_bln.select();
			return (false);
		}
	}		
	if (document.frm.jumlah_pekerjaan.value!="") {
		checkPhone(document.frm.jumlah_pekerjaan.value);
		if (!allValid) {
			alert("Data tentang Jumlah Pekerjaan Sebelumnya harus berupa data angka.");
			document.frm.jumlah_pekerjaan.select();
			return (false);
		}
	}			
	if (document.frm.posisi_yang_diminati.value != "") {
		checkIllegalchar(document.frm.posisi_yang_diminati.value);
		if (!allValid) {
			alert("Mohon isi Posisi yang diminati Anda dengan benar");
			document.frm.posisi_yang_diminati.select();
			return (false);
		}
	}
	if (document.frm.pengalaman_kerja.value != "") {
		checkIllegalchar(document.frm.pengalaman_kerja.value);
		if (!allValid) {
			alert("Mohon isi Posisi yang diminati Anda dengan benar");
			document.frm.pengalaman_kerja.select();
			return (false);
		}
	}
	if (document.frm.kerja1_mulai_bln.value!="") {
		checkPhone(document.frm.kerja1_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (bulan) harus berupa data angka.");
			document.frm.kerja1_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.kerja1_mulai_thn.value!="") {
		checkPhone(document.frm.kerja1_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (tahun) harus berupa data angka.");
			document.frm.kerja1_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.kerja1_sampai_bln.value!="") {
		checkPhone(document.frm.kerja1_sampai_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (bulan) harus berupa data angka.");
			document.frm.kerja1_sampai_bln.select();
			return (false);
		}
	}		
	if (document.frm.kerja1_sampai_thn.value!="") {
		checkPhone(document.frm.kerja1_sampai_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (tahun) harus berupa data angka.");
			document.frm.kerja1_sampai_thn.select();
			return (false);
		}
	}
	if (document.frm.kerja1.value != "") {
		checkIllegalchar(document.frm.kerja1.value);
		if (!allValid) {
			alert("Mohon isi Titel Pekerjaan dengan benar");
			document.frm.kerja1.select();
			return (false);
		}
	}
	if (document.frm.kerja1_gaji.value != "") {
		checkIllegalchar(document.frm.kerja1_gaji.value);
		if (!allValid) {
			alert("Mohon isi Gaji dengan benar");
			document.frm.kerja1_gaji.select();
			return (false);
		}
	}
	if (document.frm.kerja2_mulai_bln.value!="") {
		checkPhone(document.frm.kerja2_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (bulan) harus berupa data angka.");
			document.frm.kerja2_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.kerja2_mulai_thn.value!="") {
		checkPhone(document.frm.kerja2_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (tahun) harus berupa data angka.");
			document.frm.kerja2_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.kerja2_sampai_bln.value!="") {
		checkPhone(document.frm.kerja2_sampai_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (bulan) harus berupa data angka.");
			document.frm.kerja2_sampai_bln.select();
			return (false);
		}
	}		
	if (document.frm.kerja2_sampai_thn.value!="") {
		checkPhone(document.frm.kerja2_sampai_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (tahun) harus berupa data angka.");
			document.frm.kerja2_sampai_thn.select();
			return (false);
		}
	}		
	if (document.frm.kerja2.value != "") {
		checkIllegalchar(document.frm.kerja2.value);
		if (!allValid) {
			alert("Mohon isi Titel Pekerjaan dengan benar");
			document.frm.kerja2.select();
			return (false);
		}
	}
	if (document.frm.kerja2_gaji.value != "") {
		checkIllegalchar(document.frm.kerja2_gaji.value);
		if (!allValid) {
			alert("Mohon isi Gaji dengan benar");
			document.frm.kerja2_gaji.select();
			return (false);
		}
	}
	if (document.frm.kerja3_mulai_bln.value!="") {
		checkPhone(document.frm.kerja3_mulai_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (bulan) harus berupa data angka.");
			document.frm.kerja3_mulai_bln.select();
			return (false);
		}
	}		
	if (document.frm.kerja3_mulai_thn.value!="") {
		checkPhone(document.frm.kerja3_mulai_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (tahun) harus berupa data angka.");
			document.frm.kerja3_mulai_thn.select();
			return (false);
		}
	}		
	if (document.frm.kerja3_sampai_bln.value!="") {
		checkPhone(document.frm.kerja3_sampai_bln.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (bulan) harus berupa data angka.");
			document.frm.kerja3_sampai_bln.select();
			return (false);
		}
	}		
	if (document.frm.kerja3_sampai_thn.value!="") {
		checkPhone(document.frm.kerja3_sampai_thn.value);
		if (!allValid) {
			alert("Data tentang Pengalaman Kerja Sebelumnya (tahun) harus berupa data angka.");
			document.frm.kerja3_sampai_thn.select();
			return (false);
		}
	}		
	if (document.frm.kerja3.value != "") {
		checkIllegalchar(document.frm.kerja3.value);
		if (!allValid) {
			alert("Mohon isi Titel Pekerjaan dengan benar");
			document.frm.kerja3.select();
			return (false);
		}
	}
	if (document.frm.kerja3_gaji.value != "") {
		checkIllegalchar(document.frm.kerja3_gaji.value);
		if (!allValid) {
			alert("Mohon isi Gaji dengan benar");
			document.frm.kerja3_gaji.select();
			return (false);
		}
	}

	return true
}
