$(document).ready(function(){ 
	$(document).bind("contextmenu cut copy paste",function(e){
		toastr.info('Maaf. Fitur klik kanan, Copy, Cut, serta Paste tidak diperkenankan pada aplikasi ini', 'Notifikasi', 
					{
						timeOut: 40000,
						closeButton: true,
						showMethod: "slideDown",
						hideMethod: "slideUp",
						positionClass: "toast-bottom-left"
					})
		return false;
	}); 
	$(document).keydown(function(e){
		// alert(e.keyCode);
		if (e.ctrlKey && e.keyCode == 117 ){
			toastr.info('Maaf, anda tidak bisa melihat source', 'Notifikasi', 
						{
							timeOut: 40000,
							closeButton: true,
							showMethod: "slideDown",
							hideMethod: "slideUp",
							positionClass: "toast-bottom-left"
						})
			return false;
		}
	});
	$(document).keydown(function(event){
		if(event.keyCode == 123){
			toastr.info('Maaf, anda tidak bisa melihat source', 'Notifikasi', 
						{
							timeOut: 40000,
							closeButton: true,
							showMethod: "slideDown",
							hideMethod: "slideUp",
							positionClass: "toast-bottom-left"
						})
			return false;
			// return false;
		}
		else if (event.ctrlKey && event.shiftKey && event.keyCode==73){        
			toastr.info('Maaf, anda tidak bisa melihat source', 'Notifikasi', 
						{
							timeOut: 40000,
							closeButton: true,
							showMethod: "slideDown",
							hideMethod: "slideUp",
							positionClass: "toast-bottom-left"
						})
			return false;
		}
	});
});
