# CASH REPORT #

Fasilitas ini diperuntukkan untuk divisi keuangan dan Board of Director. Adapun fasilitas yang terdapat di dalam cash report ini sebagai berikut :

* Input Data Keuangan
* Pembuatan Buku Besar
* Pembuatan Neraca Saldo
* Menentukan Laba/Rugi
* Pembuatan Neraca

Untuk mendapatkan fasilitas diatas anda harus login terlebih dahulu sesuai dengan user id dan sandinya masing-masing.
Bagi yang tidak bisa login kedalam cash report ini, di harapkan untuk melapor pada Administrator masing-masing untuk didaftarkan akunnya.