<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->username = $this->session->userdata('username');
		$this->biodata = $this->mainmodel->getUserData($this->username);
		$this->load->model('M_Transaction', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->biodata;
		if ($this->biodata['user_type'] != 1) {
			$param = array(
					'tb_transh.id_user' => $this->biodata['user_id'],
					'tb_transh.approve' => 0
					);
			$transaction = $this->model->getTransH($param, 'tb_transh.NoVoucher', 'tb_transh.Tanggal', 'asc');
		}
		else{
			$transaction = $this->model->getTransH(null, 'tb_transh.NoVoucher', 'tb_transh.Tanggal', 'asc');
		}

		$data['transaction'] = (!is_array($transaction)) ? array() : $transaction;

		$data['content_title'] = 'Transaksi';
		$data['content_file'] = 'content/transaksi';
		$data['content_css'] = 'content/css/transaksi';
		$data['content_js'] = 'content/js/transaksi';

		$this->load->view('template/index', $data);
	}

	public function getTransaction()
	{
		$param['tb_transh.NoVoucher'] = $this->input->post('NoVoucher');

		$header = $this->model->getTransH($param, null, 'single');
		$detail[] = $this->model->getTransD($param);
		$data = array_merge($header, $detail);

		echo json_encode($data);
	}

	public function approval()
	{
		$param = array('NoVoucher' => $this->input->post('NoVoucher'));
		$data = array('approve' => $this->input->post('approve'));

		$approval = $this->model->putApproval($param, $data);
		if ($approval) {
			if ($this->input->post('approve') == 1) {
				$result = array(
						'status' => 'success',
						'result' => 'Batal Setujui',
						'message' => 'Transaksi dengan nomor voucher '.$this->input->post('NoVoucher').' telah disetujui'
						);
			}
			else {
				$result = array(
						'status' => 'success',
						'result' => 'Setujui',
						'message' => 'Transaksi dengan nomor voucher '.$this->input->post('NoVoucher').' tidak disetujui'
						);
				}
		}

		echo json_encode($result);
	}

	public function addTransaction()
	{
		$data['biodata'] = $this->biodata;
		$data['lastVoucher'] = $this->model->getVoucher(array('NoVoucher'), 'NoVoucher', 'single');
		// $data['prodi'] = $this->model->getProdi();

		$data['content_title'] = 'Transaksi';
		$data['content_subtitle'] = 'Input Data Keuangan';
		$data['content_file'] = 'content/transaksi_tambah';
		$data['content_css'] = 'content/css/transaksi_tambah';
		$data['content_js'] = 'content/js/transaksi_tambah';

		$this->load->view('template/index', $data);
	}

	public function getAccount()
	{
		$param = trim(strip_tags($_GET['term']));
		$account = $this->model->getAccounts($param);
		foreach ($account as $key => $value) {
			$data['label']= htmlentities(stripslashes($value['AccNo'] . ' - ' . $value['AccName']));
		    $data['value']= $value['AccName'];
		    $data['id']= (int) $value['AccNo'];
			//buat array yang nantinya akan di konversi ke json
		    $row_set[] = $data;
		}
		echo json_encode($row_set);
	}

	public function saveTransaction()
	{
		$header = $this->input->post('header');
		$detail = $this->input->post('detail');

		// simpan header
		$header['id_user'] = $this->biodata['user_id'];
		$header['Tanggal'] = $header['Tanggal'] . ' ' . date("H:i:s");
		$header['Tanggal_input'] = date("Ymdhis");
		$header['approve'] = 0;
		if (!empty($detail)) {
			$saveHeader = $this->model->storeHeader($header);
			if ($saveHeader) {
				foreach ($detail as $key => $value) {
					$value['NoVoucher'] = $header['NoVoucher'];
					// $value['approve'] = 0;
					$saveDetail = $this->model->storeDetail($value);
					if ($saveDetail) {
						$param = array('AccNo' => $value['AccNo']);
					}
					else {
						$dropHeader = $this->model->dropHeader($header['NoVoucher']);
					}
				}
				$this->session->set_flashdata('message', 'Data berhasil di simpan');
			}
			else {
				$this->session->set_flashdata('message', 'Data gagal di simpan, silahkan periksa input ');
			}
		}
		else {
			$this->session->set_flashdata('message', 'Data gagal disimpan, anda tidak melakukan transaksi');
		}

		redirect('transaksi','refresh');
	}

	public function editTransaction($noVoucher)
	{
		$noVoucher = base64_decode($noVoucher);
		$data['biodata'] = $this->biodata;
		$data['header'] = $this->model->getTransH(array('tb_transh.NoVoucher' => $noVoucher), null, 'single');
		$data['detail'] = $this->model->getTransD(array('tb_transh.NoVoucher' => $noVoucher));

		$data['content_title'] = 'Transaksi';
		$data['content_subtitle'] = 'Ubah Data Keuangan';
		$data['content_file'] = 'content/transaksi_ubah';
		$data['content_css'] = 'content/css/transaksi_ubah';
		$data['content_js'] = 'content/js/transaksi_ubah';

		$this->load->view('template/index', $data);
	}

	public function updateTransaction()
	{
		$param = $this->input->post('param');
		$data = $this->input->post('detail');
		
		$editor['id_user_edit'] = $this->biodata['user_id'];
		$editor['Tanggal_update'] = date("Ymdhis");
		// saving header log update
		$this->model->putApproval(array('NoVoucher' => $param['NoVoucher']), $editor);

		$update = $this->model->storeDetail($data, 'update', $param);
		if ($update) {
			$result = array('status' => 'success');
		}
		else{
			$result = array('status' => 'error');
		}
		echo json_encode($result);
	}

	public function addNewDetail()
	{
		$data = $this->input->post('detail');
		$save = $this->model->storeDetail($data);
		
		$editor['id_user_edit'] = $this->biodata['user_id'];
		$editor['Tanggal_update'] = date("Ymdhis");
		// saving header log update
		$this->model->putApproval(array('NoVoucher' => $data['NoVoucher']), $editor);

		$encrypted_id = base64_encode($data['NoVoucher']);
		// die();
		if ($save) {
			$this->session->set_flashdata('message', 'Data berhasil di tambah');
		}
		else{
			$this->session->set_flashdata('message', 'Data gagal di tambah');
		}

		redirect('transaksi/ubah/'. $encrypted_id .'','refresh');
	}

	public function deleteDetail()
	{
		$param = $this->input->post('param');
		$AccNo = $this->input->post('AccNo');
		// $header = $this->model->getTransH(array('tb_transh.NoVoucher' => $param['NoVoucher']));

		$delete = $this->model->dropDetail($param);
		if ($delete) {
			$result = array('status' => 'success');
		}
		else {
			$result = array('status' => 'success');
		}

		echo json_encode($result);
	}

}

/* End of file Transaction.php */
/* Location: ./application/controllers/Transaction.php */