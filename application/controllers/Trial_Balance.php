<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trial_Balance extends CI_Controller {

	private $monthID;

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null ) {
			redirect('auth/unauthorized','refresh');
		}

		$this->monthID = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

		$this->username = $this->session->userdata('username');
		$this->load->model('M_NeracaSaldo', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['year'] = $this->model->getYear();

		$data['content_title'] = 'Laporan';
		$data['content_subtitle'] = 'Neraca Saldo';
		$data['content_file'] = 'content/neraca_saldo';
		$data['content_css'] = 'content/css/neraca_saldo';
		$data['content_js'] = 'content/js/neraca_saldo';

		$this->load->view('template/index', $data);
	}

	public function getMonth()
	{
		$year = $this->input->post('tahun');
		$month = $this->model->getMonth($year);

		echo json_encode($month);
	}

	// public function getAccount()
	// {
	// 	$post = $this->input->post();
	// 	$param = array(
	// 			'YEAR(tb_transh.Tanggal)' => $post['tahun'],
	// 			'MONTH(tb_transh.Tanggal)' => $post['bulan'],
	// 			);
	// 	$account = $this->model->getAccount($param, null, 'tb_transd.AccNo');

	// 	echo json_encode($account);
	// }

	public function showTrialBalance()
	{
		$period = $this->input->post('periode');
		$setAccount = $this->model->getAccount(null, 'AccNo, AccName', false);
		
		if (empty($setAccount)) {
			$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-center">';
							$view .= '<h2>Data tidak tersedia.</h2>';
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';
		}
		else {
			foreach ($setAccount as $key => $value) {
				$getTrialBalance[] = $this->model->trialBalanceNow($period, $value['AccNo']);
				if (empty($getTrialBalance[$key])) {
					$getTrialBalance[$key]['AccNo'] = $value['AccNo'];
					$getTrialBalance[$key]['AccName'] = $value['AccName'];
					$getTrialBalance[$key]['debit'] = 0;
					$getTrialBalance[$key]['kredit'] = 0;
					$getTrialBalance[$key]['saldo_debit'] = 0;
					$getTrialBalance[$key]['saldo_kredit'] = 0;
				}
			}
			
			$totalDebitTrans = 0;
			$totalKreditTrans = 0;
			$totalDebitSaldo = 0;
			$totalKreditSaldo = 0;
			$totalDebitAwal = 0;
			$totalKreditAwal = 0;

			$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-right">';
							$view .= '<button class="btn btn-info" id="btn-print"><i class="fa fa-print"></i>&nbsp;Cetak</button>';
						$view .= '</div>';
					$view .= '</div>';
					$view .= '<div class="row" id="content-print">';
						$view .= '<div class="col-lg-12">';
							$view .= '<h3 class="text-center">Laporan Neraca Saldo <br> Periode Bulan '. $this->monthID[$period['bulan']] . ' ' . $period['tahun'] .'</h3>';
								$view .= '<div class="table-responsive">';
									$view .= '<table border="1" class="table">';
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th rowspan="2" style="vertical-align:middle">Nomor Akun</th>';
											$view .= '<th rowspan="2" style="vertical-align:middle">Nama Akun</th>';
											$view .= '<th colspan="2" style="text-align:center">Saldo Awal</th>';
											$view .= '<th colspan="2" style="text-align:center">Transaksi</th>';
											$view .= '<th colspan="2" style="text-align:center">Saldo Akhir</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th>Debit</th>';
											$view .= '<th>Kredit</th>';
											$view .= '<th>Debit</th>';
											$view .= '<th>Kredit</th>';
											$view .= '<th>Debit</th>';
											$view .= '<th>Kredit</th>';
										$view .= '</tr>';

											foreach ($getTrialBalance as $key => $value) {
												// print_r($value);die();
												$getTrialBalance2 = $this->model->trialBalanceRunning($period, $value['AccNo']);
												$currency = $getTrialBalance2['saldo_debit_all'] - $getTrialBalance2['saldo_kredit_all'];
												if ($currency > 0) {
													$debitStart = $currency;
													$kreditStart = 0;
												}
												else {
													$kreditStart = $currency * -1;
													$debitStart = 0;
												}
												$view .= '<tr>';
													$view .= '<td>'. $value['AccNo'] .'</td>';
													$view .= '<td>'. $value['AccName'] .'</td>';
													$view .= '<td>'. number_format($getTrialBalance2['saldo_debit_all']) .'</td>';
													$view .= '<td>'. number_format($getTrialBalance2['saldo_kredit_all']) .'</td>';
													$view .= '<td>'. number_format($value['debit']) .'</td>';
													$view .= '<td>'. number_format($value['kredit']) .'</td>';
													$view .= '<td>'. number_format(($value['saldo_debit'] + $debitStart)) .'</td>';
													$view .= '<td>'. number_format(($value['saldo_kredit'] + $kreditStart)) .'</td>';
												$view .= '</tr>';

												$totalDebitTrans += $value['debit'];
												$totalKreditTrans += $value['kredit'];
												$totalDebitSaldo += ($value['saldo_debit'] + $debitStart);
												$totalKreditSaldo += ($value['saldo_kredit'] + $kreditStart);
												$totalDebitAwal += $getTrialBalance2['saldo_debit_all'];
												$totalKreditAwal += $getTrialBalance2['saldo_kredit_all'];
											}
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th colspan="2" style="text-align:center">Total</th>';
											$view .= '<th>'. number_format($totalDebitAwal) .'</th>';
											$view .= '<th>'. number_format($totalKreditAwal) .'</th>';
											$view .= '<th>'. number_format($totalDebitTrans) .'</th>';
											$view .= '<th>'. number_format($totalKreditTrans) .'</th>';
											$view .= '<th>'. number_format($totalDebitSaldo) .'</th>';
											$view .= '<th>'. number_format($totalKreditSaldo) .'</th>';
										$view .= '</tr>';
									$view .= '</table>';
								$view .= '</div>';
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';
		}

		echo $view;
	}

}

/* End of file Trial_Balance.php */
/* Location: ./application/controllers/Trial_Balance.php */