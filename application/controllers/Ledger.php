<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ledger extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->username = $this->session->userdata('username');
		$this->load->model('M_Ledger','model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);

		$data['content_title'] = 'Laporan';
		$data['content_subtitle'] = 'Buku Besar';
		$data['content_file'] = 'content/buku_besar';
		$data['content_css'] = 'content/css/buku_besar';
		$data['content_js'] = 'content/js/buku_besar';

		$this->load->view('template/index', $data);
	}

	public function getAccount()
	{
		$date = $this->input->post('range');
		// print_r($year);die();
		$getAccount = $this->model->getTrans(array('DATE(tb_transh.Tanggal) >=' => $date['start'], 'DATE(tb_transh.Tanggal) <=' => $date['end'], ), 'tb_transd.AccNo, tb_account.AccName', 'tb_transd.AccNo');

		echo json_encode($getAccount);
	}

	public function showLedger()
	{
		$accno = $this->input->post('accno');
		$date = $this->input->post('range');
		// print_r($this->input->post());die();
		if ($accno != 'semua') {
			$param = array(
					'tb_account.AccNo' => $accno,
					'DATE(tb_transh.Tanggal) >= ' => $date['start'],
					'DATE(tb_transh.Tanggal) <= ' => $date['end']
					);
		}
		else {
			$param = array(
					'DATE(tb_transh.Tanggal) >= ' => $date['start'],
					'DATE(tb_transh.Tanggal) <= ' => $date['end']
					);
		}
		$getAccount = $this->model->getAccount($param, 'tb_account.AccNo');
		if (empty($getAccount)) {
			$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-center">';
							$view .= '<h2>Data tidak tersedia.</h2>';
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';
		}
		else {
			$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-right">';
							$view .= '<button class="btn btn-info" id="btn-print"><i class="fa fa-print"></i>&nbsp;Cetak</button>';
						$view .= '</div>';
					$view .= '</div>';
					$view .= '<div class="row" id="content-print">';
						$view .= '<div class="col-lg-12">';
							$view .= '<h3 class="text-center">Laporan Buku Besar <br>'. $this->mainmodel->format_date_ind($date['start'], 'long') . ' - ' . $this->mainmodel->format_date_ind($date['end'], 'long') .'</h3>';
							foreach ($getAccount as $key => $value1) {
								$saldoDebit = 0;
								$totalSaldoDebit = 0;
								$saldoKredit = 0;
								$totalSaldoKredit = 0;
								$totalDebit = 0;
								$totalKredit = 0;
								$data['start'] = $date['start'];
								$data['end'] = $date['end'];
								$data['AccNo'] = $value1['AccNo'];

								$getLedger = $this->model->getLedger($data);
								if ($data['start'] > date('Y-01-01')) {
									$getRecentLedger = $this->getRecentLedger($data['AccNo'], $data['start']);
									$getLedger = array_merge($getRecentLedger, $getLedger);
								}

								$view .= '<div>';
									$view .= '<span class="pull-left">';
										$view .= '<strong>' . $value1['AccName'] . '</strong>';
									$view .= '</span>';
									$view .= '<span class="pull-right">';
										$view .= '<strong>' . $value1['AccNo'] . '</strong>';
									$view .= '</span>';
									$view .= '<div class="clearfix"></div>';
								$view .= '</div>';
								$view .= '<div class="table-responsive">';
									$view .= '<table border="1" class="table">';
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th colspan="2" rowspan="2" style="vertical-align:middle">Tanggal</th>';
											$view .= '<th rowspan="2" style="vertical-align:middle">Keterangan</th>';
											$view .= '<th rowspan="2" style="vertical-align:middle">Debit</th>';
											$view .= '<th rowspan="2" style="vertical-align:middle">Kredit</th>';
											$view .= '<th colspan="2" style="text-align:center">Saldo</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th>Debit</th>';
											$view .= '<th>Kredit</th>';
										$view .= '</tr>';
										// if ($data['AccNo'] != '') {
											$i = 1;
											foreach ($getLedger as $key => $value) {
												$bulan_tahun = ($value['Tanggal'] != '') ? date('M-Y', strtotime($value['Tanggal'])) : '-';
												$tanggal = ($value['Tanggal'] != '') ? date('d', strtotime($value['Tanggal'])) : '-';
												if ($key > 0) {
													if ($totalSaldoDebit > $value['kredit']) {
														$saldoDebit = ($saldoDebit + $value['debit']) - $value['kredit'];
														$saldoKredit = 0;
														if ($saldoDebit < 0) {
															$saldoKredit = $saldoDebit * -1;
															$saldoDebit = 0;
														}
													}
													else {
														$saldoKredit = ($saldoKredit + $value['kredit']) - $value['debit'];
														$saldoDebit = 0;
														if ($saldoKredit < 0) {
															$saldoDebit = $saldoKredit * -1;
															$saldoKredit = 0;
														}
													}
												}
												else {
													if ($value['debit'] > $value['kredit']) {
														$saldoDebit = $value['debit'] - $value['kredit'];
														$saldoKredit = 0;
													}
													else {
														$saldoKredit = $value['kredit'] - $value['debit'];
														$saldoDebit = 0;
													}
												}

												$view .= '<tr>';
													$view .= '<td>'. $bulan_tahun .'</td>';
													$view .= '<td>'. $tanggal .'</td>';
													$view .= '<td>'. $value['deskripsi'] .'</td>';
													$view .= '<td>'. number_format($value['debit']) .'</td>';
													$view .= '<td>'. number_format($value['kredit']) .'</td>';
													$view .= '<td>'. number_format($saldoDebit) .'</td>';
													$view .= '<td>'. number_format($saldoKredit) .'</td>';
												$view .= '</tr>';
												
												$totalDebit += $value['debit'];
												$totalKredit += $value['kredit'];
												$totalSaldoDebit += $saldoDebit;
												$totalSaldoKredit += $saldoKredit;
												
												$i++;
											}
										// $view .= '<tr>';
										// 	$view .= '<th colspan="3">Total</th>';
										// 	$view .= '<th>'. number_format($totalDebit) .'</th>';
										// 	$view .= '<th>'. number_format($totalKredit) .'</th>';
										// 	$view .= '<th>'. number_format($totalSaldoDebit) .'</th>';
										// 	$view .= '<th>'. number_format($totalSaldoKredit) .'</th>';
										// $view .= '</tr>';
									$view .= '</table>';
								$view .= '</div>';
							}
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';
		}

		echo $view;
	}

	public function getRecentLedger($account='', $endDate='')
	{
		$saldoDebit = 0;
		$totalSaldoDebit = 0;
		$saldoKredit = 0;
		$totalSaldoKredit = 0;
		$totalDebit = 0;
		$totalKredit = 0;

		$data['start'] = date('Y-01-01');
		$data['end'] = date('Y-m-d', strtotime($endDate. '-1 day'));
		$data['AccNo'] = $account;
		$getLedger = $this->model->getLedger($data);
		$i = 1;
		foreach ($getLedger as $key => $value) {
			if ($key > 0) {
				if ($totalSaldoDebit > $value['kredit']) {
					$saldoDebit = ($saldoDebit + $value['debit']) - $value['kredit'];
					$saldoKredit = 0;
					if ($saldoDebit < 0) {
						$saldoKredit = $saldoDebit * -1;
						$saldoDebit = 0;
					}
				}
				else {
					$saldoKredit = ($saldoKredit + $value['kredit']) - $value['debit'];
					$saldoDebit = 0;
					if ($saldoKredit < 0) {
						$saldoDebit = $saldoKredit * -1;
						$saldoKredit = 0;
					}
				}
			}
			else {
				if ($value['debit'] > $value['kredit']) {
					$saldoDebit = $value['debit'] - $value['kredit'];
					$saldoKredit = 0;
				}
				else {
					$saldoKredit = $value['kredit'] - $value['debit'];
					$saldoDebit = 0;
				}
			}

			$totalDebit += $value['debit'];
			$totalKredit += $value['kredit'];
			$totalSaldoDebit += $saldoDebit;
			$totalSaldoKredit += $saldoKredit;
			
			$i++;
		}

		if ($saldoKredit == 0 && $saldoDebit == 0) {
			$ret = array();
		}
		else {
			$ret[] = array(
					'Tanggal' => '',
					'deskripsi' => 'Saldo hingga tanggal '. $endDate .'',
					'kredit' => $saldoKredit,
					'debit' => $saldoDebit
					);
		}
		return $ret;
	}

}

/* End of file Ledger.php */
/* Location: ./application/controllers/Ledger.php */