<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}
		$this->load->model('M_Journal', 'model');

		$this->username = $this->session->userdata('username');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);

		$data['content_title'] = 'Laporan';
		$data['content_subtitle'] = 'Jurnal Umum';
		$data['content_file'] = 'content/buku_jurnal';
		$data['content_css'] = 'content/css/buku_jurnal';
		$data['content_js'] = 'content/js/buku_jurnal';

		$this->load->view('template/index', $data);
	}

	public function showJournal()
	{
		// $accno = $this->input->post('accno');
		$date = $this->input->post('range');

		$data['start'] = $date['start'];
		$data['end'] = $date['end'];
		// $data['AccNo'] = $accno;
		$getJournal = $this->model->getJournal($data);

		$totalDebit = 0;
		$totalKredit = 0;

			$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-right">';
							$view .= '<button class="btn btn-info" id="btn-print"><i class="fa fa-print"></i>&nbsp;Cetak</button>';
						$view .= '</div>';
					$view .= '</div>';
					$view .= '<div class="row" id="content-print">';
						$view .= '<div class="col-lg-12">';
							$view .= '<h3 class="text-center">Laporan Jurnal Umum <br>'. $this->mainmodel->format_date_ind($date['start'], 'long') . ' - ' . $this->mainmodel->format_date_ind($date['end'], 'long') .'</h3>';
								$view .= '<div class="table-responsive">';
									$view .= '<table border="1" class="table">';
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th colspan="2" rowspan="2" style="vertical-align:middle">Tanggal</th>';
											$view .= '<th rowspan="2" style="vertical-align:middle">Akun</th>';
											$view .= '<th rowspan="2" style="vertical-align:middle">Keterangan</th>';
											$view .= '<th colspan="2"  style="text-align:center">Saldo</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:grey;color:white;">';
											$view .= '<th>Debit</th>';
											$view .= '<th>Kredit</th>';
										$view .= '</tr>';
											$i = 1;
											if (empty($getJournal)) {
												$view .= '<tr>';
													$view .= '<td colspan="6"><i>Maaf, data tidak tersedia.</i></td>';
												$view .= '</tr>';
											}
											else {
												foreach ($getJournal as $key => $value) {
													$tanggal = date_create($value['Tanggal']);

													$view .= '<tr>';
														$view .= '<td>'. date_format($tanggal, 'M - Y') .'</td>';
														$view .= '<td>'. date_format($tanggal, 'd') .'</td>';
														$view .= '<td>'. $value['AccNo'] . ' - ' . $value['AccName'] .'</td>';
														$view .= '<td>'. $value['deskripsi'] .'</td>';
														$view .= '<td>'. number_format($value['debit']) .'</td>';
														$view .= '<td>'. number_format($value['kredit']) .'</td>';
													$view .= '</tr>';
													
													$totalDebit += $value['debit'];
													$totalKredit += $value['kredit'];
													
													$i++;
												}
												$view .= '<tr>';
													$view .= '<th colspan="4">Total</th>';
													$view .= '<th>'. number_format($totalDebit) .'</th>';
													$view .= '<th>'. number_format($totalKredit) .'</th>';
												$view .= '</tr>';
											}
									$view .= '</table>';
								$view .= '</div>';
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';

		echo $view;
	}

}

/* End of file Journal.php */
/* Location: ./application/controllers/Journal.php */