<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->username = $this->session->userdata('username');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);

		$data['content_title'] = 'Laporan';
		$data['content_file'] = 'content/laporan';
		$data['content_css'] = 'content/css/laporan';
		$data['content_js'] = 'content/js/laporan';

		$this->load->view('template/index', $data);
	}

}

/* End of file Report.php */
/* Location: ./application/controllers/Report.php */