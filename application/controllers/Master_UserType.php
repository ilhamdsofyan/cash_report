<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_UserType extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}
		$this->username = $this->session->userdata('username');
		$this->load->model('M_MasterUserType', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['group'] = $this->model->getGroup(null);
		
		$data['content_title'] = 'Data Master';
		$data['content_subtitle'] = 'Jenis Pengguna';
		$data['content_file'] = 'content/jenis_pengguna';
		$data['content_css'] = 'content/css/jenis_pengguna';
		$data['content_js'] = 'content/js/jenis_pengguna';

		$this->load->view('template/index', $data);
	}

	public function getGroup()
	{
		$param = array(
				"group_id" => $this->input->post('param')
				);
		$getGroup = $this->model->getGroup($param, 'single');
		echo json_encode($getGroup);
	}

	public function saveGroup()
	{
		$method = $this->input->post('_method');
		$data = $this->input->post('data');
		$id = $this->input->post('id');

		if (ctype_alpha(@$data['group_id'])) {
			$this->session->set_flashdata('message', 'Silahkan gunakan angka 0-9 untuk Kode Grup.');
			redirect('data-master/jenis-pengguna','refresh');
		}

		$save = $this->model->storeGroup($method, $data, $id);
		if ($save == true) {
			$this->session->set_flashdata('message', 'Data Jenis Pengguna berhasil di simpan.');
		}
		else {
			$this->session->set_flashdata('message', 'Data Jenis Pengguna gagal di simpan.');
		}

		redirect('data-master/jenis-pengguna','refresh');
	}

	public function deleteGroup()
	{
		$id = $this->uri->segment(4);
		if ($id != '') {
			$delete = $this->model->dropGroup($id);
			if ($delete) {
				$this->session->set_flashdata('message', 'Data berhasil dihapus.');
			}
			else {
				$this->session->set_flashdata('message', 'Data gagal dihapus.');
			}
		}
		else {
			$this->session->set_flashdata('message', 'Silahkan pilih data yang ingin dihapus.');
		}
		redirect('data-master/jenis-pengguna','refresh');
	}

}

/* End of file Master_UserType.php */
/* Location: ./application/controllers/Master_UserType.php */