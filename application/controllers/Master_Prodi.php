<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Prodi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == NULL) {
			redirect('auth/unauthorized','refresh');
		}
		$this->username = $this->session->userdata('username');

		$this->load->model('M_MasterProdi', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['prodi'] = $this->model->getProdi(null, null, null, 'created_at');
		
		$data['content_title'] = 'Data Master';
		$data['content_subtitle'] = 'Program Studi';
		$data['content_file'] = 'content/prodi';
		$data['content_css'] = 'content/css/prodi';
		$data['content_js'] = 'content/js/prodi';

		$this->load->view('template/index', $data);
	}

	public function getProdi()
	{
		$param = array(
				"kodeprodi" => $this->input->post('param')
				);
		$getProdi = $this->model->getProdi($param, null, null, null, null, 'single');
		echo json_encode($getProdi);
	}

	public function saveProdi()
	{
		$method = $this->input->post('_method');
		$data = $this->input->post('data');
		$id = $this->input->post('id');

		$save = $this->model->storeProdi($method, $data, $id);
		if ($save == true) {
			$this->session->set_flashdata('message', 'Data Program Studi berhasil di simpan.');
		}
		else {
			$this->session->set_flashdata('message', 'Data Program Studi gagal di simpan.');
		}

		redirect('data-master/program-studi','refresh');
	}

	public function deleteProdi()
	{
		$id = $this->uri->segment(4);
		if ($id != '') {
			$delete = $this->model->dropProdi($id);
			if ($delete) {
				$this->session->set_flashdata('message', 'Data berhasil dihapus.');
			}
			else {
				$this->session->set_flashdata('message', 'Data gagal dihapus.');
			}
		}
		else {
			$this->session->set_flashdata('message', 'Silahkan pilih data yang ingin dihapus.');
		}
		redirect('data-master/program-studi','refresh');
	}

}

/* End of file Master.php */
/* Location: ./application/controllers/Master.php */