<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->username = $this->session->userdata('username');
		$this->load->model('M_Profile', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['log_statistik'] = count($this->model->getLog($this->username));

		$data['content_title'] = 'Profil Saya';
		$data['content_file'] = 'content/profil';
		$data['content_css'] = 'content/css/profil';
		$data['content_js'] = 'content/js/profil';

		$this->load->view('template/index', $data);
	}

	public function save()
	{
		$profil = $this->input->post('profil');
		$id = $this->input->post('id');

		if (!ctype_digit($profil['telepon']) && strlen($profil['telepon']) < 10) {
			$this->session->set_flashdata('message', 'Silahkan isi nomor telepon dengan benar');
			redirect('profil','refresh');
		}

		if (!empty($_FILES['photo'])) {
			// config of upload libraries
			$upload['upload_path']          = './assets/upload';
	        $upload['allowed_types']        = 'png|jpg|jpeg|gif';
	        $upload['max_size']             = 10000;
	        $upload['file_name']            = $this->username;
	        $upload['overwrite']            = true;
	        // start initialize libraries
	        $this->upload->initialize($upload);
	        if ($this->upload->do_upload('photo')) {
	        	$profil['user_image'] = 'upload/' . $this->upload->data('file_name');
	        }
		}
		$save = $this->model->storeBiodata($id, $profil);
		if ($save) {
			$this->session->set_flashdata('message', 'Data Berhasil di ubah');
		}
		else {
			$this->session->set_flashdata('message', 'Data tidak ada yang diubah, silakan periksa input anda');
		}

		redirect('profil','refresh');
	}

	public function save_login()
	{
		$data_change = $this->input->post('password');
		$id = $this->input->post('id');
		// check if username changed
		if ($data_change['username'] != $this->username) {
			$redir = 'auth/logout';
		}
		else {
			$redir = 'profil#tab_1_2';
		}
		if ($data_change['passwd'] == '') {
			unset($data_change['passwd']);
		}
		// check if password true
		$check = $this->model->checkPassword($this->username, $data_change['current_pass']);
		if ($check == true) {
			unset($data_change['current_pass']);
			$save = $this->model->storeCredential($id, $data_change);
			if ($save) {
				$this->session->set_flashdata('message', 'Password dan/atau username berhasil diubah');
			}
			else {
				$this->session->set_flashdata('message', 'Data tidak ada yang diubah, silakan periksa input anda');
			}
		}
		else {
			$this->session->set_flashdata('message', 'Password lama anda salah, silahkan periksa input anda');
		}
		redirect($redir,'refresh');
	}

}

/* End of file My_Profile.php */
/* Location: ./application/controllers/My_Profile.php */