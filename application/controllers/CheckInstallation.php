<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckInstallation extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Main', 'model');

		$check = $this->model->CheckUser();
		// print_r($check);die();
		if (!empty($check)) {
			redirect('login','refresh');
		}
	}

	public function index()
	{
		$data['content_title'] = "Pemasangan Aplikasi";
		$this->load->view('template/first_registration', $data);
	}

	public function AdminReg()
	{
		$data = $this->input->post('Reg');

		$save = $this->model->adminSave($data);
		if ($save) {
			redirect('login','refresh');
		}
		else{
			$this->session->set_flashdata('message', 'Pendaftaran Gagal');
		}
	}

}

/* End of file CheckInstallation.php */
/* Location: ./application/controllers/CheckInstallation.php */