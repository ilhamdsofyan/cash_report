<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}
		$this->username = $this->session->userdata('username');
		$this->load->model('M_MasterUser', 'model');
		$this->load->model('M_MasterUserType');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$user = $this->model->getUser(array('username <>' => $this->username));
		foreach ($user as $key => $value) {
			$data['user'][$key] = $value;
			$data['user'][$key]['diubah'] = $this->mainmodel->format_date_ind($value['updated_at'], 'long', true);
			$data['user'][$key]['dibuat'] = $this->mainmodel->format_date_ind($value['created_at'], 'long', true);
			unset($data['user'][$key]['created_at']);
			unset($data['user'][$key]['updated_at']);
		}
		$data['group'] = $this->M_MasterUserType->getGroup();
		
		$data['content_title'] = 'Data Master';
		$data['content_subtitle'] = 'Pengguna';
		$data['content_file'] = 'content/pengguna';
		$data['content_css'] = 'content/css/pengguna';
		$data['content_js'] = 'content/js/pengguna';

		$this->load->view('template/index', $data);
	}

	public function getUser()
	{
		$param = array(
				"user_id" => $this->input->post('param')
				);
		$getUser = $this->model->getUser($param, 'single');
		echo json_encode($getUser);
	}

	public function saveUser()
	{
		$method = $this->input->post('_method');
		$data = $this->input->post('data');
		$id = $this->input->post('id');

		if ($method == 'insert') {
			$data['passwd'] = '123456';
		}

		$save = $this->model->storeUser($method, $data, $id);
		if ($save == true) {
			$this->session->set_flashdata('message', 'Data Pengguna berhasil di simpan.');
		}
		else {
			$this->session->set_flashdata('message', 'Data Pengguna gagal di simpan.');
		}

		redirect('data-master/pengguna','refresh');
	}

	public function deleteUser()
	{
		$id = $this->uri->segment(4);
		if ($id != '') {
			$delete = $this->model->dropUser($id);
			if ($delete) {
				$this->session->set_flashdata('message', 'Data berhasil dihapus.');
			}
			else {
				$this->session->set_flashdata('message', 'Data gagal dihapus.');
			}
		}
		else {
			$this->session->set_flashdata('message', 'Silahkan pilih data yang ingin dihapus.');
		}
		redirect('data-master/pengguna','refresh');
	}

}

/* End of file Master_UserType.php */
/* Location: ./application/controllers/Master_UserType.php */