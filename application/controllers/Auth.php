<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	/**
	 * @author Ilham Dwi Sofyan <ilhamdsofyan@gmail.com>
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login', 'model');
	}

	public function index()
	{
		if ($this->session->userdata('username') != NULL) {
			redirect('beranda','refresh');
		}

		$data['content_title'] = 'Masuk';
		$this->load->view('template/login', $data);
	}

	public function doAuth()
	{
		$user = $this->input->post('user');
		$processLogin = $this->model->processLogin($user);

		if ($processLogin != null) {
			$this->makeSession($processLogin);
			$this->userLog();
			$userType = $this->mainmodel->getUserData($user['username']);
		}
		else {
			$this->session->set_flashdata('input', $user['username']);
			$this->session->set_flashdata('message', 'Proses Login gagal, silahkan periksa username dan password anda');

		}
		redirect('/login','refresh');
	}

	public function makeSession($user_data)
	{
		foreach ($user_data as $value) {
			$this->session->set_userdata('username', $value['username']);
		}
	}

	public function userLog()
	{
		$data['username'] = $this->session->userdata('username');
		$data['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$data['namakomputer'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$data['browser'] = $_SERVER['HTTP_USER_AGENT'];
		$data['tanggal'] = date('Y-m-d H:i:s');

		$this->model->createUserLog($data);
	}

	public function unauthorized()
	{
		$data['heading'] = '401 - Unauthorized';
		$data['message'] = 'Anda belum login. Silakan <a href="'.site_url('/login').'">masuk</a> dengan menggunakan <b>username</b> dan <b>password</b> yang telah dimiliki';
		$this->session->sess_destroy();
		$this->load->view('errors/html/error_401.php', $data);
	}

	public function notFound()
	{
		$data['heading'] = '404 - Page Not Found';
		$data['message'] = 'Halaman tidak ditemukan. <a href=javascript:window.history.go(-1);>Kembali</a>';
		$this->load->view('errors/html/error_404.php', $data);
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->sess_destroy();
		redirect('/', 'refresh');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */