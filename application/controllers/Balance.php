<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->monthID = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		$this->username = $this->session->userdata('username');

		$this->load->model('M_Neraca', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['year'] = $this->model->getYear();

		$data['content_title'] = 'Laporan';
		$data['content_subtitle'] = 'Neraca';
		$data['content_file'] = 'content/neraca';
		$data['content_css'] = 'content/css/neraca';
		$data['content_js'] = 'content/js/neraca';

		$this->load->view('template/index', $data);
	}

	public function getMonth()
	{
		$year = $this->input->post('tahun');
		$getMonth = $this->model->getMonth($year);

		echo json_encode($getMonth);
	}

	public function getProfit($period=array())
	{
		$accountIncome = $this->model->getAccount(array('AccGroup' => '4'), 'AccNo, AccName');
		$accountCost = $this->model->getAccount(array('AccGroup' => '5'), 'AccNo, AccName');
		foreach ($accountIncome as $key => $value) {
			$getIncome[$key] = $this->model->getIncome($period['tahun'], $period['bulan'], $value['AccNo']);
			if (empty($getCost[$key][0])) {
				$getCost[$key][0]['AccNo'] = $value['AccNo'];
				$getCost[$key][0]['AccName'] = $value['AccName'];
				$getCost[$key][0]['saldo_berjalan'] = 0;
			}
			if (empty($getCost[$key][1])) {
				$getCost[$key][1]['AccNo'] = $value['AccNo'];
				$getCost[$key][1]['AccName'] = $value['AccName'];
				$getCost[$key][1]['saldo_sampai'] = 0;
			}
		}
		foreach ($accountCost as $key => $value) {
			$getCost[$key] = $this->model->getCost($period['tahun'], $period['bulan'], $value['AccNo']);
			if (empty($getCost[$key][0])) {
				$getCost[$key][0]['AccNo'] = $value['AccNo'];
				$getCost[$key][0]['AccName'] = $value['AccName'];
				$getCost[$key][0]['saldo_berjalan'] = 0;
			}
			if (empty($getCost[$key][1])) {
				$getCost[$key][1]['AccNo'] = $value['AccNo'];
				$getCost[$key][1]['AccName'] = $value['AccName'];
				$getCost[$key][1]['saldo_sampai'] = 0;
			}
		}
		$sumIncome_now = 0;
		$sumIncome_run = 0;
		$sumCost_now = 0;
		$sumCost_run = 0;

		foreach ($getIncome as $key => $value) {
			$sumIncome_now += $value[0]['saldo_berjalan'];
			$sumIncome_run += $value[1]['saldo_sampai'];
		}
		foreach ($getCost as $key => $value) {
			$sumCost_now += $value[0]['saldo_berjalan'];
			$sumCost_run += $value[1]['saldo_sampai'];
		}
		return $sumIncome_run - $sumCost_run;
	}

	public function showBalance()
	{
		$period = $this->input->post();
		$accountAsset = $this->model->getAccount(array('AccGroup' => 1), 'AccNo, AccName');
		$accountLiabil1 = $this->model->getAccount(array('AccGroup' => 3), 'AccNo, AccName');
		$accountLiabil2 = $this->model->getAccount(array('AccGroup' => 2), 'AccNo, AccName');
		$accountLiabil = array_merge($accountLiabil1, $accountLiabil2);
		$profit = $this->getProfit($period);
		$i = 0;
		foreach ($accountAsset as $key => $value) {
			$getAssets[] = $this->model->getAssets($period['tahun'], $period['bulan'], $value['AccNo']);
			if (empty($getAssets[$key])) {
				$getAssets[$key]['AccNo'] = $value['AccNo'];
				$getAssets[$key]['AccName'] = $value['AccName'];
				$getAssets[$key]['saldo'] = 0;
			}
		}
		foreach ($accountLiabil as $key => $value) {
			$getLiabilities[] = $this->model->getLiabilities($period['tahun'], $period['bulan'], $value['AccNo']);
			if (empty($getLiabilities[$key])) {
				$getLiabilities[$key]['AccNo'] = $value['AccNo'];
				$getLiabilities[$key]['AccName'] = $value['AccName'];
				$getLiabilities[$key]['saldo'] = 0;
			}
			$i += $key;
		}
			$getLiabilities[$i]['AccNo'] = '313';
			$getLiabilities[$i]['AccName'] = 'Laba Periode Berjalan';
			$getLiabilities[$i]['saldo'] = $profit;
		
		$sumAssets = 0;
		$sumLiabilities = 0;
		$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-right">';
							$view .= '<button class="btn btn-info" id="btn-print"><i class="fa fa-print"></i>&nbsp;Cetak</button>';
						$view .= '</div>';
					$view .= '</div>';
					$view .= '<div class="row" id="content-print">';
						$view .= '<div class="col-lg-12">';
							$view .= '<h3 class="text-center">Laporan Neraca <br> Periode Bulan '. $this->monthID[$period['bulan']] . ' ' . $period['tahun'] .'</h3>';
								$view .= '<div class="table-responsive">';
									$view .= '<table class="table">';
										$view .= '<tr style="background-color:#6C7A89;color:white;">';
											$view .= '<th colspan="3" style="text-align:right">'. $this->monthID[$period['bulan']] .'</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#ECF0F1;">';
											$view .= '<th colspan="4">AKTIVA</th>';
										$view .= '</tr>';
										foreach ($getAssets as $key => $value) {
											$view .= '<tr>';
												$view .= '<td style="text-align:center">'. $value['AccNo'] .'</td>';
												$view .= '<td>'. $value['AccName'] .'</td>';
												$view .= '<td style="text-align:right">'. number_format($value['saldo']) .'</td>';
											$view .= '</tr>';

											$sumAssets += $value['saldo'];
										}
										$view .= '<tr style="background-color:grey;">';
											$view .= '<th colspan="2">Total Aktiva</th>';
											$view .= '<th style="text-align:right">'. number_format($sumAssets) .'</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#fff;">';
											$view .= '<th colspan="4"></th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#ECF0F1;">';
											$view .= '<th colspan="4">PASIVA</th>';
										$view .= '</tr>';
										foreach ($getLiabilities as $key => $value) {
											$view .= '<tr>';
												$view .= '<td style="text-align:center">'. $value['AccNo'] .'</td>';
												$view .= '<td>'. $value['AccName'] .'</td>';
												$view .= '<td style="text-align:right">'. number_format($value['saldo']) .'</td>';
											$view .= '</tr>';

											$sumLiabilities += $value['saldo'];
										}
										$view .= '<tr style="background-color:grey;">';
											$view .= '<th colspan="2">Total Pasiva</th>';
											$view .= '<th style="text-align:right">'. number_format($sumLiabilities) .'</th>';
										$view .= '</tr>';
									$view .= '</table>';
								$view .= '</div>';
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';

		echo $view;
	}


}

/* End of file Balance.php */
/* Location: ./application/controllers/Balance.php */