<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Akun extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->username = $this->session->userdata('username');
		$this->load->model('M_MasterAkun', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['account'] = $this->model->getAccounts(null);
		
		$data['content_title'] = 'Data Master';
		$data['content_subtitle'] = 'Bagan Akun';
		$data['content_file'] = 'content/akun';
		$data['content_css'] = 'content/css/akun';
		$data['content_js'] = 'content/js/akun';

		$this->load->view('template/index', $data);
	}

	public function getAkun()
	{
		$param = array('AccNo' => $this->input->post('param'));
		$getAkun = $this->model->getAccounts($param, 'single');

		echo json_encode($getAkun);
	}

	public function saveAkun()
	{
		$method = $this->input->post('_method');
		$data = $this->input->post('data');
		$id = $this->input->post('input-id');

		$save = $this->model->storeAccounts($method, $data, $id);
		if ($save == true) {
			$this->session->set_flashdata('message', 'Data Akun berhasil di simpan.');
		}
		else {
			$this->session->set_flashdata('message', 'Data Akun gagal di simpan.');
		}

		redirect('data-master/bagan-akun','refresh');
	}

	public function deleteAkun()
	{
		$id = $this->uri->segment(4);
		if ($id != '') {
			$delete = $this->model->dropAccounts($id);
			if ($delete) {
				$this->session->set_flashdata('message', 'Data berhasil dihapus.');
			}
			else {
				$this->session->set_flashdata('message', 'Data gagal dihapus.');
			}
		}
		else {
			$this->session->set_flashdata('message', 'Silahkan pilih data yang ingin dihapus.');
		}
		redirect('data-master/bagan-akun','refresh');
	}

}

/* End of file Master_Akun.php */
/* Location: ./application/controllers/Master_Akun.php */