<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->username = $this->session->userdata('username');
		if ($this->username == null) {
			redirect('auth/unauthorized', 'refresh');
		}

		$this->load->model('M_Dashboard', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['account'] = $this->model->getAccount('created_at', 5);
		if ($data['biodata']['user_type'] == 1) {
			$data['logged'] = $this->model->getLogged('tanggal', 5);
			$data['dataMaster'] = $this->model->getCountMaster(array('tb_account','tb_user','tb_user_group'));
		}

		$data['content_title'] = 'Beranda';
		$data['content_file'] = 'content/beranda';
		$data['content_css'] = 'content/css/beranda';
		$data['content_js'] = 'content/js/beranda';

		$this->load->view('template/index', $data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */