<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profit_Loss extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('username') == null) {
			redirect('auth/unauthorized','refresh');
		}

		$this->monthID = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		$this->username = $this->session->userdata('username');

		$this->load->model('M_ProfitLoss', 'model');
	}

	public function index()
	{
		$data['biodata'] = $this->mainmodel->getUserData($this->username);
		$data['year'] = $this->model->getYear();

		$data['content_title'] = 'Laporan';
		$data['content_subtitle'] = 'Laba Rugi';
		$data['content_file'] = 'content/laba_rugi';
		$data['content_css'] = 'content/css/laba_rugi';
		$data['content_js'] = 'content/js/laba_rugi';

		$this->load->view('template/index', $data);
	}

	public function getMonth()
	{
		$year = $this->input->post('tahun');
		$getMonth = $this->model->getMonth($year);

		echo json_encode($getMonth);
	}

	public function showProfLoss()
	{
		$period = $this->input->post();
		$accountIncome = $this->model->getAccount(array('AccGroup' => '4'), 'AccNo, AccName');
		$accountCost = $this->model->getAccount(array('AccGroup' => '5'), 'AccNo, AccName');
		foreach ($accountIncome as $key => $value) {
			$getIncome[$key] = $this->model->getIncome($period['tahun'], $period['bulan'], $value['AccNo']);
			if (empty($getCost[$key][0])) {
				$getIncome[$key][0]['AccNo'] = $value['AccNo'];
				$getIncome[$key][0]['AccName'] = $value['AccName'];
				$getIncome[$key][0]['saldo_berjalan'] = 0;
			}
			if (empty($getCost[$key][1])) {
				$getCost[$key][1]['AccNo'] = $value['AccNo'];
				$getCost[$key][1]['AccName'] = $value['AccName'];
				$getCost[$key][1]['saldo_sampai'] = 0;
			}
		}
		// print_r($accountIncome);die();
		foreach ($accountCost as $key => $value) {
			$getCost[$key] = $this->model->getCost($period['tahun'], $period['bulan'], $value['AccNo']);
			if (empty($getCost[$key][0])) {
				$getCost[$key][0]['AccNo'] = $value['AccNo'];
				$getCost[$key][0]['AccName'] = $value['AccName'];
				$getCost[$key][0]['saldo_berjalan'] = 0;
			}
			if (empty($getCost[$key][1])) {
				$getCost[$key][1]['AccNo'] = $value['AccNo'];
				$getCost[$key][1]['AccName'] = $value['AccName'];
				$getCost[$key][1]['saldo_sampai'] = 0;
			}
		}
		$sumIncome_now = 0;
		$sumIncome_run = 0;
		$sumCost_now = 0;
		$sumCost_run = 0;
		$view = '<div class="portlet light">';
				$view .= '<div class="portlet-body">';
					$view .= '<div class="row">';
						$view .= '<div class="col-lg-12 text-right">';
							$view .= '<button class="btn btn-info" id="btn-print"><i class="fa fa-print"></i>&nbsp;Cetak</button>';
						$view .= '</div>';
					$view .= '</div>';
					$view .= '<div class="row" id="content-print">';
						$view .= '<div class="col-lg-12">';
							$view .= '<h3 class="text-center">Laporan Laba Rugi <br> Periode Bulan '. $this->monthID[$period['bulan']] . ' ' . $period['tahun'] .'</h3>';
								$view .= '<div class="table-responsive">';
									$view .= '<table class="table">';
										$view .= '<tr style="background-color:#6C7A89;color:white;">';
											$view .= '<th colspan="3" style="text-align:right">'. $this->monthID[$period['bulan']] .'</th>';
											$view .= '<th style="text-align:right">'. 's/d ' . $this->monthID[$period['bulan']] .'</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#ECF0F1;">';
											$view .= '<th colspan="4">PENDAPATAN</th>';
										$view .= '</tr>';
										foreach ($getIncome as $key => $value) {
											$view .= '<tr>';
												$view .= '<td style="text-align:center">'. $value[0]['AccNo'] .'</td>';
												$view .= '<td>'. $value[0]['AccName'] .'</td>';
												$view .= '<td style="text-align:right">'. number_format($value[0]['saldo_berjalan']) .'</td>';
												$view .= '<td style="text-align:right">'. number_format($value[1]['saldo_sampai']) .'</td>';
											$view .= '</tr>';

											$sumIncome_now += $value[0]['saldo_berjalan'];
											$sumIncome_run += $value[1]['saldo_sampai'];
										}
										$view .= '<tr style="background-color:grey;">';
											$view .= '<th colspan="2">Total Pendapatan</th>';
											$view .= '<th style="text-align:right">'. number_format($sumIncome_now) .'</th>';
											$view .= '<th style="text-align:right">'. number_format($sumIncome_run) .'</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#fff;">';
											$view .= '<th colspan="4"></th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#ECF0F1;">';
											$view .= '<th colspan="4">BIAYA</th>';
										$view .= '</tr>';
										foreach ($getCost as $key => $value) {
											$view .= '<tr>';
												$view .= '<td style="text-align:center">'. $value[0]['AccNo'] .'</td>';
												$view .= '<td>'. $value[0]['AccName'] .'</td>';
												$view .= '<td style="text-align:right">'. number_format($value[0]['saldo_berjalan']) .'</td>';
												$view .= '<td style="text-align:right">'. number_format($value[1]['saldo_sampai']) .'</td>';
											$view .= '</tr>';

											$sumCost_now += $value[0]['saldo_berjalan'];
											$sumCost_run += $value[1]['saldo_sampai'];
										}
										$view .= '<tr style="background-color:grey;">';
											$view .= '<th colspan="2">Total Biaya</th>';
											$view .= '<th style="text-align:right">'. number_format($sumCost_now) .'</th>';
											$view .= '<th style="text-align:right">'. number_format($sumCost_run) .'</th>';
										$view .= '</tr>';
										$view .= '<tr style="background-color:#6C7A89;color:white;">';
											$view .= '<th colspan="2">Total Laba/Rugi</th>';
											$view .= '<th style="text-align:right">'. number_format($sumIncome_now - $sumCost_now) .'</th>';
											$view .= '<th style="text-align:right">'. number_format($sumIncome_run - $sumCost_run) .'</th>';
										$view .= '</tr>';
									$view .= '</table>';
								$view .= '</div>';
						$view .= '</div>';
					$view .= '</div>';
				$view .= '</div>';
			$view .= '</div>';

		echo $view;
	}

}

/* End of file Profit_Loss.php */
/* Location: ./application/controllers/Profit_Loss.php */