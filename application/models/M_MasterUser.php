<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_MasterUser extends CI_Model {

	public function getUser($param=array(), $type='all')
	{
		if ($type == 'all') {
			if (!empty($param)) {
				$this->db->where($param);
			}
			$this->db->join('tb_user_group', 'tb_user_group.group_id = tb_user.user_type');
			$getUser = $this->db->get('tb_user')->result_array();
		}
		else {
			if (!empty($param)) {
				$this->db->where($param);
			}
			$getUser = $this->db->get('tb_user')->row_array();
		}
		
		return $getUser;
	}

	public function storeUser($trigger='', $data, $id='')
	{
		if ($trigger != '' && !empty($data)) {
			if ($trigger == "insert") {
				$data['created_at'] = date("YmdHis");
				return $this->db->insert('tb_user', $data);
			}
			elseif ($trigger == "update" && $id != '') {
				$this->db->where('user_id', $id);
				return $this->db->update('tb_user', $data);
			}
			else{
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function dropUser($id='')
	{
		if ($id != '') {
			$this->db->where('user_id', $id);
			return $this->db->delete('tb_user');
		}
		else{
			return false;
		}
	}

}

/* End of file M_MasterUser.php */
/* Location: ./application/models/M_MasterUser.php */