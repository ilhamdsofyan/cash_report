<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_MasterUserType extends CI_Model {

	public function getGroup($param = array(), $type='all')
	{
		if ($type == 'all') {
			if (!empty($param)) {
				$this->db->where($param);
			}
			$getGroup = $this->db->get('tb_user_group')->result_array();
		}
		else {
			if (!empty($param)) {
				$this->db->where($param);
			}
			$getGroup = $this->db->get('tb_user_group')->row_array();
		}
		
		return $getGroup;
	}

	public function storeGroup($trigger='', $data, $id='')
	{
		if ($trigger != '' && !empty($data)) {
			if ($trigger == "insert") {
				$data['created_at'] = date("YmdHis");
				return $this->db->insert('tb_user_group', $data);
			}
			elseif ($trigger == "update" && $id != '') {
				$this->db->where('group_id', $id);
				return $this->db->update('tb_user_group', $data);
			}
			else{
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function dropGroup($id='')
	{
		if ($id != '') {
			$this->db->where('group_id', $id);
			$this->db->delete('tb_user_group');
			return true;
		}
		else{
			return false;
		}
	}

}

/* End of file M_Master.php */
/* Location: ./application/models/M_Master.php */