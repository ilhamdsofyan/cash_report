<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ProfitLoss extends CI_Model {

	public function getYear()
	{
		$this->db->select('YEAR(Tanggal) AS tahun');
		$this->db->group_by('YEAR(Tanggal)');
		$this->db->where('tb_transh.approve', 1);
		return $this->db->get('tb_transh')->result_array();
	}

	public function getMonth($year='')
	{
		if ($year != '') {
			$this->db->where('YEAR(Tanggal)', $year);
		}
		$this->db->select('MONTH(Tanggal) AS bulan');
		$this->db->group_by('MONTH(Tanggal)');
		$this->db->where('tb_transh.approve', 1);
		return $this->db->get('tb_transh')->result_array();
	}

	public function getAccount($param=array(), $column=array(), $groupBy='')
	{
		if (!empty($param)) {
			$this->db->where($param);
		}
		if (!empty($column)) {
			$this->db->select($column);
		}
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}

		return $this->db->get('tb_account')->result_array();
	}

	public function getIncome($year='', $month='', $accno='')
	{
		// query by month
		$this->db->select('tb_account.AccNo, tb_account.AccName, (SUM(tb_transd.debit - tb_transd.kredit) * -1) AS saldo_berjalan');
		$this->db->from('tb_account');
		$this->db->join('tb_transd', 'tb_account.AccNo = tb_transd.AccNo', 'left');
		$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher', 'inner');
		$this->db->where('YEAR(tb_transh.Tanggal)', $year);
		$this->db->where('MONTH(tb_transh.Tanggal)', $month);
		$this->db->where('tb_account.AccNo', $accno);
		$this->db->where('tb_transh.approve', 1);
		$this->db->group_by('tb_account.AccNo');
		$this->db->order_by('tb_account.AccNo', 'asc');

		$byMonth = $this->db->get()->row_array();

		// query till month
		$this->db->select('(SUM(tb_transd.debit - tb_transd.kredit) * -1) AS saldo_sampai');
		$this->db->from('tb_account');
		$this->db->join('tb_transd', 'tb_account.AccNo = tb_transd.AccNo', 'left');
		$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher', 'inner');
		$this->db->where('YEAR(tb_transh.Tanggal)', $year);
		$this->db->where('MONTH(tb_transh.Tanggal) <= ', $month);
		$this->db->where('tb_account.AccNo', $accno);
		$this->db->where('tb_transh.approve', 1);
		$this->db->group_by('tb_account.AccNo');
		$this->db->order_by('tb_account.AccNo', 'asc');

		$toMonth = $this->db->get()->row_array();

		return array($byMonth, $toMonth);
	}

	public function getCost($year='', $month='', $accno)
	{
		// query by month
		$this->db->select('tb_account.AccNo, tb_account.AccName, SUM(tb_transd.debit - tb_transd.kredit) AS saldo_berjalan');
		$this->db->from('tb_account');
		$this->db->join('tb_transd', 'tb_account.AccNo = tb_transd.AccNo', 'LEFT');
		$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher', 'INNER');
		$this->db->where('YEAR(tb_transh.Tanggal)', $year);
		$this->db->where('MONTH(tb_transh.Tanggal)', $month);
		$this->db->where('tb_account.AccNo', $accno);
		$this->db->where('tb_transh.approve', 1);
		$this->db->group_by('tb_account.AccNo');
		$this->db->order_by('tb_account.AccNo', 'asc');

		$byMonth = $this->db->get()->row_array();

		// query till month
		$this->db->select('tb_account.AccNo, tb_account.AccName, SUM(tb_transd.debit - tb_transd.kredit) AS saldo_sampai');
		$this->db->from('tb_account');
		$this->db->join('tb_transd', 'tb_account.AccNo = tb_transd.AccNo', 'LEFT');
		$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher', 'INNER');
		$this->db->where('YEAR(tb_transh.Tanggal)', $year);
		$this->db->where('MONTH(tb_transh.Tanggal) <= ', $month);
		$this->db->where('tb_account.AccNo', $accno);
		$this->db->where('tb_transh.approve', 1);
		$this->db->group_by('tb_account.AccNo');
		$this->db->order_by('tb_account.AccNo', 'asc');

		$toMonth = $this->db->get()->row_array();

		return array($byMonth, $toMonth);
	}

}

/* End of file M_ProfitLoss.php */
/* Location: ./application/models/M_ProfitLoss.php */