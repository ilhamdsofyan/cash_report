<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_NeracaSaldo extends CI_Model {

	public function getYear()
	{
		$this->db->select('YEAR(Tanggal) AS tahun');
		$this->db->group_by('YEAR(Tanggal)');
		$this->db->where('tb_transh.approve', 1);

		return $this->db->get('tb_transh')->result_array();
	}

	public function getMonth($year='')
	{
		if ($year != '') {
			$this->db->where('YEAR(Tanggal)', $year);
		}
		$this->db->select('MONTH(Tanggal) AS bulan');
		$this->db->group_by('MONTH(Tanggal)');
		$this->db->where('tb_transh.approve', 1);

		return $this->db->get('tb_transh')->result_array();
	}

	// public function getBranch($param=array(), $column=array())
	// {
	// 	if (!empty($param)) {
	// 		$this->db->where($param);
	// 	}
	// 	if (!empty($column)) {
	// 		$this->db->select($column);
	// 	}

	// 	return $this->db->get('tb_branch')->result_array();
	// }

	public function getAccount($param=array(), $column=array(), $join=true)
	{
		if (!empty($param)) {
			$this->db->where($param);
		}
		if (!empty($column)) {
			$this->db->select($column);
		}
		if ($join == true) {
			$this->db->join('tb_transd', 'tb_transd.AccNo = tb_account.AccNo');
			$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher');
			$this->db->where('tb_transh.approve', 1);
		}

		return $this->db->get('tb_account')->result_array();
	}

	public function trialBalanceNow($period=array(), $account='')
	{
		$this->db->select('tb_account.AccNo, tb_account.AccName, SUM(tb_transd.debit) AS debit, SUM(tb_transd.kredit) AS kredit,
							IF(SUM(tb_transd.debit - tb_transd.kredit) >= 0, SUM(tb_transd.debit - tb_transd.kredit), 0) AS saldo_debit,
							IF(SUM(tb_transd.debit - tb_transd.kredit) <= 0, SUM(tb_transd.kredit - tb_transd.debit), 0) AS saldo_kredit
						');
		$this->db->from('tb_account');
		$this->db->join('tb_transd', 'tb_account.AccNo = tb_transd.AccNo', 'left');
		$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher');
		$this->db->where('YEAR(tb_transh.Tanggal)', $period['tahun']);
		$this->db->where('MONTH(tb_transh.Tanggal)', $period['bulan']);

		$this->db->where('tb_account.AccNo', $account);
		$this->db->where('tb_transh.approve', 1);
		$this->db->group_by('tb_account.AccNo');
		$this->db->order_by('tb_account.AccNo', 'asc');

		return $this->db->get()->row_array();
	}

	public function trialBalanceRunning($period=array(), $account='')
	{
		$this->db->select('tb_account.AccNo, tb_account.AccName, SUM(tb_transd.debit) AS debit_all, SUM(tb_transd.kredit) AS kredit_all,
							IF(SUM(tb_transd.debit - tb_transd.kredit) >= 0, SUM(tb_transd.debit - tb_transd.kredit), 0) AS saldo_debit_all,
							IF(SUM(tb_transd.debit - tb_transd.kredit) <= 0, SUM(tb_transd.kredit - tb_transd.debit), 0) AS saldo_kredit_all
						');
		$this->db->from('tb_account');
		$this->db->join('tb_transd', 'tb_account.AccNo = tb_transd.AccNo', 'left');
		$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher');
		if ($period['bulan'] - 1 == 0) {
			$this->db->where('YEAR(tb_transh.Tanggal)', $period['tahun'] - 1);
			$this->db->where('MONTH(tb_transh.Tanggal) <= ', $period['bulan'] + 12);
		}
		else {
			$this->db->where('YEAR(tb_transh.Tanggal)', $period['tahun']);
			$this->db->where('MONTH(tb_transh.Tanggal) <= ', $period['bulan'] - 1);	
		}
		$this->db->where('tb_account.AccNo', $account);
		$this->db->where('tb_transh.approve', 1);
		$this->db->group_by('tb_account.AccNo');
		$this->db->order_by('tb_account.AccNo', 'asc');

		return $this->db->get()->row_array();
	}

}

/* End of file M_NeracaSaldo.php */
/* Location: ./application/models/M_NeracaSaldo.php */