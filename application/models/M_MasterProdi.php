<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_MasterProdi extends CI_Model {

	public function getProdi($param = array(), $column = array(), $groupBy = array(), $orderBy = '', $order = 'desc', $type='all')
	{
		if ($type == 'all') {
			if (!empty($param)) {
				$this->db->where($param);
			}
			if (!empty($column)) {
				$this->db->select($column);
			}
			if (!empty($groupBy)) {
				$this->db->group_by($groupBy);
			}
			if (!empty($orderBy)) {
				$this->db->order_by($orderBy, $order);
			}
			$getProdi = $this->db->get('tb_prodi')->result_array();
		}
		else {
			if (!empty($param)) {
				$this->db->where($param);
			}
			if (!empty($column)) {
				$this->db->select($column);
			}
			if (!empty($groupBy)) {
				$this->db->group_by($groupBy);
			}
			if (!empty($orderBy)) {
				$this->db->order_by($orderBy, $order);
			}
			$getProdi = $this->db->get('tb_prodi')->row_array();
		}
		
		return $getProdi;
	}

	public function storeProdi($trigger='', $data, $id='')
	{
		if ($trigger != '' && !empty($data)) {
			if ($trigger == "insert") {
				$data['created_at'] = date("YmdHis");
				return $this->db->insert('tb_prodi', $data);
			}
			elseif ($trigger == "update" && $id != '') {
				$this->db->where('kodeprodi', $id);
				return $this->db->update('tb_prodi', $data);
			}
			else{
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function dropProdi($id='')
	{
		if ($id != '') {
			$this->db->where('kodeprodi', $id);
			$this->db->delete('tb_prodi');
			return true;
		}
		else{
			return false;
		}
	}

}

/* End of file M_Master.php */
/* Location: ./application/models/M_Master.php */