<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Journal extends CI_Model {

	public function getJournal($data)
	{
		if (is_array($data)) {
			$this->db->join('tb_transd', 'tb_transd.NoVoucher = tb_transh.NoVoucher');
			$this->db->join('tb_account', 'tb_account.AccNo = tb_transd.AccNo');

			$this->db->order_by('tb_transh.Tanggal', 'asc');
			$this->db->where('tb_transh.approve', 1);

			$param = array(
					'DATE(tb_transh.Tanggal) >= ' => $data['start'],
					'DATE(tb_transh.Tanggal) <= ' => $data['end']
					);
			if (isset($data['AccNo'])) {
				$param['tb_transd.AccNo'] = $data['AccNo'];
			}

			$getJournal = $this->db->get_where('tb_transh', $param);
			return $getJournal->result_array();
		}
		else {
			return null;
		}
	}

}

/* End of file M_Journal.php */
/* Location: ./application/models/M_Journal.php */