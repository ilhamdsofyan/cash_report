<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Main extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function CheckUser()
	{
		$CheckUser = $this->db->get('tb_user')->result_array();
		if ($CheckUser > 0) {
			$validate = true;
		}
		else {
			$validate = false;
		}

		return $CheckUser;
	}

	public function adminSave($data)
	{
		if (!empty($data)) {
			if ($this->db->insert('tb_user', $data)) {
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function getUserData($user = '')
	{
		if ($user != '') {
			$data = $this->db->get_where('tb_user', array('username' => $user))->row_array();
		}
		else {
			$data = null;
		}

		return $data;
	}

	public function format_date_ind($date='', $format='default', $clock=false)
	{
		if ($date != '') {
			$date = date_create($date);
			$time = date_format($date, 'H:i');

			if ($format == 'default') {
				$day = date_format($date, 'd');
				$month = date_format($date, 'm');
				$year = date_format($date, 'y');
				$format_date_ind = $day . '-' . $month . '-' . $year;
				if ($clock == false) {
					$format_date_ind = $day . ' ' . $month . ' ' . $year;
				}
				else {
					$format_date_ind = $day . '-' . $month . '-' . $year . ' ' . $time;
				}
			}
			elseif ($format == 'medium') {
				$day = date_format($date, 'd');
				$month = date_format($date, 'm');
				$year = date_format($date, 'Y');
				$format_date_ind = $day . '-' . $month . '-' . $year;
				if ($clock == false) {
					$format_date_ind = $day . ' ' . $month . ' ' . $year;
				}
				else {
					$format_date_ind = $day . '-' . $month . '-' . $year . ' ' . $time;
				}
			}
			else {
				$day = date_format($date, 'd');
				$month = $this->getMonthID(date_format($date, 'n'));
				$year = date_format($date, 'Y');
				if ($clock == false) {
					$format_date_ind = $day . ' ' . $month . ' ' . $year;
				}
				else {
					$format_date_ind = $day . ' ' . $month . ' ' . $year . ' ' . $time . ' WIB';
				}
			}
		}
		else {
			$format_date_ind = '-';
		}
		return $format_date_ind;
	}

	public function getMonthID($value='')
	{
		$month = array(
				'',
				'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'Nopember',
				'Desember'
				);
		return $month[$value];
	}

}

/* End of file M_CheckInstallation.php */
/* Location: ./application/models/M_CheckInstallation.php */