<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Login extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table = "tb_user";
	}	

	public function processLogin($user)
	{
		if (!empty($user)) {
			$processLogin = $this->db->get_where($this->table, $user);
			if (count($processLogin->result_array()) == 1) {
				return $processLogin->result_array();
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}

	public function createUserLog($data)
	{
		if (empty($data)) {
			return false;
		}
		else {
			return $this->db->insert('tb_log_user', $data);
		}
	}

}

/* End of file M_Login.php */
/* Location: ./application/models/M_Login.php */