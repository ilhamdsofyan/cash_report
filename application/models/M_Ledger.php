<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Ledger extends CI_Model {

	public function getYear()
	{
		$this->db->select('YEAR(Tanggal) AS tahun');
		$this->db->group_by('YEAR(Tanggal)');
		$this->db->from('tb_transh');
		return $this->db->get()->result_array();
	}

	public function getTrans($param, $column, $groupBy='')
	{
		if (!empty($param)) {
			$this->db->where($param);
		}
		if (!empty($column)) {
			$this->db->select($column);
		}
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}

		$this->db->join('tb_user', 'tb_user.user_id = tb_transh.id_user');
		// $this->db->join('tb_prodi', 'tb_prodi.kodeprodi = tb_transh.kodeprodi');
		$this->db->join('tb_transd', 'tb_transd.NoVoucher = tb_transh.NoVoucher');
		$this->db->join('tb_account', 'tb_account.AccNo = tb_transd.AccNo');
		$this->db->where('tb_transh.approve', 1);
		$getTrans = $this->db->get('tb_transh')->result_array();
		
		return $getTrans;
	}

	public function getAccount($param=array(), $groupBy='')
	{
		if (!empty($param)) {
			$this->db->where($param);
		}
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
			$this->db->join('tb_transd', 'tb_transd.AccNo = tb_account.AccNo');
			$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher');
			$this->db->where('tb_transh.approve', 1);

		return $this->db->get('tb_account')->result_array();
	}

	public function getLedger($data)
	{
		if (!empty($data)) {
			$this->db->join('tb_transd', 'tb_transd.NoVoucher = tb_transh.NoVoucher');
			// $this->db->join('tb_account', 'tb_account.AccNo = tb_transd.AccNo');
			if ($data['AccNo'] == 'semua') {
				// $this->db->group_by('tb_transd.AccNo');
				$this->db->order_by('tb_transh.Tanggal', 'asc');
				$this->db->where('tb_transh.approve', 1);
				$getLedger = $this->db->get_where('tb_transh', array('DATE(tb_transh.Tanggal) >= ' => $data['start'], 'DATE(tb_transh.Tanggal) <= ' => $data['end']));
				
				return $getLedger->result_array();
			}
			else {
				$this->db->order_by('tb_transd.AccNo, tb_transh.Tanggal', 'asc');
				$this->db->where('tb_transh.approve', 1);
				$getLedger = $this->db->get_where('tb_transh', array('DATE(tb_transh.Tanggal) >= ' => $data['start'], 'DATE(tb_transh.Tanggal) <= ' => $data['end'], 'tb_transd.AccNo' => $data['AccNo']));

				return $getLedger->result_array();
			}
		}
		else {
			return null;
		}
	}

}

/* End of file M_Ledger.php */
/* Location: ./application/models/M_Ledger.php */