<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Profile extends CI_Model {

	public function getLog($user='')
	{
		if ($user != '') {
			$this->db->where('username', $user);
			return $this->db->get('tb_log_user')->result_array();
		}
	}

	public function storeBiodata($id='', $data)
	{
		if ($id != "" && !empty($data)) {
			$this->db->where('user_id', $id);
			return $this->db->update('tb_user', $data);
		}
		else{
			return false;
		}
	}

	public function checkPassword($user='', $pass='')
	{
		if ($user != '' && $pass != '') {
			$param = array(
					'username' => $user,
					'passwd' => $pass
					);
			$data = $this->db->get_where('tb_user', $param)->row_array();
			if (count($data) > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function storeCredential($id='', $data)
	{
		if ($id != '' && !empty($data)) {
			$this->db->where('user_id', $id);
			return $this->db->update('tb_user', $data);
		}
		else {
			return false;
		}
	}

}

/* End of file M_Profile.php */
/* Location: ./application/models/M_Profile.php */