<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_MasterAkun extends CI_Model {

	public function getAccounts($param, $type = 'all')
	{
		if ($type == 'all') {
			if (!empty($param)) {
				$this->db->where($param);
			}
			$getAccounts = $this->db->get('tb_account')->result_array();
		}
		else {
			if (!empty($param)) {
				$this->db->where($param);
			}
			$getAccounts = $this->db->get('tb_account')->row_array();
		}
		
		return $getAccounts;
	}

	public function storeAccounts($trigger='', $data, $id='')
	{
		if ($trigger != '' && !empty($data)) {
			if ($trigger == "insert") {
				$data['created_at'] = date("YmdHis");
				return $this->db->insert('tb_account', $data);
			}
			elseif ($trigger == "update" && $id != '') {
				$this->db->where('AccNo', $id);
				return $this->db->update('tb_account', $data);
			}
			else{
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function dropAccounts($id='')
	{
		if ($id != '') {
			$this->db->where('AccNo', $id);
			$delete = $this->db->delete('tb_transd');
			if ($delete) {
				$this->db->where('AccNo', $id);
				return $this->db->delete('tb_account');
			}
			else {
				return false;
			}
		}
		else{
			return false;
		}
	}	

}

/* End of file M_MasterAkun.php */
/* Location: ./application/models/M_MasterAkun.php */