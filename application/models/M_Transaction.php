<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Transaction extends CI_Model {

	public function getTransH($param=array(), $orderBy='', $type='multiple', $order='desc')
	{
		if (!empty($param)) {
			$this->db->where($param);
		}
		if ($orderBy != '') {
			$this->db->order_by($orderBy, $order);
		}
		$this->db->from('tb_transh');
		$this->db->join('tb_user', 'tb_user.user_id = tb_transh.id_user');
		// $this->db->join('tb_prodi', 'tb_prodi.kodeprodi = tb_transh.kodeprodi');
		if ($type == 'single') {
			$getTrans = $this->db->get()->row_array();
		}
		else {
			$getTrans = $this->db->get()->result_array();
		}
		
		
		return $getTrans;
	}

	public function getTransD($param=array(), $groupBy='', $orderBy='', $order='desc')
	{
		if (!empty($param)) {
			$this->db->where($param);
		}
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		if ($orderBy != '') {
			$this->db->order_by($orderBy, $order);
		}
		$this->db->from('tb_transh');
		$this->db->join('tb_transd', 'tb_transd.NoVoucher = tb_transh.NoVoucher');
		$this->db->join('tb_account', 'tb_account.AccNo = tb_transd.AccNo');

		$getTrans = $this->db->get()->result_array();
		
		return $getTrans;
	}

	public function putApproval($param, $data)
	{
		if (!empty($param) && !empty($data)) {
			$this->db->where($param);
			return $this->db->update('tb_transh', $data);
		}
		else {
			return false;
		}
	}

	public function getVoucher($column, $orderBy='', $type='all', $order='desc')
	{
		if ($type == 'all') {
			if (!empty($param)) {
				$this->db->where($param);
			}
			if (!empty($column)) {
				$this->db->select($column);
			}
			if (!empty($orderBy)) {
				$this->db->order_by($orderBy, $order);
			}
			$getGroup = $this->db->get('tb_transh')->result_array();
		}
		else {
			if (!empty($param)) {
				$this->db->where($param);
			}
			if (!empty($column)) {
				$this->db->select($column);
			}
			if (!empty($orderBy)) {
				$this->db->order_by($orderBy, $order);
			}
			$getGroup = $this->db->get('tb_transh')->row_array();
		}
		
		return $getGroup;
	}

	// public function getProdi($param = array(), $orderBy='', $order='desc')
	// {
	// 	if (!empty($param)) {
	// 		$this->db->where($param);
	// 	}
	// 	if (!empty($orderBy)) {
	// 		$this->db->order_by($orderBy, $order);
	// 	}
	// 	$getProdi = $this->db->get('tb_prodi')->result_array();
		
	// 	return $getProdi;
	// }

	public function getAccounts($param = '')
	{
		if ($param != '') {
			$this->db->like('AccNo', $param);
			$this->db->or_like('AccName', $param);
		}

		$getAccounts = $this->db->get('tb_account')->result_array();
		
		return $getAccounts;
	}

	public function storeHeader($data=array(), $method='insert', $where=array())
	{
		if (!empty($data)) {
			return $this->db->insert('tb_transh', $data);
		}
		else {
			return false;
		}
	}

	public function storeDetail($data=array(), $method='insert', $where=array())
	{
		if (!empty($data)) {
			if ($method == 'insert') {
				return $this->db->insert('tb_transd', $data);
			}
			elseif ($method == 'update') {
				$this->db->where($where);
				return $this->db->update('tb_transd', $data);
			}
		}
		else {
			return false;
		}
	}

	public function dropHeader($voucher='')
	{
		if ($voucher != '') {
			$this->db->where('NoVoucher', $voucher);
			return $this->db->delete('tb_transh');
		}
		else {
			return false;
		}
	}

	public function dropDetail($param=array())
	{
		if (!empty($param)) {
			$this->db->where($param);
			return $this->db->delete('tb_transd');
		}
		else{
			return false;
		}
	}

	// public function storeTemp($param, $field='', $type='add')
	// {
	// 	$where = array(
	// 			'tahun' => date('Y'),
	// 			'bulan' => substr($field, 6, 2)
	// 			);
	// 	$sum = $this->getSum($where, $param);
	// 	$data[$field] = $sum['saldo'];
	// 	// print_r($data[$field]);die();
	// 	if (!empty($param) && !empty($data)) {
	// 		$this->db->where($param);
	// 		return $this->db->update('tb_account', $data);
	// 	}
	// 	else{
	// 		return false;
	// 	}
	// }

	// public function getSum($period=array(), $account=array())
	// {
	// 	// Query 1
	// 	$this->db->select('IF(SUM(tb_transd.debit - tb_transd.kredit) >= 0, SUM(tb_transd.debit - tb_transd.kredit), 0) - IF(SUM(tb_transd.debit - tb_transd.kredit) <= 0, SUM(tb_transd.kredit - tb_transd.debit), 0) AS saldo');
	// 	$this->db->from('tb_transd');
	// 	$this->db->join('tb_transh', 'tb_transh.NoVoucher = tb_transd.NoVoucher', 'left');
	// 	$this->db->where('YEAR(tb_transh.Tanggal)', $period['tahun']);
	// 	$this->db->where('MONTH(tb_transh.Tanggal)', $period['bulan']);

	// 	$this->db->where($account);

	// 	return $this->db->get()->row_array();
	// }
}

/* End of file M_Transaction.php */
/* Location: ./application/models/M_Transaction.php */