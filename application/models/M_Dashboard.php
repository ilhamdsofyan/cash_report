<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Dashboard extends CI_Model {

	public function getAccount($orderBy='', $limit='' , $order='desc')
	{
		if ($orderBy != '') {
			$this->db->order_by($orderBy, $order);
		}
		if ($limit != '') {
			$this->db->limit($limit);
		}
		$getAccount = $this->db->get('tb_user')->result_array();

		return $getAccount;
	}

	public function getLogged($orderBy='', $limit='' , $order='desc')
	{
		if ($orderBy != '') {
			$this->db->order_by($orderBy, $order);
		}
		if ($limit != '') {
			$this->db->limit($limit);
		}
		$getLogged = $this->db->get('tb_log_user')->result_array();
		
		return $getLogged;
	}

	public function getCountMaster($columns)
	{
		if (!empty($columns)) {
			foreach ($columns as $value) {
				$table = $this->db->get($value)->result_array();
				$data[$value] = count($table);
			}
			return $data;
		}
		else {
			return false;
		}
	}
}

/* End of file M_Dashboard.php */
/* Location: ./application/models/M_Dashboard.php */