<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'checkinstallation';

/**
 * Route of Authorize
 */
$route['login'] = 'auth';

/**
 * Route of Dashboard
 */
$route['beranda'] = 'dashboard';

/**
 * Route of Master Account
 */
$route['data-master/bagan-akun'] = 'Master_Akun';
$route['data-master/bagan-akun/ambil']['POST'] = 'Master_Akun/getAkun';
$route['data-master/bagan-akun/simpan']['POST'] = 'Master_Akun/saveAkun';
$route['data-master/bagan-akun/hapus/(:any)'] = 'Master_Akun/deleteAkun';

/**
 * Route of Master Prodi
 */
$route['data-master/program-studi'] = 'Master_Prodi';
$route['data-master/program-studi/ambil']['POST'] = 'Master_Prodi/getProdi';
$route['data-master/program-studi/simpan']['POST'] = 'Master_Prodi/saveProdi';
$route['data-master/program-studi/hapus/(:any)'] = 'Master_Prodi/deleteProdi';

/**
 * Route of Master Type of User
 */
$route['data-master/jenis-pengguna'] = 'Master_UserType';
$route['data-master/jenis-pengguna/ambil']['POST'] = 'Master_UserType/getGroup';
$route['data-master/jenis-pengguna/simpan']['POST'] = 'Master_UserType/saveGroup';
$route['data-master/jenis-pengguna/hapus/(:any)'] = 'Master_UserType/deleteGroup';

/**
 * Route of Master User
 */
$route['data-master/pengguna'] = 'Master_User';
$route['data-master/pengguna/ambil']['POST'] = 'Master_User/getUser';
$route['data-master/pengguna/simpan']['POST'] = 'Master_User/saveUser';
$route['data-master/pengguna/hapus/(:any)'] = 'Master_User/deleteUser';

/**
 * Route of User
 */
$route['profil'] = 'My_Profile';
$route['profil/simpan']['POST'] = 'My_Profile/save';
$route['profil/ubah_login']['POST'] = 'My_Profile/save_login';

/**
 * Route of Transaction
 */
$route['transaksi'] = 'Transaction';
$route['transaksi/ambil']['POST'] = 'Transaction/getTransaction';
$route['transaksi/setujui']['POST'] = 'Transaction/approval';
$route['input-keuangan'] = 'Transaction/addTransaction';
$route['input-keuangan/akun/ambil'] = 'Transaction/getAccount';
$route['input-keuangan/simpan'] = 'Transaction/saveTransaction';
$route['transaksi/ubah/(:any)']['GET'] = 'Transaction/editTransaction/$1';
$route['transaksi/ubah/simpan']['POST'] = 'Transaction/updateTransaction';
$route['transaksi/ubah/tambah']['POST'] = 'Transaction/addNewDetail';
$route['transaksi/ubah/hapus']['POST'] = 'Transaction/deleteDetail';

/**
 * Route of Report
 */
$route['laporan'] = 'Report';

/**
 * Route of Journal
 */
$route['laporan/jurnal-umum'] = 'journal';
// $route['laporan/jurnal-umum/ambil-akun'] = 'journal/getAccount';
$route['laporan/jurnal-umum/tampil'] = 'journal/showJournal';

/**
 * Route of Ledger
 */
$route['laporan/buku-besar'] = 'Ledger';
$route['laporan/buku-besar/ambil-akun']['POST'] = 'Ledger/getAccount';
$route['laporan/buku-besar/tampil']['POST'] = 'Ledger/showLedger';

/**
 * Route of Trial Balance
 */
$route['laporan/neraca-saldo'] = 'Trial_Balance';
$route['laporan/neraca-saldo/ambil-bulan']['POST'] = 'Trial_Balance/getMonth';
$route['laporan/neraca-saldo/ambil-akun']['POST'] = 'Trial_Balance/getAccount';
$route['laporan/neraca-saldo/tampil']['POST'] = 'Trial_Balance/showTrialBalance';

/**
 * Route of Profit and Loss
 */
$route['laporan/laba-rugi'] = 'Profit_Loss';
$route['laporan/laba-rugi/ambil-bulan']['POST'] = 'Profit_Loss/getMonth';
$route['laporan/laba-rugi/tampil']['POST'] = 'Profit_Loss/showProfLoss';

/**
 * Route of Balance
 */
$route['laporan/neraca'] = 'Balance';
$route['laporan/neraca/ambil-bulan']['POST'] = 'Balance/getMonth';
$route['laporan/neraca/tampil']['POST'] = 'Balance/showBalance';

/**
 * Route of Override and Translate Uri Dashes
 */
$route['404_override'] = 'auth/notFound';
$route['translate_uri_dashes'] = FALSE;
