<!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo ROOT_URL ?>/assets/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo ROOT_URL ?>/assets/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
		<!-- FOR NUMBER AND PRICE FORMAT PLUGINS -->
		<script src="<?php echo ROOT_URL ?>/assets/jquery-priceformat/jquery-priceformat.js" type="text/javascript"></script>
		<script type="text/javascript">
			$('.price-format').priceFormat({
		          prefix: 'Rp.',
		          thousandsSeparator: ',',
		          centsLimit: 0
		    });
		    $('.number-format').priceFormat({
		          prefix: '',
		          thousandsSeparator: '',
		          centsLimit: 0
		    });
		</script>
		<!-- END NUMBER AND PRICE FORMAT PLUGINS -->