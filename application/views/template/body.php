<!-- BEGIN BODY -->
    <body class="page-container-bg-solid page-boxed">
        <!-- BEGIN HEADER -->
        <?php $this->load->view('template/header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <?php $this->load->view('template/container.php'); ?>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('template/footer.php'); ?>
        <!-- END FOOTER -->
        
        <!-- BEGIN CORE PLUGINS -->
        <?php $this->load->view('template/js_core_plugins.php'); ?>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <?php $this->load->view($content_js); ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN JS GLOBAL SCRIPT -->
        <?php $this->load->view('template/js_global_script.php'); ?>
        <!-- END JS GLOBAL SCRIPT -->
    </body>
<!-- END BODY -->
</html>