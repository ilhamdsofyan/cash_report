<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="utf-8" />
		<title><?php echo APP_NAME ?> | <?php echo $content_title ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN PAGE LEVEL STYLES -->
		<link href="<?php echo ROOT_URL ?>/assets/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT_URL ?>/assets/assets/pages/css/smartphone.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
		<!-- END PAGE LEVEL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<!-- END THEME LAYOUT STYLES -->
		<link rel="icon" type="image/png" href="<?php echo APP_FAVICON ?>" />
		</head>
	<!-- END HEAD -->

	<body class="login">
		<!-- BEGIN : LOGIN PAGE 5-1 -->
		<div class="user-login-5">
			<div class="row bs-reset">
				<div class="col-md-6 col-xs-12 bs-reset login-bg">
					<div class="smartphone">
						<div class="smartphone-camera"></div>
						<div class="smartphone-speaker"></div>
						<div class="smartphone-screen">
							<div class="smartphone-screen-in" >
								<div style="padding:10px;padding-top: 0px;" class="layer-dynamic">
									<!-- BEGIN LOGIN FORM -->
									<form action="<?php echo site_url('auth/doAuth') ?>" class="login-form" method="post">  
										<h3 class="uppercase" align="center">Login</h3>
										<hr>
										<div class="row">
											<div class="col-xs-12">
												<input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="user[username]" value="<?php echo $this->session->flashdata('input') ?>" required/> </div>
										</div>    
										<div class="row">
											<div class="col-xs-12">
												<input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="user[passwd]" required/> </div>
										</div>
										<div class="row">
											<div class="col-sm-12 text-right">
												<button class="btn blue" type="submit">Log In</button>
											</div>
										</div>
									</form>
									<!-- END LOGIN FORM -->
								</div>
							</div>
						</div>
						<div class="smartphone-home"></div>
					</div>
				</div>
				<div class="col-md-6 login-container bs-reset" style="border-left-style: dashed">
					<div class="login-content">
						<div class="row" style="margin-top:-20px;">
							<div class="col-md-6 col-xs-12">
								<img align="left" src="<?php echo APP_LOGO ?>" style="margin-top: 10px;width: 250px"/>
							</div>
						</div>
						<hr>
						<div class="login-desc">
							<p>Fasilitas ini diperuntukkan untuk divisi keuangan dan <i>Board of Director</i>. Adapun fasilitas yang terdapat di dalam <b><?php echo APP_NAME ?></b> ini sebagai berikut :</p>
							<ul>
								<li>Input Data Keuangan</li>
								<li>Pembuatan Buku Besar</li>
								<li>Pembuatan Neraca Saldo</li>
								<li>Menentukan Laba/Rugi</li>
								<li>Pembuatan Neraca</li>
							</ul>
							<p>Untuk mendapatkan fasilitas diatas anda harus login terlebih dahulu sesuai dengan user id dan sandinya masing-masing. Bagi yang tidak bisa login kedalam <b><?php echo APP_NAME ?></b> ini, di harapkan untuk melapor pada Administrator masing-masing untuk didaftarkan akunnya.</p>
						</div>
					</div>
					<div class="login-footer">
						<div class="row bs-reset">
							<!-- <div class="col-xs-1 bs-reset">
								<ul class="login-social">
									<li>
										<a href="javascript:;">
											<i class="icon-social-facebook"></i>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<i class="icon-social-twitter"></i>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<i class="icon-social-dribbble"></i>
										</a>
									</li>
								</ul>
							</div> -->
							<div class="col-xs-11 bs-reset">
								<div class="login-copyright text-right">
									<p><b><?php echo APP_NAME ?> version <?php echo APP_VERSION ?> | Copyright &copy; <?php echo CREATED_AT ?> | <?php echo APP_OWNER ?></b></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END : LOGIN PAGE 5-1 -->
		<!--[if lt IE 9]>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
		<script src="<?php echo ROOT_URL ?>/assets/assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<!-- END THEME LAYOUT SCRIPTS -->
		<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
		<?php
		if ($this->session->flashdata('message') != ''):
		?>
		<script type="text/javascript">
		jQuery(document).ready(function() {       
		   toastr.info('<?php echo $this->session->flashdata('message') ?>', 'Notifikasi', 
			{
				timeOut: 40000,
				closeButton: true,
				showMethod: "slideDown",
				hideMethod: "slideUp",
				positionClass: "toast-bottom-left"
			})
		});
		</script>
		<?php 
		endif;
		?>
		<script type="text/javascript">
		$(".mask_date").inputmask("y-m-d", {
			autoUnmask: false,
			"placeholder": "yyyy-mm-dd"
		});
		jQuery("#verify").click(function() {
			nim = jQuery("#signup-nim").val();
			birth = jQuery("#signup-birth").val();
			spinner = '<img id="preloader" src="<?php echo ROOT_URL ?>/assets/assets/pages/img/loading-spinner-blue.gif" class="pull-right" style="margin-right:10px;margin-top:5px;">';
			jQuery(".signup-form .form-actions").append(spinner);
			jQuery(".signup-form #verify").attr('disabled', true);
			jQuery.ajax({
				type :"POST",
				data :{nim:nim,birth:birth},
				url :"<?php echo site_url('auth/verifySignup/') ?>",
				dataType : "json",
				success : function (val){
					if (val == '0'){
						var userpwd = '<div class="form-group input-pwd">' +
									'<label for="signup-pwd">Masukan Sandi(maksimal 7 karakter)</label>' +
									'<input class="form-control placeholder-no-fix form-group" type="password" placeholder="Sandi" name="signup[pwd]" id="signup-pwd" maxlength="7" /> ' +
									'</div>'
						var button = '<button type="submit" class="btn blue btn-success pull-right" id="btn-submit">Submit</button>'
						jQuery(".signup-form .form-actions #verify").remove();
						jQuery(".signup-form #signup-birth").remove();
						jQuery(".signup-form .form-actions #preloader").remove();
						jQuery(".signup-form .back-btn").hide();

						jQuery(".signup-form .form-actions").append(userpwd);
						jQuery(".signup-form .form-actions").append(button);
					}
					else if (val == '1'){
						notification('Akun anda sudah dibuat');
						jQuery(".signup-form .form-actions #preloader").remove();
						jQuery(".signup-form #verify").attr('disabled', false);
					}
					else {
						notification('Maaf, kami tidak menemukan anda pada database kami atau anda sudah tidak aktif lagi pada kampus yang bersangkutan. Silahkan hubungi ke bagian akademik di kampus masing-masing');
						jQuery(".signup-form .form-actions #preloader").remove();
						jQuery(".signup-form #verify").attr('disabled', false);
					}
					function notification(msg){
						toastr.info(msg, 'Notifikasi', 
						{
							timeOut: 40000,
							closeButton: true,
							showMethod: "slideDown",
							hideMethod: "slideUp",
							positionClass: "toast-bottom-left"
						})
					}
				}
			});
		});

		// check window width
		$(document).ready(function(){
			if ($(document).width() < 900) {
				$(".login-container").hide();
			}
			else {
				$(".login-container").show();	
			}
		});
		$(window).resize(function(){
			if ($(document).width() < 900) {
				$(".login-container").hide();
			}
			else {
				$(".login-container").show();	
			}
		})
		</script>
		<!-- <script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/validate.js" type="text/javascript"></script> -->
</body>

</html>