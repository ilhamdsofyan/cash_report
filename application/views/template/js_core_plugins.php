<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
 <script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
	   <?php
		if ($this->session->flashdata('message') != ''):
		?>
		<script type="text/javascript">
		jQuery(document).ready(function() {       
		   toastr.info('<?php echo $this->session->flashdata('message') ?>', 'Notifikasi', 
			{
				timeOut: 40000,
				closeButton: true,
				showMethod: "slideDown",
				hideMethod: "slideUp",
				positionClass: "toast-bottom-left"
			})
		});
		</script>
		<?php 
		endif;
		?>

<?php if ($biodata['user_type'] != 1): ?>
	<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/validate.js" type="text/javascript"></script>
<?php endif ?>