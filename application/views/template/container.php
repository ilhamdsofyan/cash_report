<div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <?php $this->load->view('template/page_head.php') ?>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        
                        <!-- END PAGE BREADCRUMBS -->
                        <?php if ($content_title == 'Laporan' && isset($content_subtitle)): ?>
                            <?php 
                                $submenus = array(
                                        'journal' => array(
                                                    'title' => 'Jurnal Umum',
                                                    'link' => 'jurnal-umum',
                                                    'icon' => 'fa-bookmark-o'
                                                    ),
                                        'ledger' => array(
                                                    'title' => 'Buku Besar',
                                                    'link' => 'buku-besar',
                                                    'icon' => 'fa-book'
                                                    ),
                                        'triBal' => array(
                                                    'title' => 'Neraca Saldo',
                                                    'link' => 'neraca-saldo',
                                                    'icon' => 'fa-balance-scale'
                                                    ),
                                        'proLoss' => array(
                                                    'title' => 'Laba Rugi',
                                                    'link' => 'laba-rugi',
                                                    'icon' => 'fa-list'
                                                    ),
                                        'balance' => array(
                                                    'title' => 'Neraca',
                                                    'link' => 'neraca',
                                                    'icon' => 'fa-university'
                                                    ),
                                        );
                             ?>
                            <div class="row">
                                <div class="col-lg-3 col-xs-12">
                                    <div class="list-group">
                                        <?php foreach ($submenus as $key => $value): ?>
                                            <?php $active = ($content_subtitle == $value['title']) ? 'active' : '' ?>
                                            <a class="list-group-item <?php echo $active ?>" href="<?php echo $value['link'] ?>"><i class="fa <?php echo $value['icon'] ?> fa-fw" aria-hidden="true"></i>&nbsp; <?php echo $value['title'] ?></a>                                        
                                        <?php endforeach ?>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-xs-12">
                                    
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <?php $this->load->view($content_file) ?>
                            <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                        <?php elseif ($content_title == 'Data Master' && isset($content_subtitle)): ?>
                            <?php 
                                $submenus = array(
                                        'account' => array(
                                                    'title' => 'Bagan Akun',
                                                    'link' => 'bagan-akun',
                                                    'icon' => 'fa-list-ol'
                                                    ),
                                        // 'prodi' => array(
                                        //             'title' => 'Program Studi',
                                        //             'link' => 'program-studi',
                                        //             'icon' => 'fa-building'
                                        //             ),
                                        'user' => array(
                                                    'title' => 'Pengguna',
                                                    'link' => 'pengguna',
                                                    'icon' => 'fa-user'
                                                    ),
                                        'type' => array(
                                                    'title' => 'Jenis Pengguna',
                                                    'link' => 'jenis-pengguna',
                                                    'icon' => 'fa-group'
                                                    ),
                                        );
                                if ($biodata['user_type'] != 1) {
                                    unset($submenus['user']);
                                    unset($submenus['type']);
                                }
                             ?>
                            <div class="row">
                                <div class="col-lg-3 col-xs-12">
                                    <div class="list-group">
                                        <?php foreach ($submenus as $key => $value): ?>
                                            <?php $active = ($content_subtitle == $value['title']) ? 'active' : '' ?>
                                            <a class="list-group-item <?php echo $active ?>" href="<?php echo $value['link'] ?>"><i class="fa <?php echo $value['icon'] ?> fa-fw" aria-hidden="true"></i>&nbsp; <?php echo $value['title'] ?></a>                                        
                                        <?php endforeach ?>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-xs-12">
                                    
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <?php $this->load->view($content_file) ?>
                            <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                        <?php else: ?>
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <?php $this->load->view($content_file) ?>
                            <!-- END PAGE CONTENT INNER -->
                        <?php endif ?>
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>