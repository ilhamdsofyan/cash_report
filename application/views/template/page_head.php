<div class="page-head">
					<div class="container">
						<!-- BEGIN PAGE TITLE -->
						<div class="page-title">
							<h1> <?php echo $content_title . ' ' . @$content_subtitle ?> </h1>
						</div>

						<!-- END PAGE TITLE -->
						<!-- BEGIN PAGE TOOLBAR -->
						<?php $this->load->view('template/page_toolbar.php') ?>
						<!-- END PAGE TOOLBAR -->
					</div>
				</div>
