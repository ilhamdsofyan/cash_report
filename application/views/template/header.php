<?php 
    if ($biodata['user_image'] == '') {
        $foto = ROOT_URL . '/assets/images/user.jpg';
    }
    else {
        $foto = ROOT_URL . '/assets/' . $biodata['user_image'];
    }
 ?>
<div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="">
                            <img src="<?php echo ROOT_URL ?>/assets/images/logo-small.png" alt="logo" class="logo-default image-zoom-ease" style="margin-top:12px;width:200px;">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="<?php echo $biodata['realname'] ?>" class="img-circle" src="<?php echo $foto ?>">
                                    <span class="username username-hide-mobile" id="usermhs" ><?php echo $biodata['realname'] ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?php echo site_url('profil'); ?>">
                                            <i class="icon-user"></i> Profil Biodata </a>
                                    </li>
                                   
                                    <li>
                                        <a href="<?php echo site_url('auth/logout'); ?> ">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu  ">
                        <ul class="nav navbar-nav">
                        <?php 
                        $menus = array(
                                'dashboard' => array(
                                    'title' => 'Beranda',
                                    'link' => site_url('beranda'),
                                    ),
                                'master' => array(
                                    'title' => 'Data Master',
                                    'link' => site_url('data-master/bagan-akun'),
                                    ),
                                'transaction' => array(
                                    'title' => 'Transaksi',
                                    'link' => site_url('transaksi'),
                                    ),
                                'report' => array(
                                    'title' => 'Laporan',
                                    'link' => site_url('laporan'),
                                    ),
                            );
                        if ($biodata['user_type'] == 3 || $biodata['user_type'] == 4) {
                            unset($menus['master']);
                            unset($menus['transaction']);
                        }
                        if ($biodata['user_type'] == 2) {
                            unset($menus['master']);
                            unset($menus['report']);
                        }
                        foreach ($menus as $key => $value) :
                            $active = ($content_title == $value['title']) ? 'active' : '' ; 
                        ?>
                            <li class="menu-dropdown classic-menu-dropdown <?php echo $active ?>">
                                <a href="<?php echo $value['link'] ?>"> <?php echo $value['title'] ?>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                        <?php    
                        endforeach;
                        ?>
                           
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>