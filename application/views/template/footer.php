        <!-- BEGIN PRE-FOOTER -->
       
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container"> <?php echo APP_NAME ?> version <?php echo APP_VERSION ?> | Copyright &copy; <?php echo CREATED_AT ?> | <?php echo APP_OWNER ?>
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->