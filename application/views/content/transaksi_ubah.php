	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
					<div class="portlet-body">
						<!-- <form name="form-transaction" action="input-keuangan/simpan" method="post"> -->
							<div class="form-group">
								<div class="row">
									<!-- <div class="col-lg-2 col-xs-12" disabled hidden>
										<label for="input-prodi">Program Studi</label>
									</div>
									<div class="col-lg-4 col-xs-12" disabled hidden>
										<select name="header[kodeprodi]" class="form-control" id="input-prodi" required>
											<option value="" selected disabled>-- Silahkan Pilih Program Studi --</option>
											<?php foreach ($prodi as $key => $value): ?>
												<option value="<?php echo $value['kodeprodi'] ?>"> <?php echo $value['namaprodi'] ?> </option>
											<?php endforeach ?>
										</select>
									</div> -->
									<div class="col-lg-2 col-xs-12">
										<label for="input-code">Nomor Voucher</label>
									</div>
									<div class="col-lg-4 col-xs-12">
										<input type="text" name="header[NoVoucher]" id="input-code" class="form-control" value="<?php echo $header['NoVoucher'] ?>" readonly required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-2 col-xs-12">
										<label for="input-date">Keterangan</label>
									</div>
									<div class="col-lg-4 col-xs-12">
										<textarea name="header[deskripsi]" class="form-control" required style="resize: none; overflow-y: auto;min-height: 55px"><?php echo $header['deskripsi'] ?></textarea>
										<small><i>Ketikkan '-' (tanpa kutip) jika tidak ada.</i></small>
									</div>
									<div class="col-lg-2 col-xs-12">
										<label for="input-date">Tanggal</label>
									</div>
									<div class="col-lg-4 col-xs-12">
										<input type="text" name="header[Tanggal]" id="input-date" class="form-control mask_date" value="<?php echo $header['Tanggal'] ?>" required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
								</div>
							</div>
							<div class="form-group">
								<div class="row">
								</div>
							</div>
							<form method="post" action="tambah" id="form-detail">
								<fieldset>
									<legend></legend>
									<div class="row" align="right">
										<div class="col-lg-4 col-xs-12">
											<input type="text" name="display" id="input-account" class="form-control" placeholder="No Akun - Nama Akun">
											<input type="hidden" name="detail[AccNo]" id="account-id">
											<input type="hidden" name="account_name" id="account-name">
											<input type="hidden" name="detail[NoVoucher]" id="novoucher" value="<?php echo $header['NoVoucher'] ?>">
											<small><i>Ketikkan dan pilih sesuai referensi.</i></small>
										</div>
										<div class="col-lg-3 col-xs-12">
											<input type="text" name="detail[debit]" id="input-debit" class="form-control number-format" placeholder="Debit">
											<small><i>Ketikkan angka 0 jika tidak ada.</i></small>
										</div>
										<div class="col-lg-3 col-xs-12">
											<input type="text" name="detai[kredit]" id="input-kredit" class="form-control number-format" placeholder="Kredit">
											<small><i>Ketikkan angka 0 jika tidak ada.</i></small>
										</div>
										<div class="col-lg-2 col-xs-12">
											<button class="btn btn-info" id="insert_detail" type="button"><i class="fa fa-plus"></i>&nbsp;Sisipkan</button>
										</div>
									</div>
								</fieldset>
							</form>
							<hr>
							<div class="table-responsive">
								<table class="table table-condense table-bordered" id="table-detail">
									<thead>
										<tr>
											<th>Nomor Akun</th>
											<th>Nama Akun</th>
											<th>Debit</th>
											<th>Kredit</th>
											<th>#</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($detail as $key => $value): ?>
											<tr>
												<td>
													<input type='text' name='detail[<?php echo $key ?>][AccNo]' value='<?php echo $value['AccNo'] ?>' id='input-accno<?php echo $key ?>' class='form-control accno' required>
												</td>
												<td>
													<input type='text' name='AccName<?php echo $key ?>' value='<?php echo $value['AccName'] ?>' id='input-accname<?php echo $key ?>' class='form-control' readonly required>
												</td>
												<td>
													<input type='number' name='detail[<?php echo $key ?>][debit]' value='<?php echo $value['debit'] ?>' id='input-debit<?php echo $key ?>' class='form-control' required>
												</td>
												<td>
													<input type='number' name='detail[<?php echo $key ?>][kredit]' value='<?php echo $value['kredit'] ?>' id='input-kredit<?php echo $key ?>' class='form-control' required>
												</td>
												<td>
													<input type='hidden' name='detail_<?php echo $key ?>_id' value='<?php echo $value['id'] ?>' id='input-id<?php echo $key ?>' required>
													<button type="button" class="btn btn-warning btn_edit"><i class='fa fa-edit'></i>&nbsp;Revisi</button>
													<a href='javascript:void(0);' class='btn btn-danger delete_row'><i class='fa fa-trash-o'></i>&nbsp;Hapus</a>
												</td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-6" align="left">
										<a href="../../transaksi" class="btn btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
									</div>
									<div class="col-lg-6" align="right">
										<!-- <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane"></i>&nbsp;Simpan</button> -->
										<!-- <input type="submit" name="submit" class="btn btn-success" value="Simpan">	 -->
									</div>
								</div>
							</div>
						<!-- </form> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>