<div class="portlet light">
	<div class="portlet-title tabbable-line">
		<div class="caption caption-md">
			<span class="caption-subject font-blue-madison bold uppercase">Informasi Jurnal Umum</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="form-group">
			<label class="control-label">Pilih Jangka Waktu</label>
			<input type="text" id="select-date" class="form-control daterange-picker">
		</div>
		<!-- <div class="form-group">
			<label class="control-label">Pilih Nomor Akun</label>
			<select class="form-control" id="select-account" disabled>
				<option value="" selected></option>
			</select>
		</div> -->
		<button class="btn btn-primary" id="show-journal">Tampilkan</button>
	</div>
</div>
<div id="journal-content"></div>