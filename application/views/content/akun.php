	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
				   
					<div class="portlet-body">
						<div align="right">
							<button class="btn btn-primary" id="add" data-toggle="modal" data-target="#modal"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12">
								<!-- <div class="table-responsive"> -->
									<table class="table table-striped table-condensed table-hover" id="example">
										<thead>
											<tr>
												<th>No</th>
												<th>No. Akun</th>
												<th>Nama Akun</th>
												<th>Grup</th>
												<th class="col-lg-3">#</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$i = 1;
												foreach ($account as $key => $value) {
											 ?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $value['AccNo'] ?></td>
													<td><?php echo $value['AccName'] ?></td>
													<td><?php echo $value['AccGroup'] ?></td>
													<td align="center">
														<button class="btn btn-warning edit" id="<?php echo $value['AccNo'] ?>" data-toggle="modal" data-target="#modal"><i class="fa fa-pencil"></i>&nbsp; Ubah</button>
														<a href="bagan-akun/hapus/<?php echo $value['AccNo'] ?>" class="btn btn-danger" onclick="return confirm('Jika anda menghapus akun ini, maka semua transaksi yang berhubungan dengan akun ini akan dihapus!')"><i class="fa fa-trash"></i>&nbsp; Hapus</a>
														<!-- <button class="btn btn-danger" id="delete"><i class="fa fa-trash"></i>&nbsp; Hapus</button> -->
													</td>
												</tr>
											<?php
													$i++; 
												}
											 ?>
										</tbody>
									</table>
								<!-- </div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					<!-- <img id="loading-image" src="<?php echo ROOT_URL ?>/assets/assets/pages/img/loading-spinner-blue.gif"> -->
						<form id="form-account" action="bagan-akun/simpan" method="post">
							<input type="hidden" name="_method" value="">
							<input type="hidden" name="input-id" value="">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-code">Nomor Akun</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[AccNo]" id="input-code" class="form-control">
										<small><i>Contoh : 110001</i></small>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-name">Nama Akun</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[AccName]" id="input-name" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-group">Grup Akun</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[AccGroup]" id="input-group" class="form-control">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit" form="form-account" class="btn btn-success">Simpan</button>
						<button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
			</div>
</div>