	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
				   
					<div class="portlet-body">
						<div align="right">
							<button class="btn btn-primary" id="add" data-toggle="modal" data-target="#modal"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-stiped table-bordered">
										<thead style="background-color: #2b609e;color: #fff">
											<tr>
												<th style="vertical-align: middle;">No</th>
												<th><i>Username</i></th>
												<th>Nama Pengguna</th>
												<th><i>Email</i></th>
												<th>Tipe</th>
												<th>Dibuat Pada</th>
												<th>Diubah Pada</th>
												<th>#</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												if (!empty($user)) {
													$i = 1;
													foreach ($user as $key => $value) {
											 ?>
													<tr>
														<td><?php echo $i ?></td>
														<td><?php echo $value['username'] ?></td>
														<td><?php echo $value['realname'] ?></td>
														<td><?php echo $value['email'] ?></td>
														<td><?php echo $value['group_name'] ?></td>
														<td><?php echo $value['dibuat']; ?></td>
														<td><?php echo $value['diubah']; ?></td>
														<td>
															<button class="btn btn-warning btn-block edit" id="<?php echo $value['user_id'] ?>" data-toggle="modal" data-target="#modal"><i class="fa fa-pencil"></i>&nbsp; Ubah</button>
															<a href="pengguna/hapus/<?php echo $value['user_id'] ?>" class="btn btn-danger btn-block" onclick="return confirm('Yakin ingin dihapus?')"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
														</td>
													</tr>
											<?php 
														$i++;
													}
												}
												else {
											?>
													<tr>
														<td colspan="4"><i>Data Tidak Ada.</i></td>
													</tr>
											<?php
												}
											 ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					<!-- <img id="loading-image" src="<?php echo ROOT_URL ?>/assets/assets/pages/img/loading-spinner-blue.gif"> -->
						<form action="pengguna/simpan" method="post" id="form-pengguna">
							<input type="hidden" name="_method" value="insert">
							<input type="hidden" name="id" id="input-id">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-name">Nama Pengguna</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[realname]" id="input-name" class="form-control" required>
										<small><i>Masukkan Nama Lengkap</i></small>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-user"><i>Username</i></label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[username]" id="input-user" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-user"><i>Password</i></label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<div class="input-group">
											<input type="password" name="data[passwd]" id="input-passwd" class="form-control" required>
											<span class="input-group-btn">
												<button class="btn btn-info" id="show-pass" type="button"><i class="fa fa-eye s"></i></button>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-type">Hak Akses</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<select name="data[user_type]" id="input-type" class="form-control" required>
											<option value="" selected disabled> -- Pilih Hak Akses Sebagai -- </option>
											<?php foreach ($group as $key => $value): ?>
												<option value="<?php echo $value['group_id'] ?>"> <?php echo $value['group_name'] ?> </option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit" form='form-pengguna' class="btn btn-success btn-outline">Simpan</button>
						<button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
			</div>
</div>