<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN Portlet PORTLET-->
			<div class="portlet light">
			   
				<div class="portlet-body">
					<div align="right">
						<a href="input-keuangan" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<!-- <div class="table-responsive"> -->
								<table class="table table-striped table-condensed table-hover" id="example">
									<thead>
										<tr>
											<th>No Voucher</th>
											<th>Tanggal</th>
											<th>Keterangan</th>
											<th>Operator</th>
											<th class="col-lg-1">#</th>
											<?php if ($biodata['user_type'] == 1): ?>
												<th>Approval</th>
											<?php endif ?>
										</tr>
									</thead>
									<tbody>
										<?php 
											$i = 1;
											foreach ($transaction as $key) {
											// $check = ($key['approve'] == 1) ? 'checked' : '';
											$text = ($key['approve'] == 1) ? 'Disetujui' : 'Belum Disetujui';
											$style = ($key['approve'] == 1) ? "style='color:green'" : "style='color:red'";
										 ?>
											<tr>
												<td><?php echo $key['NoVoucher'] ?></td>
												<td><?php echo $key['Tanggal'] ?></td>
												<td><?php echo $key['deskripsi'] ?></td>
												<td><?php echo $key['realname'] ?></td>
													<td align="center">
														<a href="transaksi/ubah/<?php echo base64_encode($key['NoVoucher']) ?>" class="btn btn-warning btn-block"><i class="fa fa-pencil"></i>&nbsp;Ubah</a>
														<?php if ($biodata['user_type'] == 1): ?>
															<a name="<?php echo $i ?>" class="btn btn-primary btn-block check" id="<?php echo $key['NoVoucher'] ?>" data-toggle="modal" data-target="#modal"><i class="fa fa-eye"></i>&nbsp;Lihat</a>
														<?php endif ?>
													</td>
												<?php if ($biodata['user_type'] == 1): ?>
													<td align="center" id="approval<?php echo $i ?>" <?php echo $style ?>><?php echo $text ?></td>
												<?php endif ?>
											</tr>
										<?php
												$i++; 
											}
										 ?>
									</tbody>
								</table>
							<!-- </div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="row form-group">
					<div class="col-lg-3 col-xs-12">
						<label><b>No Voucher</b></label>
					</div>
					<div class="col-lg-3 col-xs-12">
						<label id="NoVoucher"></label>
					</div>
					<div class="col-lg-3 col-xs-12">
						<label><b>Operator</b></label>
					</div>
					<div class="col-lg-3 col-xs-12">
						<label id="realname"></label>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-3 col-xs-12">
						<label><b>Tanggal Transaksi</b></label>
					</div>
					<div class="col-lg-3 col-xs-12">
						<label id="Tanggal"></label>
					</div>
					<div class="col-lg-3 col-xs-12">
						<label><b>Tanggal Input</b></label>
					</div>
					<div class="col-lg-3 col-xs-12">
						<label id="Tanggal_input"></label>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-3 col-xs-12">
						<label><b>Keterangan</b></label>
					</div>
					<div class="col-lg-9 col-xs-12">
						<label id="deskripsi"></label>
					</div>
				</div>
				<fieldset>
					<legend>Transaksi</legend>
					<div class="table-responsive">
						<table class="table table-condensed table-bordered" id="table_modal">
							<thead>
								<tr>
									<th>No</th>
									<th>Nomor Akun</th>
									<th>Nama Akun</th>
									<th>Debit</th>
									<th>Kredit</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning approve" name="#" id="#">Setujui</button>
				<button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>