<script type="text/javascript">
	$(document).ready(function(){
		$('#select-year').change(function() {
			$.ajax({
				url: 'laba-rugi/ambil-bulan',
				type: 'post',
				data: {tahun:$(this).val()},
				dataType: 'json',
				success: function(data) {
					$('#select-month').empty();
					$('#select-month').prop('disabled', false);
					$('#select-month').append('<option value="" selected disabled> - Silahkan Pilih Bulan - </option>');
				
					$.each(data, function(key, value){
						var monthID = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
						var select = '<option value="' + value.bulan + '"> ' + monthID[value.bulan] + ' </option>';
						$('#select-month').append(select);
					});
				},
				error: function(data) {
					// body...
				}
			})
		});

		$('#show-profloss').click(function(){
			var year = $('#select-year').val();
			var month = $('#select-month').val();
			
			if (year == null || month == null) {
				toastr.info('Silahkan isi semua parameter pencarian', 'Notifikasi', 
				{
					timeOut: 40000,
					closeButton: true,
					showMethod: "slideDown",
					hideMethod: "slideUp",
					positionClass: "toast-bottom-left"
				});
				return false;
			}

			$.ajax({
				url: 'laba-rugi/tampil',
				type: 'post',
				data: {tahun:year, bulan:month},
				success: function(view){
					$('#content-profloss').empty();
					$('#content-profloss').append(view);

					$("#btn-print").click(function(){
						var content = $("#content-print")[0];
						var windows = window.open('','Print-Window');
						windows.document.open();
						windows.document.write('<html><head><title>Laporan Laba Rugi</title><link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /></head><body onload="window.print()">'+content.innerHTML+'</body></html>');
						windows.document.close();
					});
				},
				error: function(view){

				}
			})
		});
	})
</script>