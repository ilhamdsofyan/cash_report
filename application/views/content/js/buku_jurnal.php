<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/moment.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/select2/select2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".daterange-picker").daterangepicker({
			locale: {
			  format: 'YYYY-MM-DD'
			},
		});

		$("#select-account").select2({
			placeholder: "-- Silahkan Pilih Nomor Akun --",
			width: '100%'
		});

		// $("#select-date").change(function(){
		// 	var date = $(this).val();
		// 	var mulai = date.substr(0, 10);
		// 	var akhir = date.substr(13, 10);
		// 	$.ajax({
		// 		url: 'jurnal-umum/ambil-akun',
		// 		type: 'post',
		// 		data: {range:{start:mulai, end:akhir}},
		// 		dataType: 'json',
		// 		success: function(data) {
		// 			$("#select-account").empty();
		// 			$("#select-account").prop("disabled", false);
		// 			$("#select-account").append('<option value="semua">Semua Akun</option>');
		// 			$.each(data, function(key, value){
		// 				$("#select-account").append('</option><option value="' + value.AccNo + '"> ' + value.AccNo + ' - ' + value.AccName + ' </option>');
		// 			})
		// 		},
		// 		error: function() {

		// 		}
		// 	})
		// });

		$("#show-journal").click(function(){
			$("#journal-content").slideUp('fast');
			var date = $("#select-date").val();
			// var account = $("#select-account").val();
			var mulai = date.substr(0, 10);
			var akhir = date.substr(13, 10);
			var data;
			// if (account != '') {
				$("#journal-content").empty();
				data = {range:{start:mulai, end:akhir}};
				$.ajax({
					url: 'jurnal-umum/tampil',
					data: data,
					type: 'post',
					success: function(value) {
						$("#journal-content").slideDown('slow');
						$("#journal-content").append(value);
						
						$("#btn-print").click(function(){
							var content = $("#content-print")[0];
							var windows = window.open('','Print-Window');
							windows.document.open();
							windows.document.write('<html><head><title>Laporan Buku Jurnal</title><link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /></head><body onload="window.print()">'+content.innerHTML+'</body></html>');
							windows.document.close();
						});
					},
					error: function(value) {
						$("#journal-content").append("<h1>Data tidak ditemukan, silahkan coba lagi.</h1>");
					}
				});
			// }
			// else {
				// toastr.info('Maaf, anda tidak memilih satupun parameter', 'Notifikasi', 
				// 	{
				// 		timeOut: 40000,
				// 		closeButton: true,
				// 		showMethod: "slideDown",
				// 		hideMethod: "slideUp",
				// 		positionClass: "toast-bottom-left"
				// 	})
				// return false;
			// }
		});

	});
</script>