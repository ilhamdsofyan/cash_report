<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".mask_date").inputmask("y-m-d", {
			autoUnmask: false,
			"placeholder": "yyyy-mm-dd"
		});

		$("#btn-pw-save").click(function(){
			var pw1 = $("#pw1").val();
			var pw2 = $("#pw2").val();
			if ($(".uname").val() == '') {
				toastr.info('Username tidak boleh kosong', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
	            return $(".uname").focus();
			}
			if ($(".pwd").val() == '') {
				toastr.info('Password Lama tidak boleh kosong', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
	            return $(".pwd").focus();
			}
			if ($(".uname").val() != "<?php echo $biodata['username'] ?>") {
				if(confirm("Yakin ingin merubah username? anda akan keluar aplikasi!")){
					if (pw1 != pw2){
						toastr.info('Kata sandi tidak sama, silahkan dicek ulang', 'Notifikasi', 
			            {
			                timeOut: 40000,
			                closeButton: true,
			                showMethod: "slideDown",
			                hideMethod: "slideUp",
			                positionClass: "toast-bottom-left"
			            });
					}
					else {
						$("#form-change-pw").submit();
					}
				}
				else {
					return false;
				}
			}
			if (pw1 != pw2){
				toastr.info('Kata sandi tidak sama, silahkan dicek ulang', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
			}
			else {
				$("#form-change-pw").submit();
			}
		});
	});
</script>