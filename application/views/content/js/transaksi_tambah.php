<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/moment.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".mask_date").daterangepicker({
			singleDatePicker: true,
			locale:{
				format: 'YYYY-MM-DD'			
			}
		});

		$( function() {
			$("#input-account").autocomplete({
				source: "<?php echo site_url('input-keuangan/akun/ambil') ?>",
				minLength: 2,
				maxLength: 10,
				focus: function(event, ui){
					$("#input-account").val(ui.item.label);
					return false;
				},
				select: function(event, ui){
					// alert(ui.item.id);
					$("#account-id").val(ui.item.id);
					$("#account-name").val(ui.item.value);
					return false;
				}
			});
		});

		function emptyDetail() {
			var akun = $("#input-account").val('');
			var akunId = $("#account-id").val('');
			var akunNama = $("#account-name").val('');
			var debit = $("#input-debit").val('');
			var kredit = $("#input-kredit").val('');
		}

		var i = 0;
		$("#insert_detail").click(function(){
			// alert(i);
			var akun = $("#input-account").val();
			var akunId = $("#account-id").val();
			var akunNama = $("#account-name").val();
			var debit = $("#input-debit").val();
			var kredit = $("#input-kredit").val();
			if (debit == 0 && kredit == 0) {
				toastr.info('Silahkan lengkapi isi nominal uang pada debit atau kredit', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
			}
			else if (akun == '' || akunId == '' || akunNama == '' || debit == '' || kredit == '') {
				toastr.info('Silahkan lengkapi data input atau gunakan Nomor Akun yang tersedia di daftar', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
			}
			else {
				if ($("#table-detail tbody tr td").length == 1) {
					$("#table-detail tbody tr").remove();
				}
				else if ($("#table-detail tbody tr td").length < 1) {
					$("#table-detail tbody tr").append("<tr><td colspan='5'><i>Tidak ada transaksi.</i></td></tr>");
				}
				var detail = '';
				detail += "<tr>";
					detail += "<td><input type='text' name='detail[" + i + "][AccNo]' value='" + akunId + "' id='input-accno" + i + "' class='form-control' readonly required></td>";
					detail += "<td><input type='text' name='AccName" + i + "' value='" + akunNama + "' id='input-accname" + i + "' class='form-control' readonly required></td>";
					detail += "<td><input type='number' name='detail[" + i + "][debit]' value='" + debit + "' id='input-debit" + i + "' class='form-control' required></td>";
					detail += "<td><input type='number' name='detail[" + i + "][kredit]' value='" + kredit + "' id='input-kredit" + i + "' class='form-control' required></td>";
					detail += "<td><a href='javascript:void(0);' class='btn btn-danger delete_row'><i class='fa fa-trash-o'></i>&nbsp;Hapus</a></td>";
				detail += "</tr>";
				i++;

				$("#table-detail tbody").append(detail);
				emptyDetail();

				$(".delete_row").click(function(){
					$(this).parent().parent().remove();
					if ($("#table-detail tbody tr td").length < 1) {
							$("#table-detail tbody").append("<tr><td colspan='5'><i>Tidak ada transaksi.</i></td></tr>");
						}
				});
			}
		});

	});
</script>