<script type="text/javascript">
	$(document).ready(function(){
		$("#modal").on("hide.bs.modal", function (e) {
			// reset
			$('#form-prodi').trigger('reset');
		});

		$("#add").click(function(){
			$("#input-code").prop("disabled", false);
			$(".modal-title").text("Tambah Jenis Pengguna");
			$("input[name=_method]").val("insert");
		})

		$(".edit").click(function(){
			$(".modal-title").text("Ubah Jenis Pengguna");
			$("input[name=_method]").val("update");
			var id = $(this).attr('id');
			$("#input-id").val(id);
			$.ajax({
				url: 'jenis-pengguna/ambil',
				data: {param:id},
				type: 'post',
				dataType: 'json',
				success: function(data){
					$("#input-code").val(data.group_id);
					$("#input-code").prop("disabled", true);
					$("#input-name").val(data.group_name);
				},
				error: function() {
					// body...
				}
			});
		})
	})
</script>