<script type="text/javascript">
	$(document).ready(function(){
		$("#modal").on("hide.bs.modal", function (e) {
			// reset
			$('#form-pengguna').trigger('reset');
		});

		$("#add").click(function(){
			$(".modal-title").text("Tambah Pengguna");
			$("input[name=_method]").val("insert");
		});

		$(".edit").click(function(){
			$(".modal-title").text("Ubah Pengguna");
			$("input[name=_method]").val("update");
			var id = $(this).attr('id');
			$("#input-id").val(id);
			$.ajax({
				url: 'pengguna/ambil',
				data: {param:id},
				type: 'post',
				dataType: 'json',
				success: function(data){
					$("#input-user").val(data.username);
					$("#input-name").val(data.realname);
					$("#input-type").val(data.user_type);
					$("#input-passwd").val(data.passwd);
				},
				error: function() {
					// body...
				}
			});
		});

		$('#show-pass').click(function(){
			if ($('i.s').attr('class') == 'fa fa-eye s') {
				$('#input-passwd').prop('type', 'text');
				$('i.s').prop('class', 'fa fa-eye-slash s');
			}
			else {
				$('#input-passwd').prop('type', 'password');
				$('i.s').prop('class', 'fa fa-eye s');
			}
		});
	})
</script>