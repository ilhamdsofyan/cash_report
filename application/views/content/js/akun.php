<script src="<?php echo ROOT_URL ?>/assets/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#modal").on("hide.bs.modal", function (e) {
			// reset
			$('#form-account').trigger('reset');
		});

		var table = $('#example');

		var oTable = table.dataTable({

		// Internationalisation. For more info refer to http://datatables.net/manual/i18n
		"language": {
			"aria": {
				"sortAscending": ": activate to sort column ascending",
				"sortDescending": ": activate to sort column descending"
			},
			"emptyTable": "Data tidak ada",
			"info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Daftar tidak ada",
			"infoFiltered": "(pencarian dari _MAX_ total data)",
			"lengthMenu": "Menampilkan _MENU_ Data",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada data yang cocok"
		},

		buttons: [],

		// setup responsive extension: http://datatables.net/extensions/responsive/
		responsive: true,

		//"ordering": false, disable column ordering 
		//"paging": false, disable pagination

		"order": [
			[0, 'asc']
		],

		"lengthMenu": [
			[5, 10, 15, 20, -1],
			[5, 10, 15, 20, "Semua"] // change per page values here
		],
		// set the initial value
		"pageLength": 5,

		"dom": "<'row' <'col-lg-12'B>><'row'<'col-md-6 col-sm-12'l><'right col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
		});

		function reload() {
			oTable.ajax.reload();
		}
	});
	
	$("#add").click(function(){
		$("#input-code").prop("disabled", false);
		$(".modal-title").text("Tambah COA/Akun");
		$("input[name=_method]").val("insert");
	});

	$(".edit").click(function(){
		$(".modal-title").text("Ubah COA/Akun");
		$("input[name=_method]").val("update");
		var id = $(this).attr('id');
		$("input[name=input-id]").val(id);

		$.ajax({
			url: 'bagan-akun/ambil',
			data: {param:id},
			type: "post",
			dataType: "json",
			success: function(data){
				$("#input-code").val(data.AccNo);
				$("#input-code").prop("disabled", true);
				$("#input-name").val(data.AccName);
				$("#input-group").val(data.AccGroup);
			},
			error: function(){

			}
		});
	});	

</script>