<script src="<?php echo ROOT_URL ?>/assets/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#modal").on("hide.bs.modal", function (e) {
			// reset
			$('#form-account').trigger('reset');
		});

		var table = $('#example');

		var oTable = table.dataTable({

		// Internationalisation. For more info refer to http://datatables.net/manual/i18n
		"language": {
			"aria": {
				"sortAscending": ": activate to sort column ascending",
				"sortDescending": ": activate to sort column descending"
			},
			"emptyTable": "Data tidak ada",
			"info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Daftar tidak ada",
			"infoFiltered": "(pencarian dari _MAX_ total data)",
			"lengthMenu": "Menampilkan _MENU_ Data",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada data yang cocok"
		},

		buttons: [],

		// setup responsive extension: http://datatables.net/extensions/responsive/
		responsive: true,

		//"ordering": false, disable column ordering 
		//"paging": false, disable pagination

		"order": [
			[0, 'asc']
		],

		"lengthMenu": [
			[5, 10, 15, 20, -1],
			[5, 10, 15, 20, "Semua"] // change per page values here
		],
		// set the initial value
		"pageLength": 5,

		"dom": "<'row' <'col-lg-12'B>><'row'<'col-md-6 col-sm-12'l><'text-right col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
		});

		function reload() {
			oTable.ajax.reload();
		}
	});

	$('.approve').click(function(){
		var novoucher = $(this).attr('name');
		var no_urut = $(this).attr('id');
		var approval;
		if($(this).text() == 'Setujui'){
			approval = 1;
		}
		else {
			approval = 0;
		}

		$.ajax({
			url: 'transaksi/setujui',
			type: 'post',
			data: {NoVoucher:novoucher, approve:approval},
			dataType: 'json',
			success: function(data){
				if (data.status == 'success') {
					$('.approve').text(data.result);
					if (data.result == 'Setujui') {
						$('#approval'+no_urut).text('Belum Disetujui');
						$('#approval'+no_urut).attr('style', 'color:red');
					}
					else {
						$('#approval'+no_urut).text('Disetujui');
						$('#approval'+no_urut).attr('style', 'color:green');
					}
					toastr.info(data.message, 'Notifikasi', 
					{
						timeOut: 1000,
						closeButton: true,
						showMethod: "slideDown",
						hideMethod: "slideUp",
						positionClass: "toast-bottom-left"
					});
				}
			},
			error: function(data){
				
			}
		});
	});

	$('.check').click(function(){
		var novoucher = $(this).attr('id');
		var table;
		var no_urut = $(this).attr('name');
		var i = 1;
		$.ajax({
			url: 'transaksi/ambil',
			type: 'post',
			data: {NoVoucher:novoucher},
			dataType: 'json',
			success: function(data){
				// header
				$('#NoVoucher').text(data.NoVoucher);
				$('#realname').text(data.realname);
				$('#Tanggal').text(data.Tanggal);
				$('#Tanggal_input').text(data.Tanggal_input);
				$('#deskripsi').text(data.deskripsi);
				$('.approve').attr('name', data.NoVoucher);
				$('.approve').attr('id', no_urut);
				if (data.approve == 1) {
					$('.approve').text('Batal setujui');
				}
				else {
					$('.approve').text('Setujui');
				}

				$('#table_modal tbody').empty();
				//detail
				$.each(data[0], function(key, value){

					table =  '<tr>';
						table += '<td>' + i + '</td>';
						table += '<td>' + value.AccNo + '</td>';
						table += '<td>' + value.AccName + '</td>';
						table += '<td>' + parseInt(value.debit).format() + '</td>';
						table += '<td>' + parseInt(value.kredit).format() + '</td>';
					table += '</tr>';

					$('#table_modal tbody').append(table);
					i++;
				});
			},
			error: function(data){

			}
		})
	});

	Number.prototype.format = function(n, x) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
	};
	String.prototype.unformat = function() {
	    return this.replace("Rp. ", "").replace(/\./g, "");
	};
</script>