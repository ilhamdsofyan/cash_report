<script type="text/javascript">
	$(document).ready(function(){
		// Desc Journal
		var html_jn = "<p align='justify'><b>Buku Jurnal</b> adalah media pencatatan transaksi secara kronologis berupa pendebitan dan pengkreditan rekening beserta penjelasan yang diperlukan dari transaksi tersebut.</p>";
		$('.report-jn').popover({
			trigger: 'hover',
			html: true,
			container: 'body',
			content: html_jn,
			placement: 'top'
		});
		// Desc Big Book
		var html_bb = "<p align='justify'><b>Buku besar</b> adalah kumpulan akun yang disusun sedemikian rupa sehingga mudah ditemukan saat diperlukan. <i>Dwi Harti(2015:43)</i>.</p>";
		$('.report-bb').popover({
			trigger: 'hover',
			html: true,
			container: 'body',
			content: html_bb,
			placement: 'top'
		});
		// Desc Trial Balance
		var html_ns = "<p align='justify'><b>Neraca Saldo (trial balance)</b> adalah daftar yang memuat seluruh saldo dari akun yang terdapat pada buku besar. <i>Dwi Harti(2015:43)</i>.</p>";
		$('.report-ns').popover({
			trigger: 'hover',
			html: true,
			container: 'body',
			content: html_ns,
			placement: 'top'
		});
		// Desc Profit and Loss
		var html_lr = "<p align='justify'><b>Laporan Laba Rugi (income statement)</b> adalah laporan kinerja atau kemampuan dalam memperoleh keuntungan. <i>Dwi Harti(2015:43)</i>.</p>";
		$('.report-lr').popover({
			trigger: 'hover',
			html: true,
			container: 'body',
			content: html_lr,
			placement: 'top'
		});
		// Desc Balance
		var html_nr = "<p align='justify'><b>Neraca</b> (balance sheet) adalah laporan keuangan suatu entitas yang dihasilkan pada suatu periode akuntansi yang menunjukkan posisi keuangan suatu perusahaan pada akhir periode tersebut. <i>Dwi Harti(2015:43)</i>.</p>";
		$('.report-nr').popover({
			trigger: 'hover',
			html: true,
			container: 'body',
			content: html_nr,
			placement: 'top'
		});
	})
</script>