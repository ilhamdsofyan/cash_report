<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/moment.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".mask_date").daterangepicker({
			singleDatePicker: true,
			locale:{
				format: 'YYYY-MM-DD'			
			}
		});

		$("#input-account").autocomplete({
			source: "<?php echo site_url('input-keuangan/akun/ambil') ?>",
			minLength: 2,
			maxLength: 10,
			focus: function(event, ui){
				$("#input-account").val(ui.item.label);
				return false;
			},
			select: function(event, ui){
				// alert(ui.item.id);
				$("#account-id").val(ui.item.id);
				$("#account-name").val(ui.item.value);
				return false;
			}
		});

		$(".accno").autocomplete({
			source: "<?php echo site_url('input-keuangan/akun/ambil') ?>",
			minLength: 2,
			maxLength: 10,
			focus: function(event, ui){
				// $("#input-account").val(ui.item.label);
				$(this).val(ui.item.id);
				return false;
			},
			select: function(event, ui){
				// alert(ui.item.id);
				// $("#account-id").val(ui.item.id);
				$(this).closest("tr").find("td:eq(1) input").val(ui.item.value);
				// $("#account-name").val(ui.item.value);
				return false;
			}
		});

		$("#insert_detail").click(function(){
			// alert(i);
			var novoucher = "<?php echo $header['NoVoucher'] ?>";
			var akun = $("#input-account").val();
			var akunId = $("#account-id").val();
			var akunNama = $("#account-name").val();
			var debit = $("#input-debit").val();
			var kredit = $("#input-kredit").val();
			if (debit == 0 && kredit == 0) {
				toastr.info('Silahkan lengkapi isi nominal uang pada debit atau kredit', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
			}
			else if (akun == '' || akunId == '' || akunNama == '' || debit == '' || kredit == '') {
				toastr.info('Silahkan lengkapi data input atau gunakan Nomor Akun yang tersedia di daftar', 'Notifikasi', 
	            {
	                timeOut: 40000,
	                closeButton: true,
	                showMethod: "slideDown",
	                hideMethod: "slideUp",
	                positionClass: "toast-bottom-left"
	            });
			}
			else {
				$('#form-detail').submit();
			}
		});

		$(".delete_row").click(function(){
			if (confirm('Anda yakin ingin menghapus detail yang sudah di simpan?')) {
				var row = $(this).parent().parent();
				var novoucher = "<?php echo $header['NoVoucher'] ?>";
				var id = $(this).closest("tr").find("td:eq(4) input").val();
				var accno = $(this).closest("tr").find("td:eq(0) input").val();
				var data = {param:{NoVoucher:novoucher, id:id}, AccNo:accno};
				
				deleteRow(data, row);
			}
			else{
				return false;
			}
		});

		$(".btn_edit").click(function(){
			if (confirm('Anda yakin ingin menghapus detail yang sudah di simpan?')) {
				var novoucher = "<?php echo $header['NoVoucher'] ?>";
				var id = $(this).closest("tr").find("td:eq(4) input").val();
				var accno = $(this).closest("tr").find("td:eq(0) input").val();
				var debit = $(this).closest("tr").find("td:eq(2) input").val();
				var kredit = $(this).closest("tr").find("td:eq(3) input").val();

				var data = {param:{NoVoucher:novoucher, id:id}, detail:{AccNo:accno, debit:debit, kredit:kredit}};
				
				updateRow(data);
			}
			else{
				return false;
			}
		});
	});

	/**
	 * Function Support
	 */
	function updateRow(data) {
		$.ajax({
			url: 'simpan',
			type: 'post',
			data: data,
			dataType: 'json',
			success: function(data){
				if (data.status == 'success') {
					toastr.info('Data berhasil di ubah', 'Notifikasi', 
					{
					    timeOut: 40000,
					    closeButton: true,
					    showMethod: "slideDown",
					    hideMethod: "slideUp",
					    positionClass: "toast-bottom-left"
					});
				}
				else{
					toastr.info('Data gagal diubah', 'error', 
					{
					    timeOut: 40000,
					    closeButton: true,
					    showMethod: "slideDown",
					    hideMethod: "slideUp",
					    positionClass: "toast-bottom-left"
					});
				}
			},
			error: function(data){

			}
		});
	}

	function deleteRow(data, row) {
		$.ajax({
			url: 'hapus',
			type: 'post',
			data: data,
			dataType: 'json',
			success: function(data){
				if (data.status == 'success') {
					row.remove();
					if ($("#table-detail tbody tr td").length < 1) {
						$("#table-detail tbody").append("<tr><td colspan='5'><i>Tidak ada transaksi.</i></td></tr>");
					}
					toastr.info('Data berhasil di hapus', 'Notifikasi', 
					{
					    timeOut: 40000,
					    closeButton: true,
					    showMethod: "slideDown",
					    hideMethod: "slideUp",
					    positionClass: "toast-bottom-left"
					});

				}
				else{
					toastr.info('Error saat menghapus data', 'error', 
					{
					    timeOut: 40000,
					    closeButton: true,
					    showMethod: "slideDown",
					    hideMethod: "slideUp",
					    positionClass: "toast-bottom-left"
					});
				}
			},
			error: function(data){

			}
		});
	}
</script>