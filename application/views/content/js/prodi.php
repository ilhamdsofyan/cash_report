<script type="text/javascript">
	$(document).ready(function(){
		$("#modal").on("hide.bs.modal", function (e) {
			// reset
			$('#form-prodi').trigger('reset');
		});

		$("#add").click(function(){
			$("#input-code").prop("disabled", false);
			$(".modal-title").text("Tambah Program Studi");
			$("input[name=_method]").val("insert");
		})

		$(".edit").click(function(){
			$(".modal-title").text("Ubah Program Studi");
			$("input[name=_method]").val("update");
			var id = $(this).attr('id');
			$("#input-id").val(id);
			$.ajax({
				url: 'program-studi/ambil',
				data: {param:id},
				type: 'post',
				dataType: 'json',
				success: function(data){
					$("#input-code").val(data.kodeprodi);
					$("#input-code").prop("disabled", true);
					$("#input-name").val(data.namaprodi);
				},
				error: function() {
					// body...
				}
			});
		})
	})
</script>