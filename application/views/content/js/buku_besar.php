<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/moment.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo ROOT_URL ?>/assets/select2/select2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".daterange-picker").daterangepicker({
			locale: {
			  format: 'YYYY-MM-DD'
			},
		});

		$("#select-account").select2({
			placeholder: "-- Silahkan Pilih Nomor Akun --",
			width: '100%'
		});

		$("#select-year").change(function(){
			var year = $(this).val();
			var mulai = year.substr(0, 10);
			var akhir = year.substr(13, 10);
			$.ajax({
				url: 'buku-besar/ambil-akun',
				type: 'post',
				data: {range:{start:mulai, end:akhir}},
				dataType: 'json',
				success: function(data) {
					$("#select-account").empty();
					$("#select-account").prop("disabled", false);
					$("#select-account").append('<option value="semua">Semua Akun</option>');
					$.each(data, function(key, value){
						$("#select-account").append('</option><option value="' + value.AccNo + '"> ' + value.AccNo + ' - ' + value.AccName + ' </option>');
					})
				},
				error: function() {

				}
			})
		});

		$("#show-ledger").click(function(){
			$("#ledger-content").slideUp('fast');
			var year = $("#select-year").val();
			var account = $("#select-account").val();
			var mulai = year.substr(0, 10);
			var akhir = year.substr(13, 10);
			var data;
			if (year != null) {
				if (account != null) {
					data = {range:{start:mulai, end:akhir}, accno:account};
				}
				else {
					data = {range:{start:mulai, end:akhir}};
				}
				$("#ledger-content").empty();
				$.ajax({
					url: 'buku-besar/tampil',
					data: data,
					type: 'post',
					success: function(value) {
						$("#ledger-content").slideDown('slow');
						$("#ledger-content").append(value);
						
						$("#btn-print").click(function(){
							var content = $("#content-print")[0];
							var windows = window.open('','Print-Window');
							windows.document.open();
							windows.document.write('<html><head><title>Laporan Buku Besar</title><link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /></head><body onload="window.print()">'+content.innerHTML+'</body></html>');
							windows.document.close();
						});
					},
					error: function(value) {
						$("#ledger-content").append("<h1>Data tidak ditemukan, silahkan coba lagi.</h1>");
					}
				});
			}
			else {
				toastr.info('Maaf, anda tidak memilih satupun parameter', 'Notifikasi', 
					{
						timeOut: 40000,
						closeButton: true,
						showMethod: "slideDown",
						hideMethod: "slideUp",
						positionClass: "toast-bottom-left"
					})
				return false;
			}
		});

	});
</script>