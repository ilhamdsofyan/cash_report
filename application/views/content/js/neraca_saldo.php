<script src="<?php echo ROOT_URL ?>/assets/daterange-picker/moment.js" type="text/javascript"></script>
<!-- <script src="<?php echo ROOT_URL ?>/assets/select2/select2.min.js" type="text/javascript"></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		// $('#input-date').daterangepicker({
		// 	singleDatePicker: true,
		// 	format: 'MM/YYYY'
		// });

		// $("#select-account").select2({
		// 	placeholder: "-- Silahkan Pilih Nomor Akun --",
		// 	width: '100%'
		// });

		$('#select-year').change(function() {
			$.ajax({
				url: 'neraca-saldo/ambil-bulan',
				type: 'post',
				data: {tahun:$(this).val()},
				dataType: 'json',
				success: function(data) {
					$('#select-month').empty();
					$('#select-month').prop('disabled', false);
					$('#select-month').append('<option value="" selected disabled> - Silahkan Pilih Bulan - </option>');
				
					$.each(data, function(key, value){
						var monthID = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
						var select = '<option value="' + value.bulan + '"> ' + monthID[value.bulan] + ' </option>';
						$('#select-month').append(select);
					});
				},
				error: function(data) {
					// body...
				}
			})
		});

		// $('#select-month').change(function(){
		// 	var year = $('#select-year').val();
		// 	var month = $(this).val();
		// 	$.ajax({
		// 		url: 'neraca-saldo/ambil-akun',
		// 		type: 'post',
		// 		data: {tahun:year, bulan:month},
		// 		dataType: 'json',
		// 		success: function(data) {
		// 			$('#select-account').empty();
		// 			$('#select-account').prop('disabled', false);
		// 			$('#select-account').append('<option value=""></option>');
		// 			$('#select-account').append('<option value="semua">Semua Akun</option>');

		// 			$.each(data, function(key, value){
		// 				var select = '<option value="' + value.AccNo + '"> ' + value.AccName + ' </option>';
		// 				$('#select-account').append(select);
		// 			});
		// 		},
		// 		error: function(data) {
		// 			// body...
		// 		}
		// 	})
		// });

		$('#show-tribal').click(function(){
			$('#content-tribal').slideUp('fast');
			$('#content-tribal').empty();

			var year = $('#select-year').val();
			var month = $('#select-month').val();
			if (year == null || month == null) {
				toastr.info('Silahkan isi semua parameter pencarian', 'Notifikasi', 
				{
					timeOut: 40000,
					closeButton: true,
					showMethod: "slideDown",
					hideMethod: "slideUp",
					positionClass: "toast-bottom-left"
				});
				return false;
			}

			$.ajax({
				url: 'neraca-saldo/tampil',
				type: 'post',
				data: {periode:{tahun:year, bulan:month}},
				success: function(view){
					$('#content-tribal').slideDown('slow');
					$('#content-tribal').append(view);

					$("#btn-print").click(function(){
							var content = $("#content-print")[0];
							var windows = window.open('','Print-Window');
							windows.document.open();
							windows.document.write('<html><head><title>Laporan Buku Besar</title><link href="<?php echo ROOT_URL ?>/assets/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /></head><body onload="window.print()">'+content.innerHTML+'</body></html>');
							windows.document.close();
						});
				},
				error: function(view){
					$('#content-tribal').append('Data tidak ditemukan');
				}
			})
		})
	})
</script>