<div class="portlet light">
	<div class="portlet-title">
		<div class="caption caption-md">
			<span class="caption-subject font-blue-madison bold uppercase">Informasi Neraca Saldo</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="form-group">
			<label class="control-label">Periode</label>
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<select id="select-year" class="form-control">
						<option value="" selected disabled> - Silahkan Pilih Tahun - </option>
						<?php foreach ($year as $key => $value): ?>
							<option value="<?php echo $value['tahun'] ?>"> <?php echo $value['tahun'] ?> </option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-lg-6 col-xs-12">
					<select id="select-month" class="form-control" disabled>
						<option value="" selected disabled> - Silahkan Pilih Bulan - </option>
					</select>
				</div>
			</div>
		</div>
		<!-- <div class="form-group">
			<label class="control-label">Pilih Nomor Akun</label>
			<select class="form-control" id="select-account" disabled>
				<option value="" selected></option>
			</select>
		</div> -->
		<!-- <div class="form-group">
			<div class="row">
				<div class="col-lg-3 col-xs-12">
					<label class="control-label" for="input-daterange">Nama Cabang</label>
				</div>
				<div class="col-lg-9 col-xs-12">
					<div class="form-group">
						<select class="form-control" id="select-account" disabled>
							<option value="" selected></option>
						</select>
					</div>
				</div>
			</div>
		</div> -->
		<div class="text-right">		
			<button class="btn btn-primary" id="show-tribal"><i class="fa fa-search"></i>&nbsp;Tampilkan</button>
		</div>
	</div>
</div>
<div id="content-tribal"></div>