<?php 
	if ($biodata['user_image'] == '') {
		$foto = ROOT_URL . '/assets/images/user.jpg';
	}
	else {
		$foto = ROOT_URL . '/assets/' . $biodata['user_image'];
	}
 ?>
<div class="page-content-inner">
							<div class="row">
								<div class="col-md-12">
									<!-- BEGIN PROFILE SIDEBAR -->
									<div class="profile-sidebar">
										<!-- PORTLET MAIN -->
										<div class="portlet light profile-sidebar-portlet ">
											<!-- SIDEBAR USERPIC -->
											<div class="profile-userpic">
												<img src="<?php echo $foto ?>" class="img-responsive" alt=""> </div>
											<!-- END SIDEBAR USERPIC -->
											<!-- SIDEBAR USER TITLE -->
											<div class="profile-usertitle">
												<div class="profile-usertitle-name"><?php echo $biodata['realname'] ?> </div>
												<!-- <div class="profile-usertitle-job"> <?php echo $biodata['username'] ?> </div> -->
											</div>
											<!-- END SIDEBAR USER TITLE -->
											<!-- SIDEBAR BUTTONS -->
											<div class="profile-userbuttons">
											   <!--  <button type="button" class="btn btn-circle green btn-sm"></button>
												<button type="button" class="btn btn-circle red btn-sm"></button> -->
											</div>
											<!-- END SIDEBAR BUTTONS -->
											<!-- SIDEBAR MENU -->
											<div class="profile-usermenu">
											   <!--  <ul class="nav">
													<li>
														<a href="">
															<i class="icon-home"></i>  </a>
													</li>
													<li class="active">
														<a href="">
															<i class="icon-settings"></i>  </a>
													</li>
													<li>
														<a href="">
															<i class="icon-info"></i>  </a>
													</li>
												</ul> -->
											</div>
											<!-- END MENU -->
										</div>
										<!-- END PORTLET MAIN -->
										<!-- PORTLET MAIN -->
										<div class="portlet light ">
											<!-- STAT -->
											<div class="row list-separated profile-stat">
												
												<div class="col-md-12 col-sm-12 col-xs-6">
													<div class="uppercase profile-stat-title"> <?php echo $log_statistik ?> </div>
													<div class="uppercase profile-stat-text"> Statistik jumlah Login </div>
												</div>
												
											</div>
											<!-- END STAT -->
											<!-- <div>
												<h4 class="profile-desc-title"></h4>
												<span class="profile-desc-text"></span>
												<div class="margin-top-20 profile-desc-link">
													<i class="fa fa-instagram"></i>
													<a href=""></a>
												</div>
												<div class="margin-top-20 profile-desc-link">
													<i class="fa fa-twitter"></i>
													<a href=""></a>
												</div>
												<div class="margin-top-20 profile-desc-link">
													<i class="fa fa-facebook"></i>
													<a href=""></a>
												</div>
											</div> -->
										</div>
										<!-- END PORTLET MAIN -->
									</div>
									<!-- END BEGIN PROFILE SIDEBAR -->
									<!-- BEGIN PROFILE CONTENT -->
									<div class="profile-content">
										<div class="row">
											<div class="col-md-12">
												<div class="portlet light ">
													<div class="portlet-title tabbable-line">
														<div class="caption caption-md">
															<i class="icon-globe theme-font hide"></i>
															<span class="caption-subject font-blue-madison bold uppercase">Informasi Pengguna</span>
														</div>
														<ul class="nav nav-tabs">
															<li class="active">
																<a href="#tab_1_1" data-toggle="tab">Biodata Pribadi</a>
															</li>
															<li>
																<a href="#tab_1_2" data-toggle="tab">Pengaturan Masuk</a>
															</li>
														</ul>
													</div>
													<div class="portlet-body">
														<div class="tab-content">
															<!-- PESONAL INFO TAB -->
															<div class="tab-pane active" id="tab_1_1">
																<form role="form" method="post" action="<?php echo site_url('profil/simpan') ?>" enctype="multipart/form-data">
																	<input type="hidden" name="id" value="<?php echo $biodata['user_id'] ?>">
																	<div class="form-group">
																		<label class="control-label">Nama Lengkap</label>
																		<input type="text" name="profil[realname]" class="form-control" value="<?php echo $biodata['realname'] ?>" required/> </div>
																	<div class="form-group">
																		<label class="control-label">TempatLahir</label>
																		<input type="text" name="profil[tempat_lahir]" class="form-control" value="<?php echo $biodata['tempat_lahir'] ?>" required/> </div>
																	<div class="form-group">
																		<label class="control-label">Tanggal Lahir (tahun-bulan-tanggal)</label>
																		<input type="text" name="profil[tanggal_lahir]" class="form-control mask_date" value="<?php echo $biodata['tanggal_lahir'] ?>" required/> </div>
																	<div class="form-group">
																		<label class="control-label">Jenis Kelamin</label>
																		<?php
																		$gender = array('-- Pilih Jenis Kelamin --', 'Laki-laki', 'Perempuan');
																		?>
																		<select name="profil[jenis_kelamin]" class="form-control" required>
																			<?php 
																			foreach ($gender as $key => $value) {
																				$selected = ($biodata['jenis_kelamin'] == $key) ? 'selected' : '' ;
																			?>
																				<option value="<?php echo $key ?>" <?php echo $selected ?> ><?php echo $value ?> </option>
																			<?php
																			}
																			?>
																		</select>
																	</div>
																	<div class="form-group">
																		<label class="control-label">Alamat</label>
																		<textarea name="profil[alamat]" class="form-control" required style="min-height: 68px;overflow-y: auto;resize: none"><?php echo $biodata['alamat'] ?></textarea></div>
																	<div class="form-group">
																		<label class="control-label">Telepon</label>
																		<input type="text"  name="profil[telepon]" class="form-control" id="phone" value="<?php echo $biodata['telepon'] ?>" required /> </div>
																	<div class="form-group">
																		<label class="control-label">Email</label>
																		<input type="email"  name="profil[email]" class="form-control" value="<?php echo $biodata['email'] ?>" required/> </div>
																	<div class="form-group">
																		<label class="control-label">Foto</label>
																		<input type="file" name="photo" class="form-control" accept="image/*" /> </div>
																	<div class="margiv-top-10">
																		<button type="submit" class="btn green"> Simpan </button>
																		<button type="reset" class="btn default"> Reset </button>
																	</div>
																</form>
															</div>
															<!-- END PERSONAL INFO TAB -->
															<!-- CHANGE PASSWORD TAB -->
															<div class="tab-pane" id="tab_1_2">
																<form action="<?php echo site_url('profil/ubah_login') ?>" id="form-change-pw" method="post">
																	<input type="hidden" name="id" value="<?php echo $biodata['user_id'] ?>">
																	<div class="form-group">
																		<label class="control-label"><i>Username</i></label>
																		<input type="text" name="password[username]" class="form-control uname" value="<?php echo $biodata['username'] ?>" /> </div>
																	<div class="form-group">
																		<label class="control-label">Sandi Saat Ini</label>
																		<input type="password" name="password[current_pass]" class="form-control pwd" /> </div>
																	<div class="form-group">
																		<label class="control-label">Sandi Baru</label>
																		<input type="password" name="password[passwd]" id="pw1" class="form-control" /> 
																		<small><i>Kosongkan jika hanya merubah username</i></small></div>
																	<div class="form-group">
																		<label class="control-label">Ketik Ulang Sandi</label>
																		<input type="password" id="pw2" class="form-control" /><small><i>Kosongkan jika hanya merubah username</i></small></div>
																	<div class="margin-top-10">
																		<button type="button" id="btn-pw-save" class="btn green"> Simpan </button>
																		<button type="reset" class="btn default"> Reset </button>
																	</div>
																</form>
																
															</div>
															<!-- END CHANGE PASSWORD TAB -->
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- END PROFILE CONTENT -->
								</div>
							</div>
						</div>