<?php 
	// buat nomor voucher auto increment
	if (!empty($lastVoucher)) {
		// $exVoucher = explode("-", $lastVoucher['NoVoucher']);
		$date = substr($lastVoucher['NoVoucher'], 1, 4) . '01';
		$dateVoucher = implode("-", (str_split($date, 2)));
		$getMonth = date_format(date_create($dateVoucher), "m");

		if (date("m") != $getMonth) {
			$serial = "01";
		}
		else {
			if (substr($lastVoucher['NoVoucher'], -2) < 9) {
				$serial = substr($lastVoucher['NoVoucher'], -2) + 1;
				$serial = '0'.$serial;
			}
			else {
				$serial = (substr($lastVoucher['NoVoucher'], -2) + 1);
			}
		}
	}
	else {
		$serial = "01";
	}
 ?>
	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
					<div class="portlet-body">
						<form name="form-transaction" action="input-keuangan/simpan" method="post">
							<div class="form-group">
								<div class="row">
									<!-- <div class="col-lg-2 col-xs-12" disabled hidden>
										<label for="input-prodi">Program Studi</label>
									</div>
									<div class="col-lg-4 col-xs-12" disabled hidden>
										<select name="header[kodeprodi]" class="form-control" id="input-prodi" required>
											<option value="" selected disabled>-- Silahkan Pilih Program Studi --</option>
											<?php foreach ($prodi as $key => $value): ?>
												<option value="<?php echo $value['kodeprodi'] ?>"> <?php echo $value['namaprodi'] ?> </option>
											<?php endforeach ?>
										</select>
									</div> -->
									<div class="col-lg-2 col-xs-12">
										<label for="input-code">Nomor Voucher</label>
									</div>
									<div class="col-lg-4 col-xs-12">
										<input type="text" name="header[NoVoucher]" id="input-code" class="form-control" value="<?php echo 'V' . date('ym') . $biodata['user_id'] . @$serial ?>" readonly required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-2 col-xs-12">
										<label for="input-date">Keterangan</label>
									</div>
									<div class="col-lg-4 col-xs-12">
										<textarea name="header[deskripsi]" class="form-control" required style="resize: none; overflow-y: auto;min-height: 55px"></textarea>
										<small><i>Ketikkan '-' (tanpa kutip) jika tidak ada.</i></small>
									</div>
									<div class="col-lg-2 col-xs-12">
										<label for="input-date">Tanggal</label>
									</div>
									<div class="col-lg-4 col-xs-12">
										<input type="text" name="header[Tanggal]" id="input-date" class="form-control mask_date" value="<?php echo date('Y-m-d') ?>" required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
								</div>
							</div>
							<div class="form-group">
								<div class="row">
								</div>
							</div>
							<fieldset>
								<legend></legend>
								<div class="row" align="right">
									<div class="col-lg-4 col-xs-12">
										<input type="text" name="display" id="input-account" class="form-control" placeholder="No Akun - Nama Akun">
										<input type="hidden" name="account_id" id="account-id" class="form-control">
										<input type="hidden" name="account_name" id="account-name" class="form-control">
										<small><i>Ketikkan dan pilih sesuai referensi.</i></small>
									</div>
									<div class="col-lg-3 col-xs-12">
										<input type="text" name="debit" id="input-debit" class="form-control number-format" placeholder="Debit">
										<small><i>Ketikkan angka 0 jika tidak ada.</i></small>
									</div>
									<div class="col-lg-3 col-xs-12">
										<input type="text" name="kredit" id="input-kredit" class="form-control number-format" placeholder="Kredit">
										<small><i>Ketikkan angka 0 jika tidak ada.</i></small>
									</div>
									<div class="col-lg-2 col-xs-12">
										<button class="btn btn-info" id="insert_detail" onclick="return false"><i class="fa fa-plus"></i>&nbsp;Sisipkan</button>
									</div>
								</div>
							</fieldset>
							<hr>
							<div class="table-responsive">
								<table class="table table-condense table-bordered" id="table-detail">
									<thead>
										<tr>
											<th>Nomor Akun</th>
											<th>Nama Akun</th>
											<th>Debit</th>
											<th>Kredit</th>
											<th>#</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="5"><i>Tidak ada transaksi.</i></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-6" align="left">
										<a href="transaksi" class="btn btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
									</div>
									<div class="col-lg-6" align="right">
										<button type="submit" class="btn btn-success"><i class="fa fa-paper-plane"></i>&nbsp;Simpan</button>
										<!-- <input type="submit" name="submit" class="btn btn-success" value="Simpan">	 -->
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					<!-- <img id="loading-image" src="<?php echo ROOT_URL ?>/assets/assets/pages/img/loading-spinner-blue.gif"> -->
						<form action="#" method="post">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-code">Nomor Akun</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[AccNo]" id="input-code" class="form-control">
										<small><i>Contoh : 110001</i></small>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success btn-outline">Simpan</button>
						<button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
			</div>
</div>