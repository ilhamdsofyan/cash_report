	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet light">
				   
					<div class="portlet-body">
						<div align="right">
							<button class="btn btn-primary" id="add" data-toggle="modal" data-target="#modal"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-stiped table-bordered">
										<thead style="background-color: #2b609e;color: #fff">
											<tr>
												<th>No</th>
												<th>Kode Grup</th>
												<th>Nama Grup</th>
												<th>Dibuat Pada</th>
												<th>Diubah Pada</th>
												<th>#</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												if (!empty($group)) {
													$i = 1;
													foreach ($group as $key => $value) {
											 ?>
													<tr>
														<td><?php echo $i ?></td>
														<td><?php echo $value['group_id'] ?></td>
														<td><?php echo $value['group_name'] ?></td>
														<td>
															<?php 
																$create = date_create($value['created_at']);
																echo date_format($create, 'j F Y | H:i A') 
															?>
														</td>
														<td>
															<?php 
																$update = date_create($value['updated_at']);
																echo date_format($update, 'j F Y | H:i A') 
															?>
														</td>
														<td>
															<button class="btn btn-warning btn-block edit" id="<?php echo $value['group_id'] ?>" data-toggle="modal" data-target="#modal"><i class="fa fa-pencil"></i>&nbsp; Ubah</button>
															<a href="jenis-pengguna/hapus/<?php echo $value['group_id'] ?>" class="btn btn-danger btn-block" onclick="return confirm('Yakin ingin dihapus?')"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
														</td>
													</tr>
											<?php 
														$i++;
													}
												}
												else {
											?>
													<tr>
														<td colspan="4"><i>Data Tidak Ada.</i></td>
													</tr>
											<?php
												}
											 ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					<!-- <img id="loading-image" src="<?php echo ROOT_URL ?>/assets/assets/pages/img/loading-spinner-blue.gif"> -->
						<form action="jenis-pengguna/simpan" method="post" id="form-prodi">
							<input type="hidden" name="_method" value="insert">
							<input type="hidden" name="id" id="input-id">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-code">Kode Grup</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[group_id]" id="input-code" class="form-control">
										<small><i>Gunakan Angka 0-9</i></small>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-xs-12">
										<label for="input-code">Nama Grup</label>
									</div>
									<div class="col-lg-9 col-xs-12">
										<input type="text" name="data[group_name]" id="input-name" class="form-control">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit" form='form-prodi' class="btn btn-success btn-outline">Simpan</button>
						<button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				<!-- /.modal-dialog -->
			</div>
</div>