<div class="portlet light">
	<div class="portlet-title tabbable-line">
		<div class="caption caption-md">
			<span class="caption-subject font-blue-madison bold uppercase">Informasi Buku Besar</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="form-group">
			<label class="control-label">Pilih Jangka Waktu</label>
			<input type="text" id="select-year" class="form-control daterange-picker">
			<!-- <label class="control-label">Pilih Jangka Tahun</label>
			<select class="form-control" id="select-year">
				<option value="" selected disabled> -- Silahkan Pilih Tahun -- </option>
				<?php foreach ($year as $key => $value): ?>
					<option value="<?php echo $value['tahun'] ?>"> <?php echo $value['tahun'] ?> </option>
				<?php endforeach ?>
			</select> -->
		</div>
		<div class="form-group">
			<label class="control-label">Pilih Nomor Akun</label>
			<select class="form-control" id="select-account" disabled>
				<option value="" selected></option>
			</select>
		</div>
		<button class="btn btn-primary" id="show-ledger">Tampilkan</button>
	</div>
</div>
<div id="ledger-content"></div>