<div class="page-content-inner">
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<div class="portlet box blue-madison">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-list-alt"></i>Menu</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"> </a>
								<a href="" class="fullscreen"> </a>
								<!-- <a href="javascript:;" class="remove"> </a> -->
							</div>
						</div>
						<div class="portlet-body">
							<?php if ($biodata['user_type'] != 4): ?>
								<div class="row">
									<div class="col-lg-3 col-md-offset-1 col-xs-12">
										<div class="panel panel-success report-jn">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<br>
														<i class="fa fa-bookmark-o fa-4x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div><h1>Jurnal Umum</h1></div>
													</div>
												</div>
											</div>
											<a href="<?php echo site_url('laporan/jurnal-umum') ?>">
												<div class="panel-footer">
													<span class="pull-left">Menuju Tautan</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									<div class="col-lg-3 col-xs-12">
										<div class="panel panel-primary report-bb">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<br>
														<i class="fa fa-book fa-4x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div><h1>Buku Besar</h1></div>
													</div>
												</div>
											</div>
											<a href="<?php echo site_url('laporan/buku-besar') ?>">
												<div class="panel-footer">
													<span class="pull-left">Menuju Tautan</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									<div class="col-lg-3 col-xs-12">
										<div class="panel panel-info report-ns">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<br>
														<i class="fa fa-balance-scale fa-4x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div><h1>Neraca Saldo</h1></div>
													</div>
												</div>
											</div>
											<a href="<?php echo site_url('laporan/neraca-saldo') ?>">
												<div class="panel-footer">
													<span class="pull-left">Menuju Tautan</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
								</div>
							<?php endif ?>
							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-lg-3 col-md-offset-3 col-xs-12">
											<div class="panel panel-warning report-lr">
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-3">
															<br>
															<!-- <i class="fa fa-plus fa-4x"></i> -->
															<i class="fa fa-list fa-4x"></i>
														</div>
														<div class="col-xs-9 text-right">
															<div><h1>Laba Rugi</h1></div>
														</div>
													</div>
												</div>
												<a href="<?php echo site_url('laporan/laba-rugi') ?>">
													<div class="panel-footer">
														<span class="pull-left">Menuju Tautan</span>
														<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
														<div class="clearfix"></div>
													</div>
												</a>
											</div>
										</div>
										<div class="col-lg-3 col-xs-12">
											<div class="panel panel-danger report-nr">
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-3">
															<br>
															<i class="fa fa-university fa-4x"></i>
														</div>
														<div class="col-xs-9 text-right">
															<div><h1>Neraca</h1></div>
														</div>
													</div>
												</div>
												<a href="<?php echo site_url('laporan/neraca') ?>">
													<div class="panel-footer">
														<span class="pull-left">Menuju Tautan</span>
														<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
														<div class="clearfix"></div>
													</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END Portlet PORTLET-->
		</div>
	</div>
</div>