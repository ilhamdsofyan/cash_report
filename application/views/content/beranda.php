<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN Portlet PORTLET-->
			<div class="portlet box red-sunglo">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-info"></i>Info Financial Report </div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
						<a href="" class="fullscreen"> </a>
						<a href="javascript:;" class="remove"> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="textDestination">
						<strong>Selamat datang, <?php echo $biodata['realname'] ?></strong><br/><br/> Fasilitas ini di khususkan bagi para karyawan <b>LP3I</b> khususnya divisi keuangan.<br/>Untuk itu silahkan di pergunakan dengan baik dan di jadikan sebagai sarana penginputan data keuangan yang nantinya akan di laporkan.
					</div>
				</div>
			</div>
			<!-- END Portlet PORTLET-->
		</div>
	</div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="portlet box blue-madison">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-info"></i>Mini Menu</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"> </a>
							<a href="" class="fullscreen"> </a>
							<!-- <a href="javascript:;" class="remove"> </a> -->
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-lg-12">
								<?php if ($biodata['user_type'] == 1): ?>
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-hdd-o"></i>Data Master</div>
										<div class="tools">
											<a href="javascript:;" class="collapse"> </a>
											<a href="" class="fullscreen"> </a>
											<!-- <a href="javascript:;" class="remove"> </a> -->
										</div>
									</div>
									<div class="portlet-body">
										<div class="row">
											<div class="col-lg-3 col-md-offset-1 col-xs-12">
												<div class="panel panel-primary">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-3">
																<br>
																<i class="fa fa-file fa-4x"></i>
															</div>
															<div class="col-xs-9 text-right">
																<div><h1><?php echo $dataMaster['tb_account'] ?></h1></div>
																<div>COA / Bagan Akun</div>
															</div>
														</div>
													</div>
													<a href="<?php echo site_url('data-master/bagan-akun') ?>">
														<div class="panel-footer">
															<span class="pull-left">Lihat Selengkapnya</span>
															<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
															<div class="clearfix"></div>
														</div>
													</a>
												</div>
											</div>
											<div class="col-lg-3 col-xs-12">
												<div class="panel panel-primary">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-3">
																<br>
																<i class="fa fa-user fa-4x"></i>
															</div>
															<div class="col-xs-9 text-right">
																<div><h1><?php echo $dataMaster['tb_user'] ?></h1></div>
																<div>Pengguna Aplikasi</div>
															</div>
														</div>
													</div>
													<a href="<?php echo site_url('data-master/pengguna') ?>">
														<div class="panel-footer">
															<span class="pull-left">Lihat Selengkapnya</span>
															<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
															<div class="clearfix"></div>
														</div>
													</a>
												</div>
											</div>
											<div class="col-lg-3 col-xs-12">
												<div class="panel panel-primary">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-3">
																<br>
																<i class="fa fa-group fa-4x"></i>
															</div>
															<div class="col-xs-9 text-right">
																<div><h1><?php echo $dataMaster['tb_user_group'] ?></h1></div>
																<div>Tipe Pengguna</div>
															</div>
														</div>
													</div>
													<a href="<?php echo site_url('data-master/jenis-pengguna') ?>">
														<div class="panel-footer">
															<span class="pull-left">Lihat Selengkapnya</span>
															<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
															<div class="clearfix"></div>
														</div>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endif; ?>
								<?php if ($biodata['user_type'] == 1 || $biodata['user_type'] == 2): ?>
									<div class="row">
	    								<div class="col-md-3 col-md-offset-4">
											<div class="panel panel-info">
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12" align="center">
															<br>
															<i class="fa fa-money fa-4x"></i>
															<div>Transaksi</div>
														</div>
													</div>
												</div>
												<a href="<?php echo site_url('transaksi') ?>">
													<div class="panel-footer">
														<span class="pull-left">Menuju Tautan</span>
														<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
														<div class="clearfix"></div>
													</div>
												</a>
											</div>
										</div>
									</div>
								<?php endif; ?>
							<?php if ($biodata['user_type'] != 2): ?>
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet box yellow">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-bar-chart"></i>Laporan</div>
												<div class="tools">
													<a href="javascript:;" class="collapse"> </a>
													<a href="" class="fullscreen"> </a>
													<!-- <a href="javascript:;" class="remove"> </a> -->
												</div>
											</div>
											<div class="portlet-body">
												<?php if ($biodata['user_type'] != 2 && $biodata['user_type'] != 4): ?>
													<div class="row">
														<div class="col-lg-3 col-md-offset-1 col-xs-12">
															<div class="panel panel-warning">
																<div class="panel-heading">
																	<div class="row">
																		<div class="col-xs-3">
																			<br>
																			<i class="fa fa-bookmark-o fa-4x"></i>
																		</div>
																		<div class="col-xs-9 text-right">
																			<div><h1>Jurnal Umum</h1></div>
																		</div>
																	</div>
																</div>
																<a href="<?php echo site_url('laporan/buku-besar') ?>">
																	<div class="panel-footer">
																		<span class="pull-left">Menuju Tautan</span>
																		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																		<div class="clearfix"></div>
																	</div>
																</a>
															</div>
														</div>
														<div class="col-lg-3 col-xs-12">
															<div class="panel panel-warning">
																<div class="panel-heading">
																	<div class="row">
																		<div class="col-xs-3">
																			<br>
																			<i class="fa fa-book fa-4x"></i>
																		</div>
																		<div class="col-xs-9 text-right">
																			<div><h1>Buku Besar</h1></div>
																		</div>
																	</div>
																</div>
																<a href="<?php echo site_url('laporan/buku-besar') ?>">
																	<div class="panel-footer">
																		<span class="pull-left">Menuju Tautan</span>
																		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																		<div class="clearfix"></div>
																	</div>
																</a>
															</div>
														</div>
														<div class="col-lg-3 col-xs-12">
															<div class="panel panel-warning">
																<div class="panel-heading">
																	<div class="row">
																		<div class="col-xs-3">
																			<br>
																			<i class="fa fa-balance-scale fa-4x"></i>
																		</div>
																		<div class="col-xs-9 text-right">
																			<div><h1>Neraca Saldo</h1></div>
																		</div>
																	</div>
																</div>
																<a href="<?php echo site_url('laporan/neraca-saldo') ?>">
																	<div class="panel-footer">
																		<span class="pull-left">Menuju Tautan</span>
																		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																		<div class="clearfix"></div>
																	</div>
																</a>
															</div>
														</div>
													</div>
												<?php endif ?>
												<div class="row">
													<div class="col-lg-3 col-md-offset-3 col-xs-12">
														<div class="panel panel-warning">
															<div class="panel-heading">
																<div class="row">
																	<div class="col-xs-3">
																		<br>
																		<i class="fa fa-plus fa-4x"></i>
																		<i class="fa fa-minus fa-4x"></i>
																	</div>
																	<div class="col-xs-9 text-right">
																		<div><h1>Laba Rugi</h1></div>
																	</div>
																</div>
															</div>
															<a href="<?php echo site_url('laporan/laba-rugi') ?>">
																<div class="panel-footer">
																	<span class="pull-left">Menuju Tautan</span>
																	<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																	<div class="clearfix"></div>
																</div>
															</a>
														</div>
													</div>
													<div class="col-lg-3 col-xs-12">
														<div class="panel panel-warning">
															<div class="panel-heading">
																<div class="row">
																	<div class="col-xs-3">
																		<br>
																		<i class="fa fa-university fa-4x"></i>
																	</div>
																	<div class="col-xs-9 text-right">
																		<div><h1>Neraca</h1></div>
																		<br><br>
																	</div>
																</div>
															</div>
															<a href="<?php echo site_url('laporan/neraca') ?>">
																<div class="panel-footer">
																	<span class="pull-left">Menuju Tautan</span>
																	<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
																	<div class="clearfix"></div>
																</div>
															</a>
														</div>
													</div>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
							<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			<?php if (isset($logged)): ?>
				<div class="col-lg-12 col-xs-12">
					<div class="portlet box blue-madison">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-info"></i>Akun Login</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"> </a>
								<a href="" class="fullscreen"> </a>
								<a href="javascript:;" class="remove"> </a>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div class="table-responsive"> -->
								<table class="table table-striped table-bordered" id="example">
									<thead>
										<tr>
											<th>No</th>
											<th>Username</th>
											<th>IP</th>
											<th>Waktu</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											if (!empty($logged)) {
												$i = 1;
												foreach ($logged as $key => $value) {
										?>
													<tr>
														<td><?php echo $i ?></td>
														<td><?php echo $value['username'] ?></td>
														<td><?php echo $value['ipaddress'] ?></td>
														<td><?php echo $value['tanggal'] ?></td>
													</tr>
										<?php
												$i++;
												}
											}
											else { ?>
											<tr>
												<td colspan="4"><i><b>Data Tidak Tersedia.</b></i></td>
											</tr>
										<?php 
											}
										 ?>
									</tbody>
								</table>	
							<!-- </div> -->
							<hr>
							<small>** daftar list akun login</small>
						</div>
					</div>				
				</div>
			<?php endif ?>
			</div>
			<!-- END Portlet PORTLET-->
		</div>
	</div>
</div>